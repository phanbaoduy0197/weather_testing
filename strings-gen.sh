#!/usr/bin/env bash

#read *.arb files and gen code into /lib/res/strings.dart file
#java -jar ./tools/res_generator.jar type=string path=./lib/res/l10n default_language=en check_missing=true template_file=./lib/res/strings.dart gen_mode=message

#read /lib/res/strings.dart and gen messages files to lib/res/l10n
flutter pub pub run easy_localization:generate -f keys -o locale_keys.g.dart

#java -jar ./tools/res_generator.jar type=string path=./lib/res/l10n default_language=en check_missing=false template_file=./lib/res/strings.dart gen_mode=key