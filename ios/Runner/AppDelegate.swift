import UIKit
import Flutter
import AppTrackingTransparency

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let channel = FlutterMethodChannel(name: "com.example.weather_testing/native",
                                            binaryMessenger: controller.binaryMessenger)
      channel.setMethodCallHandler({
      [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
      self?.handleMethodCall(call: call, result: result)
    })


//    #if Live
//    GMSServices.provideAPIKey("AIzaSyDQjnGOXiqcMaeOZpzD2KYL9ah_pZNVnwI")
//    #elseif Staging
//    GMSServices.provideAPIKey("AIzaSyDQjnGOXiqcMaeOZpzD2KYL9ah_pZNVnwI")
//    #endif
    GeneratedPluginRegistrant.register(with: self)

    if #available(iOS 10.0, *) {
      UNUserNotificationCenter.current().delegate = self
    }

    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }


  func currentFlavor() -> String  {
    #if Live
    return "Live";
    #elseif Staging
    return "Staging";
    #else
    return "unkown";
    #endif
  }

  func handleMethodCall(call: FlutterMethodCall, result: @escaping FlutterResult) {
    if call.method == "flavor" {
      result(currentFlavor())
    } else if call.method == "noti5Permission" {
      let current = UNUserNotificationCenter.current()
      current.getNotificationSettings { (settings) in
        switch settings.authorizationStatus {
        case .denied:
          result(-1)
        case .notDetermined:
          result(0)
        default:
          result(1)

        }
      }
    } else if call.method == "setNoti5UnseenCount" {
      let ud = UserDefaults.init(suiteName: "group.com.rever.ios")
      if let count = call.arguments as? Int {
        ud?.setValue(count, forKey: "pns_agents_unseen_count_\(currentFlavor())")
        ud?.synchronize()
      }
    } else if(call.method == "getTrackingPermission") {
      result(getTrackingPermission())
      return
    } else if(call.method == "requestTrackingPermission") {
      requestTrackingPermission(result: result)
      return
    }
  }


  func requestTrackingPermission(result: @escaping FlutterResult)  {
    if #available(iOS 14.5, *) {
      if ATTrackingManager.trackingAuthorizationStatus == .notDetermined {
        ATTrackingManager.requestTrackingAuthorization { (status) in
          result(status.rawValue)
        }
      } else {
        result(getTrackingPermission())
      }
    } else {
      result(3)
    }
  }

  func getTrackingPermission() -> UInt {
    if #available(iOS 14.5, *) {
      return ATTrackingManager.trackingAuthorizationStatus.rawValue
    }

    return 3
  }
}
