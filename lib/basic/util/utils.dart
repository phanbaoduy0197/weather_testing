import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:intl/intl.dart';
import 'package:path/path.dart' as p;
import 'package:path_provider/path_provider.dart';
import "package:unorm_dart/unorm_dart.dart" as unorm;

typedef Callback = void Function();
typedef Callback1<T> = void Function(T);
typedef Callback2<T1, T2> = void Function(T1, T2);
typedef Callback3<T1, T2, T3> = void Function(T1, T2, T3);
typedef CallbackR<R> = R Function();
typedef CallbackR1<R, T> = R Function(T);
typedef CallbackR2<R, T1, T2> = R Function(T1, T2);
typedef CallbackR3<R, T1, T2, T3> = R Function(T1, T2, T3);

class Tuple2<T, S> {
  final T item1;
  final S item2;

  const Tuple2(this.item1, this.item2);

  @override
  String toString() => '[$item1, $item2]';

  @override
  bool operator ==(other) =>
      other is Tuple2 && other.item1 == item1 && other.item2 == item2;

  @override
  int get hashCode => item1.hashCode ^ item2.hashCode;

  List toList() {
    if (item1 == null && item2 == null) return [];
    if (item1 == null && item2 != null) return [item2];
    if (item1 != null && item2 == null) return [item1];
    return [item1, item2];
  }

  Tuple2<T, S> copyWith({
    T? item1,
    S? item2,
  }) {
    return Tuple2<T, S>(
      item1 ?? this.item1,
      item2 ?? this.item2,
    );
  }
}

class Tuple3<T, S, U> {
  final T item1;
  final S item2;
  final U item3;

  Tuple3(this.item1, this.item2, this.item3);

  @override
  String toString() => '[$item1, $item2, $item3]';

  @override
  bool operator ==(other) =>
      other is Tuple3 &&
      other.item1 == item1 &&
      other.item2 == item2 &&
      other.item3 == item3;

  @override
  int get hashCode => item1.hashCode ^ item2.hashCode ^ item3.hashCode;
}

class Tuple4<T, S, U, K> {
  final T item1;
  final S item2;
  final U item3;
  final K item4;

  Tuple4(this.item1, this.item2, this.item3, this.item4);

  @override
  String toString() => '[$item1, $item2, $item3,$item4]';

  @override
  bool operator ==(other) =>
      other is Tuple4 &&
      other.item1 == item1 &&
      other.item2 == item2 &&
      other.item3 == item3 &&
      other.item4 == item4;

  @override
  int get hashCode =>
      item1.hashCode ^ item2.hashCode ^ item3.hashCode ^ item4.hashCode;
}

extension StringExtension on String {
  String removeAccents() {
    // return result;
    final combining = RegExp(r"[\u0300-\u036F]");
    final combining2 = RegExp(r"[đĐ]");
    var result = unorm.nfd(this);
    result = result.replaceAll(combining, "").replaceAll(combining2, "d");
    return result;
  }

  String removeNonBreakingSpace() {
    return replaceAll(' ', '\u00A0');
  }

  bool isInt() {
    return int.tryParse(this) == null ? false : true;
  }

  double toDouble([double? fallback]) {
    return double.tryParse(this) ?? fallback ?? 0.0;
  }

  int toInt([int? fallback]) {
    return int.tryParse(this) ?? fallback ?? 0;
  }

  bool isHtml() {
    final regExp = RegExp(
      r"<\/?[a-z][\s\S]*>",
      caseSensitive: false,
    );
    return regExp.hasMatch(this);
  }

  String removeAllHtmlTags() {
    RegExp exp = RegExp(r"<[^>]*>", multiLine: true, caseSensitive: true);
    return replaceAll(exp, '');
  }

  String cleanupHtml() {
    String html = this;
    // String html = this
    //     .replaceAllMapped(RegExp("text-decoration: underline;"), (match) => "");
    html = html.replaceAllMapped(
        RegExp("<meta charset=\"utf-8\" />"), (match) => "");
    html = html.replaceAllMapped(
        RegExp("border-top:0px;"), (match) => "border-top: double;");
    html = html.replaceAllMapped(RegExp("rowspan=\"+d\""), (match) => "");
    return html;
  }

  String? get nullOrValue {
    if (isNotEmpty == true) {
      return this;
    }
    return null;
  }
}

extension FileExtension on File {
  String get fileName => p.basename(path);
  String get extension => p.extension(path);
  bool get isPng => extension.toLowerCase().endsWith("png");
  bool get isJpg {
    final ext = extension.toLowerCase();
    return ext.endsWith("jpg") || ext.endsWith("jpeg");
  }

  String subfixFileName(String subfix) =>
      "${p.basenameWithoutExtension(path)}$subfix${p.extension(path)}";
}

extension NumExtension1 on num {
  num clamp(num min, num max) {
    if (this < min) return min;
    if (this > max) return max;
    return this;
  }
}

extension IntExtension on int {
  int fibonacci() {
    // calculate fibonacci with hash map cache
    final cache = <int, int>{};
    int fib(int n) {
      if (n == 0 || n == 1) {
        return n;
      }
      if (cache.containsKey(n)) {
        return cache[n]!;
      }
      final result = fib(n - 1) + fib(n - 2);
      cache[n] = result;
      return result;
    }

    return fib(this);
  }
}

final _futureLock = <String, Completer>{};
Future runFutureWithLock(String lock, CallbackR<Future> block) {
  if (_futureLock[lock] != null) {
    return _futureLock[lock]!.future;
  }

  final completer = Completer();
  _futureLock[lock] = completer;
  block().then((value) {
    completer.complete(value);
    _futureLock.remove(lock);
  }).catchError((err, stack) {
    completer.completeError(err, stack);
    _futureLock.remove(lock);
  });

  return completer.future;
}

int millisecondsSinceEpoch() => DateTime.now().millisecondsSinceEpoch;

extension DateTimeExt on DateTime {
  static String? kDefaultLocale = "vi";
  DateTime addMonth(int monthToAdd) {
    return DateTime(year, month + monthToAdd, day, hour, minute, second,
        millisecond, microsecond);
  }

  bool isToday() {
    return onSameDate(DateTime.now());
  }

  bool onSameDate(DateTime otherDate) {
    return year == otherDate.year &&
        month == otherDate.month &&
        day == otherDate.day;
  }

  bool onSameMonth(DateTime otherDate) {
    return year == otherDate.year && month == otherDate.month;
  }

  DateTime stripTime() {
    return DateTime(year, month, day);
  }

  String formatDateInVie() {
    final weekDay =
        weekday != 7 ? "Thứ ${(weekday + 1).toString()}" : "Chủ nhật";
    final formatDate =
        "$weekDay${DateFormat(", 'ngày' dd/MM/yyyy", "vi-VN").format(DateTime(year, month, day))}";
    return formatDate;
  }

  String formatAppointmentDate() {
    final dateFormat = DateFormat('MMMMEEEEd');
    return dateFormat.format(this);
  }

  String formatHHmm() {
    final dateFormat = DateFormat.Hm();
    return dateFormat.format(this);
  }

  String formatYearMonth() {
    final dateFormat = DateFormat('MMMM, y', kDefaultLocale);
    return dateFormat.format(this);
  }

  String formatFull() {
    final dateFormat = DateFormat.yMMMMd().add_Hm();
    return dateFormat.format(this);
  }

  DateTime copyWith({
    int? year,
    int? month,
    int? day,
    int? hour,
    int? minute,
    int? second,
    int? millisecond,
    int? microsecond,
  }) {
    return DateTime(
      year ?? this.year,
      month ?? this.month,
      day ?? this.day,
      hour ?? this.hour,
      minute ?? this.minute,
      second ?? this.second,
      millisecond ?? this.millisecond,
      microsecond ?? this.microsecond,
    );
  }
}

extension ListExt<E> on List<E> {
  List<E> listByToggle(E item, [bool prepend = true]) {
    if (contains(item)) {
      //remove
      return [...where((element) => element != item)];
    } else {
      return prepend ? [item, ...this] : [...this, item];
    }
  }
}

extension PdfUint8List on Uint8List {
  Future<String> writeToTmpFile(String name) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    final File file = File('${directory.path}/$name');
    final raf = file.openSync(mode: FileMode.write);
    raf.writeFromSync(this);
    raf.closeSync();
    return file.path;
  }
}
