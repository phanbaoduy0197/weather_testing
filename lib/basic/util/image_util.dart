import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/widgets.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:uuid/uuid.dart';

Future<File> compressImage(
  File input, {
  double maxWidth = 720.0,
  double maxHeight = 108,
}) async {
  Completer<File> completer = Completer<File>();
  final imageFile = Image.file(input);

  imageFile.image
      .resolve(const ImageConfiguration())
      .addListener(ImageStreamListener((info, _) async {
    double xScale = info.image.width / maxWidth;
    double yScale = info.image.height / maxHeight;
    double scale = max(xScale, yScale);

    final dir = await path_provider.getTemporaryDirectory();
    File output = createFile("${dir.absolute.path}/${const Uuid().v4()}.jpg");

    final result = await FlutterImageCompress.compressAndGetFile(
      input.path,
      output.path,
      quality: 96,
      minWidth: scale > 1 ? info.image.width ~/ scale : info.image.width,
      minHeight: scale > 1 ? info.image.height ~/ scale : info.image.height,
      format: CompressFormat.jpeg,
    );
    completer.complete(result);
  }));

  return completer.future;
}

double getScaleRatio(BuildContext context) {
  // var pixRatio = MediaQuery.of(context).devicePixelRatio;
  return 0.0;
}

File createFile(String path) {
  final file = File(path);
  if (!file.existsSync()) {
    file.createSync(recursive: true);
  }

  return file;
}
