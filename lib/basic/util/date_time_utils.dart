import 'package:base_entities/basic/util/utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

String formatTimeOfDuration(int seconds) {
  String result = "";

  if (seconds > 0) {
    if (seconds < 60) {
      result = "00:${_formatDecimalTime(seconds)}";
    } else {
      int minutes = seconds ~/ 60;
      result = _formatDecimalTime(seconds % 60);
      if (minutes < 60) {
        result = "${_formatDecimalTime(minutes)}:$result";
      } else {
        int hours = minutes ~/ 60;
        result =
            "${_formatDecimalTime(hours)}:${_formatDecimalTime(minutes % 60)}:$result";
      }
    }
  } else {
    result = "00:00";
  }
  return result;
}

String _formatDecimalTime(int number) {
  return number <= 9 ? "0$number" : "$number";
}

//convert từ timeStamp sang datetime
//data truyền vào là int
DateTime convertIntToDateTime(int timestamp) {
  return DateTime.fromMillisecondsSinceEpoch(timestamp);
}

DateTime? convertIntToDateTimeNull(int? timestamp) {
  if (timestamp == null || timestamp == 0) return null;
  return DateTime.fromMillisecondsSinceEpoch(timestamp);
}

String compareDisplayTime(DateTime date) {
  var now = DateTime.now();
  var diff = now.difference(date);
  var time = '';
  if (diff.inMinutes <= 1) {
    time = 'Vừa mới';
  } else if (diff.inMinutes > 1 && diff.inMinutes <= 60) {
    time = '${diff.inMinutes} phút trước';
  } else if (diff.inHours > 0 && diff.inHours < 24) {
    time = "${diff.inHours} giờ trước";
  } else if (diff.inDays > 0 && diff.inDays < 7) {
    time = '${diff.inDays} ngày trước';
  } else if (diff.inDays >= 7 && diff.inDays < 31) {
    time = "${(diff.inDays / 7).floor()} tuần trước";
  } else if (diff.inDays >= 31 && diff.inDays < 365) {
    time = "${(diff.inDays / 30).floor()} tháng trước";
  } else {
    time = "${(diff.inDays / 365).floor()} năm trước";
  }
  return time;
}

String readTimeStampFromInt(int? timestamp, {String? special}) {
  if (timestamp == null) return "";
  var format = DateFormat('HH:mm ${special ?? "•"} dd/MM/yyyy');
  var date = DateTime.fromMillisecondsSinceEpoch(timestamp);
  return format.format(date);
}

String convertDateTimeToObvious(DateTime? date) {
  if (date == null) return "";
  return "${date.day} th${date.month}, ${date.year}";
}

String getMonthYear(DateTime date) {
  return "Tháng ${date.month}, ${date.year}";
}

String getWeekTime(DateTime date1, DateTime? date2) {
  return "${date1.day}, th${date1.month}${date2 == null ? "" : " - ${date2.day}, th${date2.month}"} / ${date1.year}";
}

String formatGetMonth(int value) {
  switch (value) {
    case 1:
      return "Jan";
    case 2:
      return "Feb";
    case 3:
      return "Mar";
    case 4:
      return "Apr";
    case 5:
      return "May";
    case 6:
      return "Jun";
    case 7:
      return "Jul";
    case 8:
      return "Aug";
    case 9:
      return "Sep";
    case 10:
      return "Oct";
    case 11:
      return "Nov";
    case 12:
      return "Dec";
    default:
      return "Jan";
  }
}

//format time dd/mm/yyyy
String convertTimeStampToString(String? timeStamp) {
  if (timeStamp == null) return "";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp.toInt());
  return DateFormat('dd/MM/yyyy').format(date);
}

//01 th4,2022.
String kformatDateTime(int? timeStamp, BuildContext context) {
  if (timeStamp == null) return "";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  var day = date.day;
  final String language =
      EasyLocalization.of(context)?.locale.languageCode ?? "vi";
  if (language == "vi") {
    var rday = '$day';
    if (day < 10) {
      rday = '0$rday';
    }
    return "$rday th${date.month},${date.year}";
  }
  return "${date.day} ${formatGetMonth(date.month)},${date.year}";
}

String kformatDateTime1(int? timeStamp, BuildContext context) {
  if (timeStamp == null) return "";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  var day = date.day;
  final String language =
      EasyLocalization.of(context)?.locale.languageCode ?? "vi";
  if (language == "vi") {
    var rday = '$day';
    if (day < 10) {
      rday = '0$rday';
    }
    return "$rday th${date.month},${date.year}";
  }
  return "${date.day} ${formatGetMonth(date.month)},${date.year},dat";
}

//01 th4,2022./no context
String krformatDateTime(int? timeStamp) {
  if (timeStamp == null || timeStamp == 0) return "";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  var day = date.day;
  var rday = '$day';
  if (day < 10) {
    rday = '0$rday';
  }
  return "$rday th${date.month}, ${date.year}";
}

//01 th4./no context
String formatDateTime1(int? timeStamp,
    {bool withTime = false, bool withYear = false}) {
  if (timeStamp == null || timeStamp == 0) return "00";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  var day = '${date.day}';
  if (date.day < 10) {
    day = '0$day';
  }
  var rs = "$day th${date.month}";
  if (withYear) {
    rs = "$rs${withYear ? ", ${date.year}" : ""}";
  }
  if (withTime) {
    final mt = date.minute < 10 ? "0${date.minute}" : "${date.minute}";
    final h = date.hour < 10 ? "0${date.hour}" : "${date.hour}";
    rs = "$rs, $h:$mt";
  }
  return rs;
}

String formaTime1(int? timeStamp,
    {bool withTime = false, bool withYear = false}) {
  if (timeStamp == null || timeStamp == 0) return "00";
  final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
  final mt = date.minute < 10 ? "0${date.minute}" : "${date.minute}";
  final h = date.hour < 10 ? "0${date.hour}" : "${date.hour}";
  return "$h:$mt";
}

//01 th4./no context
String formatDateTime2(int? timeStamp,
    {bool withTime = false, SummaryInterval? interval}) {
  if (interval == SummaryInterval.dayRange) {
    if (timeStamp == null || timeStamp == 0) return "00";
    final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
    var day = '${date.day}';
    if (date.day < 10) {
      day = '0$day';
    }
    var rs = "$day th${date.month}";
    if (withTime) {
      final mt = date.minute < 10 ? "0${date.minute}" : "${date.minute}";
      final h = date.hour < 10 ? "0${date.hour}" : "${date.hour}";
      rs = "$rs, $h:$mt";
    }
    return rs;
  }
  if (interval == SummaryInterval.weekRange) {
    if (timeStamp == null || timeStamp == 0) return "00";
    final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
    final startOfYear = DateTime(date.year, 1, 1, 0, 0);
    final firstMonday = startOfYear.weekday;
    final daysInFirstWeek = 8 - firstMonday;
    final diff = date.difference(startOfYear);
    var weeks = ((diff.inDays - daysInFirstWeek) / 7).ceil();
    return 'W${weeks + 1}';
  }
  if (interval == SummaryInterval.monthRange) {
    if (timeStamp == null || timeStamp == 0) return "00";
    final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
    var day = '${date.day}';
    if (date.day < 10) {
      day = '0$day';
    }
    var rs = "th${date.month},${date.year}";
    return rs;
  }
  if (interval == SummaryInterval.quarterRange) {
    if (timeStamp == null || timeStamp == 0) return "00";
    final date = DateTime.fromMillisecondsSinceEpoch(timeStamp);
    var quater = (date.month + 2) / 3;
    var rs = "Quý ${quater.toInt()},${date.year}";

    return rs;
  }
  return '';
}

//01 th4,2022.
String? kformatDateTimeS(String? timeStamp, BuildContext context) {
  if (timeStamp == null || timeStamp == '0' || double.parse(timeStamp) < 0) {
    return null;
  }
  final date = DateTime.fromMillisecondsSinceEpoch(int.parse(timeStamp));
  var day = date.day;
  final String language =
      EasyLocalization.of(context)?.locale.languageCode ?? "vi";
  if (language == "vi") {
    var rday = '$day';
    if (day < 10) {
      rday = '0$rday';
    }
    return "$rday th${date.month},${date.year}";
  }
  return "${date.day} ${formatGetMonth(date.month)},${date.year}";
}

//
enum SummaryInterval {
  dayRange,
  weekRange,
  monthRange,
  quarterRange,
}

extension SummaryIntervalExt on SummaryInterval {
  String get displayText {
    switch (this) {
      case SummaryInterval.dayRange:
        return 'Ngày';
      case SummaryInterval.weekRange:
        return 'Tuần';
      case SummaryInterval.monthRange:
        return 'Tháng';
      case SummaryInterval.quarterRange:
        return 'Quý';
      default:
        return '';
    }
  }

  String get getValue {
    switch (this) {
      case SummaryInterval.dayRange:
        return 'DAY';
      case SummaryInterval.weekRange:
        return 'WEEK';
      case SummaryInterval.monthRange:
        return 'MONTH';
      case SummaryInterval.quarterRange:
        return 'QUARTER';
      default:
        return '';
    }
  }
}
