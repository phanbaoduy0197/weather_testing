// import 'dart:io';
//
// import 'package:base_entities/basic/colors/base_colors.dart';
// import 'package:base_entities/generated/locale_keys.g.dart';
// import 'package:easy_localization/easy_localization.dart';
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:wechat_assets_picker/wechat_assets_picker.dart';
//
// import 'image_util.dart';
//
// class MyImagePicker {
//   Future<bool> _handlePhotoPermission(
//     BuildContext context, {
//     required VoidCallback showSetting,
//   }) async {
//     final result = await PhotoManager.requestPermissionExtend();
//     if (result != PermissionState.authorized &&
//         result != PermissionState.limited) {
//       showSetting();
//       return false;
//     }
//
//     return true;
//   }
//
//   // Future<bool> _handleCameraPermission(
//   //     BuildContext context, {
//   //       required VoidCallback showSetting,
//   //     }) async {
//   //   final perm = await Permission.camera.request();
//   //   if (perm != PermissionStatus.granted) {
//   //     showSetting();
//   //     return false;
//   //   }
//   //
//   //   return true;
//   // }
//
//   Future<File?> pickImage(
//     BuildContext context,
//     bool mounted, {
//     bool isRotate = true,
//   }) async {
//     final hasPerm = await _handlePhotoPermission(
//       context,
//       showSetting: () => showOpenSettingsPopup(
//         context,
//         "pleaseGrandPhotoPermission".tr(),
//       ),
//     );
//     if (!hasPerm) {
//       return null;
//     }
//
//     try {
//       File? file;
//       if (!mounted) return null;
//       final assets = await AssetPicker.pickAssets(
//         context,
//         pickerConfig: AssetPickerConfig(
//           maxAssets: 1,
//           sortPathDelegate: const CustomSortPathDelegate(),
//           textDelegate: CustomAssetTextDelegate(),
//           requestType: RequestType.image,
//           // specialItemPosition: SpecialItemPosition.prepend,
//           shouldRevertGrid: false,
//           pickerTheme: AssetPicker.themeData(BColors.primaryColor),
//           // allowSpecialItemWhenEmpty: true,
//           // specialItemBuilder: (context, entity, index) {
//           //   return _CameraButton(onPressed: () async {
//           //     file =
//           //     await _showCamera(context, type, mounted, isRotate: isRotate);
//           //     if (file != null) {
//           //       if (!mounted) return;
//           //       Navigator.of(context).pop();
//           //     }
//           //   });
//           // },
//         ),
//       );
//
//       if (assets != null && assets.isNotEmpty) {
//         file = await assets.first.file;
//       }
//
//       if (file != null) {
//         file = await compressImage(file, maxWidth: 2048, maxHeight: 2048);
//       }
//
//       return file;
//     } catch (e, _) {
//       // showToast(e.toString());
//       // Log.e(e, stack);
//       return null;
//     }
//   }
//
//   Future<List<File>?> pickMultiImage(
//     BuildContext context,
//     bool mounted, {
//     bool isRotate = true,
//     int maxLength = 5,
//   }) async {
//     final hasPerm = await _handlePhotoPermission(
//       context,
//       showSetting: () => showOpenSettingsPopup(
//         context,
//         "pleaseGrandPhotoPermission".tr(),
//       ),
//     );
//     if (!hasPerm) {
//       return null;
//     }
//
//     try {
//       List<File> file = [];
//       if (!mounted) return null;
//       final assets = await AssetPicker.pickAssets(
//         context,
//         pickerConfig: AssetPickerConfig(
//           maxAssets: maxLength,
//           sortPathDelegate: const CustomSortPathDelegate(),
//           textDelegate: CustomAssetTextDelegate(),
//           requestType: RequestType.image,
//           // specialItemPosition: SpecialItemPosition.prepend,
//           shouldRevertGrid: false,
//           pickerTheme: AssetPicker.themeData(BColors.primaryColor),
//           // allowSpecialItemWhenEmpty: true,
//           // specialItemBuilder: (context, entity, index) {
//           //   return _CameraButton(onPressed: () async {
//           //     final res = await _showCamera(context, KycImageType.none, mounted,
//           //         isRotate: isRotate);
//           //     if (res != null) {
//           //       file.add(res);
//           //       if (!mounted) return;
//           //       Navigator.of(context).pop();
//           //     }
//           //   });
//           // },
//         ),
//       );
//
//       if (assets != null && assets.isNotEmpty) {
//         final List<File> listFile =
//             (await Future.wait(assets.map((e) async => e.file)))
//                 .where((e) => e != null)
//                 .cast<File>()
//                 .toList();
//         final List<File> listCompress = (await Future.wait(listFile
//             .map((e) => compressImage(e, maxWidth: 2048, maxHeight: 2048))));
//         file.addAll(listCompress);
//       }
//
//       if (file.isNotEmpty == true) {
//         return file;
//       }
//       return null;
//     } catch (e, _) {
//       // showToast(e.toString());
//       // Log.e(e, stack);
//       return null;
//     }
//   }
// }
//
// class CustomSortPathDelegate extends SortPathDelegate<AssetPathEntity> {
//   const CustomSortPathDelegate();
//
//   // @override
//   // void sort(List<AssetPathEntity> list) {
//   //   for (AssetPathEntity entity in list) {
//   //     if (entity.isAll) {
//   //       entity = entity.copyWith(name: "All");
//   //     }
//   //   }
//   //   if (list.any((AssetPathEntity e) => e.lastModified != null)) {
//   //     list.sort((AssetPathEntity path1, AssetPathEntity path2) {
//   //       if (path1.lastModified == null || path2.lastModified == null) {
//   //         return 0;
//   //       }
//   //       if (path2.lastModified!.isAfter(path1.lastModified!)) {
//   //         return 1;
//   //       }
//   //       return -1;
//   //     });
//   //   }
//   //   list.sort((AssetPathEntity path1, AssetPathEntity path2) {
//   //     if (path1.isAll) {
//   //       return -1;
//   //     }
//   //     if (path2.isAll) {
//   //       return 1;
//   //     }
//   //     if (_isCamera(path1)) {
//   //       return -1;
//   //     }
//   //     if (_isCamera(path2)) {
//   //       return 1;
//   //     }
//   //     if (_isScreenShot(path1)) {
//   //       return -1;
//   //     }
//   //     if (_isScreenShot(path2)) {
//   //       return 1;
//   //     }
//   //     return 0;
//   //   });
//   // }
//
//   int otherSort(AssetPathEntity path1, AssetPathEntity path2) {
//     return path1.name.compareTo(path2.name);
//   }
//
//   bool _isCamera(AssetPathEntity entity) {
//     return entity.name == 'Camera';
//   }
//
//   bool _isScreenShot(AssetPathEntity entity) {
//     return entity.name == 'Screenshots' || entity.name == 'Screenshot';
//   }
//
//   @override
//   void sort(List<PathWrapper<AssetPathEntity>> list) {
//     final List<AssetPathEntity> l = list.map((e) => e.path).toList();
//     for (AssetPathEntity entity in l) {
//       if (entity.isAll) {
//         entity = entity.copyWith(name: "All");
//       }
//     }
//     if (l.any((AssetPathEntity e) => e.lastModified != null)) {
//       l.sort((AssetPathEntity path1, AssetPathEntity path2) {
//         if (path1.lastModified == null || path2.lastModified == null) {
//           return 0;
//         }
//         if (path2.lastModified!.isAfter(path1.lastModified!)) {
//           return 1;
//         }
//         return -1;
//       });
//     }
//     l.sort((AssetPathEntity path1, AssetPathEntity path2) {
//       if (path1.isAll) {
//         return -1;
//       }
//       if (path2.isAll) {
//         return 1;
//       }
//       if (_isCamera(path1)) {
//         return -1;
//       }
//       if (_isCamera(path2)) {
//         return 1;
//       }
//       if (_isScreenShot(path1)) {
//         return -1;
//       }
//       if (_isScreenShot(path2)) {
//         return 1;
//       }
//       return 0;
//     });
//   }
// }
//
// class CustomAssetTextDelegate extends AssetPickerTextDelegate {
//   @override
//   String get confirm => "done".tr();
//
//   @override
//   String get cancel => LocaleKeys.cancel.tr();
//
//   @override
//   String get edit => "edit".tr();
//
//   @override
//   String get gifIndicator => 'GIF';
//
//   // @override
//   // String get heicNotSupported => LocaleKeys.heicNotSupported.tr();
//
//   @override
//   String loadFailed = "loadFailed".tr();
//
//   @override
//   String get original => "original".tr();
//
//   @override
//   String get preview => "preview".tr();
//
//   @override
//   String get select => "select".tr();
//
//   @override
//   String get emptyList => "emptyList".tr();
//
//   @override
//   String get unSupportedAssetType => "unSupportedAssetType".tr();
//
//   @override
//   String get unableToAccessAll => "unableToAccessAll".tr();
//
//   @override
//   String get viewingLimitedAssetsTip => "viewingLimitedAssetsTip".tr();
//
//   @override
//   String get changeAccessibleLimitedAssets =>
//       "changeAccessibleLimitedAssets".tr();
//
//   @override
//   String get accessAllTip => "accessAllTip".tr();
//
//   @override
//   String get goToSystemSettings => "openSettings".tr();
//
//   @override
//   String get accessLimitedAssets => "accessLimitedAssets".tr();
//
//   @override
//   String get accessiblePathName => "accessiblePathName".tr();
// }
//
// Future showOpenSettingsPopup(BuildContext context, String message) async {
//   // Apple review: không hỏi lại xin quyền
//   return Fluttertoast.showToast(msg: message);
//   // if (Platform.isIOS) {
//   //   return showToastMsg(message);
//   // } else {
//   //   return showDialog(
//   //       context: context,
//   //       builder: (context) => DefaultPopupWidget(
//   //             content: message,
//   //             title: "Lỗi truy cập",
//   //             selectConfirm: () async {
//   //               Navigator.maybePop(context);
//   //               openAppSettings();
//   //             },
//   //           ));
//   // }
// }
