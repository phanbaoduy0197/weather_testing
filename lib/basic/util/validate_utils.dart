bool? validateEmail(String? value) {
  if (value == null || value.isEmpty) return false;
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(value);
}

bool validatePassword(String password) {
  return password.isNotEmpty && password.length >= 6;
}

bool? validatePhone(String? value) {
  if (value?.isEmpty == true || value == null) return false;
  String pattern =
      r'^((09[0|1|2|3|4|6|7|8|9]|08[1|2|3|4|5|6|8|9]|07[0|6|7|8|9]|05[2|6|8|9]|03[2|3|4|5|6|7|8|9])+([0-9]{7})|(0101+([0-9]{6}))|([0-9]{11}))$';
  RegExp regex = RegExp(pattern);
  final res = regex.hasMatch(value);
  return res;
}

bool? validatePhoneVN(String? value) {
  if (value?.isEmpty == true || value == null) return false;
  String pattern = r'^(\+84[1-9]{1}|84[1-9]{1}|0[3|5|7|8|9])+([0-9]{8})\b';
  // String pattern = r'^(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})$';
  RegExp regex = RegExp(pattern);
  final res = regex.hasMatch(value);
  return res;
}

bool? validateIsNumber(String? value) {
  if (value?.isEmpty == true || value == null) return false;
  String pattern = r'^[0-9]+$';
  // String pattern = r'^(((\+|)84)|0)(3|5|7|8|9)+([0-9]{8})$';
  RegExp regex = RegExp(pattern);
  final res = regex.hasMatch(value);
  return res;
}

bool? validateLinks(String? link) {
  if (link?.isEmpty == true || link == null) return false;
  String pattern = r'(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-?=%.]+';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(link);
}

// bool validateComment(String value) {
//   return value.isNotEmpty && value.length < 201;
// }

// bool? validateName(String? name) {
//   if (name == null || name.isEmpty) return null;
//   return name.trim().length > 0 && name.length > 1;
// }

// bool validateUsernameKeyId(String value) {
//   if (value.isEmpty) return false;
//   String pattern = r'^[a-z][a-z0-9]{5,9}$';
//   RegExp regex = new RegExp(pattern);
//   return regex.hasMatch(value);
// }

bool validateIdCard(String? value, {bool isAlwaysTrue = false}) {
  if (isAlwaysTrue) {
    if (value != null && value.trim().isNotEmpty) {
      return true;
    }
    return false;
  }
  if (value == null || value.isEmpty) return false;
  String pattern = r'^(([0-9]{9}|[0-9]{12})|([0-9]{9}, [0-9]{12}))$';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(value);
}

bool validateNotSpecialCharacter(String? value) {
  if (value == null || value.isEmpty) return false;
  String pattern = r'^([a-zA-Z0-9]{7,})+$';
  RegExp regex = RegExp(pattern);
  return regex.hasMatch(value);
}
