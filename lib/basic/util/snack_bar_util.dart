import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:flutter/material.dart';

showSnackBar(
  BuildContext context,
  String? msg, {
  bool isShowIconLeft = true,
  Widget? iconLeft,
  Widget? iconRight,
  bool isShowIconRight = false,
}) {
  final snackBar = SnackBar(
    behavior: SnackBarBehavior.floating,
    backgroundColor: getColor().textBase,
    padding: const EdgeInsets.only(top: 14, bottom: 18, left: 16, right: 16),
    duration: const Duration(milliseconds: 2000),
    content: Row(
      children: [
        if (isShowIconLeft)
          iconLeft ??
              Icon(
                Icons.error,
                size: 24,
                color: getColor().error,
              ),
        if (isShowIconLeft)
          const SizedBox(
            width: 10,
          ),
        Expanded(
          child: Text(
            msg ?? "",
            style: context.tt.body2.white,
          ),
        ),
        if (isShowIconRight)
          const SizedBox(
            width: 10,
          ),
        if (isShowIconRight) iconRight ?? const SizedBox(),
      ],
    ),
  );
  ScaffoldMessenger.of(context).clearSnackBars();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}

extension ContextSnackBarExt on BuildContext {
  snackBarSuccess(String? msg) {
    showSnackBar(
      this,
      msg,
      iconLeft: Icon(
        Icons.check_circle,
        size: 24,
        color: getColor().white,
      ),
    );
  }

  snackBarError(String? msg) {
    showSnackBar(
      this,
      msg,
    );
  }
}
