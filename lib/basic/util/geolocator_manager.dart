// ignore_for_file: use_build_context_synchronously

import 'package:base_entities/module/widget/widgets.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

class GeolocatorManager {
  late final GeolocatorPlatform geolocatorPlatform =
      GeolocatorPlatform.instance;
  bool hasPermission = false;
  Future<bool> handlePermission(BuildContext context) async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await geolocatorPlatform.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return false;
    }

    permission = await geolocatorPlatform.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await geolocatorPlatform.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.

        return false;
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      final res = await RAlertDialog.confirm(
        context: context,
        title: "Thông báo",
        message: "Đã bị chặn từ hệ thống. Nhấn tiếp tục để vào cài đặt",
      );
      if (res == true) {
        openAppSettings();
      }
      return false;
    }
    hasPermission = true;

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return true;
  }

  Future<Position?> getCurrentPosition(BuildContext context) async {
    final hasPermission = await handlePermission(context);

    if (!hasPermission) {
      return null;
    }

    return geolocatorPlatform.getCurrentPosition();
  }
}
