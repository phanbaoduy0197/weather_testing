class DImages {
  static String get cloud => "assets/images/img_cloud.png";
  static String get cloudy => "assets/images/img_cloudy.png";
  static String get cloudyRain => "assets/images/img_cloudy_rain.png";
  static String get homeDiscover => "assets/images/img_home_discover.png";
  static String get homeDiscover2 => "assets/images/img_home_discover_2.png";
  static String get raining => "assets/images/img_raining.png";
  static String get storm => "assets/images/img_storm.png";
  static String get sun => "assets/images/img_sun.png";
  static String get userCircle => "assets/images/img_user_circle.png";
  static String get userCircleBold => "assets/images/img_user_circle_bold.png";
}
