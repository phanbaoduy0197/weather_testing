import 'package:flutter/material.dart';

const dateTimeFormat = "yyyy-MM-dd'T'HH:mm:ssZ";
const dateFormat = "dd/MM/yyyy";
const timeFormat = "HH:mm";
final minDate = DateTime(1900, 1, 1, 0, 0, 0);
final maxDate = DateTime(3000, 1, 1, 0, 0, 0);
const minYearOldUsedApp = 18;
typedef WidgetBuilder1<T> = Widget Function(T);
typedef Maker<T> = T Function();
typedef Maker1<T, P> = T Function(P);
typedef Maker2<T, P1, P2> = T Function(P1, P2);
