import 'dart:convert';

import 'package:base_entities/basic/misc/logger.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

const _doubleFormat = "#,##0.##";
const _intFormat = "#,##0";

extension MapJsonExt on Map {
  String toJsonString() => jsonEncode(this);
}

extension StringExt on String {
  dynamic toJsonObject() => jsonDecode(this);

  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1)}";
  }

  String parseIcon() {
    debugPrint("http://openweathermap.org/img/w/$this.png");
    return "http://openweathermap.org/img/w/$this.png";
  }

  bool isWeather() {
    if (this == "thanh_pho_ha_noi" || this == "thanh_pho_ho_chi_minh") {
      return true;
    }
    return false;
  }

  String configCity() {
    if (this == "thanh_pho_ha_noi") {
      return "Hanoi";
    } else if (this == "thanh_pho_ho_chi_minh") {
      return "Ho Chi Minh City";
    }
    return this;
  }
}

extension NumExt on num? {
  num? checkZero() {
    if (this == 0) {
      return null;
    }
    return this;
  }

  String? formatInt({String format = _intFormat, String? locale}) {
    try {
      if (this == 0) return "0";
      final format = NumberFormat(_intFormat, locale);
      return format.format(this);
    } catch (err, stack) {
      Log.e("unable to format string int $this", err, stack);
    }

    return null;
  }

  String formatDouble({String format = _doubleFormat, String? locale}) {
    try {
      final nf = NumberFormat(format, locale);
      return nf.format(this);
    } catch (err, stack) {
      Log.e("unable to format string double $this", err, stack);
    }

    return "0";
  }
}

extension BuildContextExt on BuildContext {
  double get w => MediaQuery.of(this).size.width;
  double get h => MediaQuery.of(this).size.height;
}
