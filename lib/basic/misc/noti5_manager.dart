import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:base_entities/app/app_entities/noti.dart';
import 'package:base_entities/basic/misc/cache_manager.dart';
import 'package:base_entities/basic/misc/disposable.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/di/locator.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/rxdart.dart';

class Noti5Manager implements Disposable {
  static const permDenied = -1;
  static const permNotDetermined = 0;
  static const permAuthorized = 1;

  FirebaseMessaging get firebaseMessaging => FirebaseMessaging.instance;
  final unseenCount = BehaviorSubject<int>.seeded(0);

  final locaNoti5Plugin = FlutterLocalNotificationsPlugin();
  // NotificationRepository get noti5Repo => locator<NotificationRepository>();
  RCacheManager get cacheMgr => locator<RCacheManager>();
  final onHandleNoti5 = PublishSubject<RNotificationData>();
  final _subs = <StreamSubscription>[];
  final androidChannel = const AndroidNotificationChannel(
    'rever_agents_channel', // id
    'Rever Notifications', // title
    description: 'This channel is used for notifications.', // description
    importance: Importance.max,
  );

  Noti5Manager() {
    _subs.addAll([
      firebaseMessaging.onTokenRefresh
          .distinct()
          .skip(1)
          .listen((token) => _onToken(token)),
      FirebaseMessaging.onMessage.listen(
        onMessage,
      ),
      FirebaseMessaging.onMessageOpenedApp.listen(onMessageOpenApp),
    ]);
  }

  @override
  void dispose() {
    unseenCount.close();
    for (var e in _subs) {
      e.cancel();
    }
  }

  Future init() async {
    _checkPermission();
  }

  void _checkPermission() async {
    _setupPushNotif();
  }

  void _onToken(String token) {
    submitToken(token);
  }

  Future<bool> requestPermission(BuildContext context,
      {required VoidCallback showSetting}) async {
    if (Platform.isIOS) {
      final settings = await firebaseMessaging.requestPermission();

      if (settings.authorizationStatus == AuthorizationStatus.denied) {
        showSetting();
      }

      if (settings.authorizationStatus == AuthorizationStatus.authorized) {
        await _setupPushNotif();
        updateUnseenCount();
        return true;
      }
    } else if (Platform.isAndroid) {
      final settings = await firebaseMessaging.requestPermission();
      if (settings.authorizationStatus != AuthorizationStatus.denied) {
        await _setupPushNotif();
        return true;
      }
    }

    return false;
  }

  void submitToken([String? token]) async {
    // ...
    token ??= await firebaseMessaging.getToken();
    Log.i("submit pns token: $token");
    try {
      if (token == null) return;
      // await noti5Repo.submitFCMToken(token, Platform.isIOS ? "ios" : "android");
    } catch (e, st) {
      Log.e(e, st);
    }
  }

  void clearToken() async {
    try {
      final token = await firebaseMessaging.getToken();
      if (token != null) {
        // await noti5Repo.deleteFCMToken(token);
      }
    } catch (e, st) {
      Log.e(e, st);
    }
  }

  Future _setupPushNotif() async {
    const androidSettings = AndroidInitializationSettings('ic_notif');
    const DarwinInitializationSettings initializationSettingsDarwin =
        DarwinInitializationSettings();

    const settings = InitializationSettings(
      android: androidSettings,
      iOS: initializationSettingsDarwin,
    );

    await locaNoti5Plugin.initialize(settings,
        onDidReceiveNotificationResponse: (noti) async {
      Log.i("handle payload: $noti");
      if (noti.payload?.isNotEmpty == true) {
        final json = jsonDecode(noti.payload!);
        final data = RNotificationData.fromJson(json);
        onHandleNoti5.add(data);
      }
    });

    //trên iOS cần config cái này để hiện foreground
    if (Platform.isIOS) {
      await firebaseMessaging.setForegroundNotificationPresentationOptions(
        alert: true,
        badge: true,
        sound: true,
      );
    }
  }

  // do cái widget init sau, nên khi nó init sẽ gọi method này để lấy
  static dynamic handleMsg;
  void fetchInitialUri() async {
    final initialMsg = await firebaseMessaging.getInitialMessage();
    if (initialMsg != null && handleMsg != null) {
      Log.i("got initial message");
      handleMsg = initialMsg;
      onMessageOpenApp(initialMsg);
    }
  }

  Future onMessage(RemoteMessage message) async {
    Log.i("did receive message: ${message.messageId}");

    RNotificationData data;
    try {
      data = RNotificationData.fromJson(message.data);
      final extra = data.extraDataObject;
      // kết quả KYC không cần hiện
      if (extra is RN5KycResultExtraData) {
        onHandleNoti5.add(data);
        return;
      }
    } catch (e, s) {
      Log.e("unable to parse pn5", e, s);
      return;
    }

    if (Platform.isAndroid) {
      //hiện noti5 khi foreground trên android
      RemoteNotification? notification = message.notification;
      AndroidNotification? android = message.notification?.android;

      // If `onMessage` is triggered with a r_notification, construct our own
      // local r_notification to show to users using the created channel.
      if (notification != null && android != null) {
        AndroidBitmap<Object>? largeIcon;
        String payload;
        if (android.imageUrl?.isNotEmpty == true) {
          final file = await cacheMgr.getSingleFile(android.imageUrl!);
          largeIcon = FilePathAndroidBitmap(file.path);
        }

        payload = jsonEncode(message.data);
        Log.i("show local noti5 ${notification.title}");
        locaNoti5Plugin.show(
          message.data["id"]?.hashCode ?? notification.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
            android: AndroidNotificationDetails(
              androidChannel.id,
              androidChannel.name,
              channelDescription: androidChannel.description,
              icon: android.smallIcon,
              largeIcon: largeIcon,
            ),
          ),
          payload: payload,
        );
      }
    }
  }

  Future updateUnseenCount() async {
    int count = 0;
    // count = await locator<NotificationRepository>().countUnseenNoti5();
    if (unseenCount.isClosed) return;

    unseenCount.sink.add(count);
  }

  void onMessageOpenApp(RemoteMessage message) {
    Log.i("did touch message: ${message.messageId} ${message.data}");
    try {
      final data = RNotificationData.fromJson(message.data);
      onHandleNoti5.add(data);
    } catch (e, s) {
      Log.e("unable to handle pn5", e, s);
    }
  }
}

// Future showOpenSettingsPopup(BuildContext context) async {
//   return await showDialog(
//       context: context,
//       builder: (context) => DefaultPopupWidget(
//             content:
//                 "Không có quyền truy cập, vui lòng vào Cài đặt để thay đổi",
//             title: "Thông báo",
//             selectConfirm: () async {
//               Navigator.maybePop(context);
//               await openAppSettings();
//             },
//           ));
// }
