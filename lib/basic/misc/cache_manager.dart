import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/module/auth/model/session.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

///
/// Class này để:
/// - silence error trong Cached Network Image (Khi chạy integrationt test sẽ làm test fail)
/// - Thêm auth vào header
///
class RCacheManager extends CacheManager {
  Session? session;
  RCacheManager() : super(Config(DefaultCacheManager.key));

  @override
  Stream<FileResponse> getFileStream(String url,
      {String? key,
      Map<String, String>? headers,
      bool withProgress = false}) async* {
    if (session?.value != null) {
      headers ??= <String, String>{};
      headers.putIfAbsent("Authorization", () => "Bearer ${session!.value}");
    }

    var stream = super
        .getFileStream(url,
            key: key, headers: headers, withProgress: withProgress)
        .handleError((Object obj) {
      Log.w(obj);
      return null;
    });

    try {
      await for (var value in stream) {
        yield value;
      }
    } catch (ex) {
      Log.w(ex);
    }
  }

  @override
  Future<FileInfo> downloadFile(String url,
      {String? key,
      Map<String, String>? authHeaders,
      bool force = false}) async {
    if (session?.value != null) {
      authHeaders ??= <String, String>{};
      authHeaders.putIfAbsent(
          "Authorization", () => "Bearer ${session!.value}");
    }

    return super
        .downloadFile(url, key: key, authHeaders: authHeaders, force: force);
  }
}
