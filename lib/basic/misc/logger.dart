import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:logging/logging.dart';
import 'package:flutter/foundation.dart';

typedef OnLogData = void Function(Level level, Object message,
    [Object? error, StackTrace? stackTrace]);

class Log {
  static late Log _instance;
  Logger logger = Logger.root;
  OnLogData? onLogData;

  Log._debug() : this._(level: Level.FINE, onLogData: _nullLog);
  Log._error() : this._(level: Level.SEVERE, onLogData: _crashlyticsLog);

  Log._({String name = "Logger", Level level = Level.OFF, this.onLogData}) {
    Logger.root.onRecord.listen((record) {
      debugPrint('[${record.level.name.toUpperCase()}] ${record.message}');
      if (record.error != null) {
        debugPrint("${record.error}");
      }
      if (record.stackTrace != null) {
        debugPrint("${record.stackTrace}");
      }
    });
    logger.level = level;
  }

  static void init() {
    _instance = kDebugMode ? Log._debug() : Log._error();
  }

  static void setLevel(Level level) {
    _instance.logger.level = level;
  }

  static void i(message, [Object? error, StackTrace? stackTrace]) {
    final loc = _getLogLocation();

    _instance.logger.info("${loc[0]}: $message${loc[1]}", error, stackTrace);
    _instance.onLogData!(Level.INFO, message, error, stackTrace);
  }

  static void w(message, [Object? error, StackTrace? stackTrace]) {
    final loc = _getLogLocation();

    _instance.logger.warning("${loc[0]}: $message${loc[1]}", error, stackTrace);
    _instance.onLogData!(Level.WARNING, message, error, stackTrace);
  }

  static void d1(message, [Object? error, StackTrace? stackTrace]) {
    final loc = _getLogLocation();

    _instance.logger.finer("${loc[0]}: $message${loc[1]}", error, stackTrace);
    _instance.onLogData!(Level.FINER, message, error, stackTrace);
  }

  static void d(message, [Object? error, StackTrace? stackTrace]) {
    final loc = _getLogLocation();

    _instance.logger.fine("${loc[0]}: $message${loc[1]}", error, stackTrace);
    _instance.onLogData!(Level.FINE, message, error, stackTrace);
  }

  static void e(message, [Object? error, StackTrace? stackTrace]) {
    final loc = _getLogLocation();

    _instance.logger.severe("${loc[0]}: $message${loc[1]}", error, stackTrace);
    _instance.onLogData!(Level.SEVERE, message, error, stackTrace);
  }

  /// Get login line location oin source code
  ///
  /// Return array has 2 elements:
  ///   1st: method name
  ///   2nd: file & line location
  static List<String> _getLogLocation() {
    final regexCodeLine = RegExp(r"#2\s+([^\s]+).+(\(package:.+\))");
    final match = regexCodeLine.firstMatch(StackTrace.current.toString());
    if (match?.groupCount == 2) {
      return [match!.group(1)!, match.group(2)!];
    } else if (match?.groupCount == 1) {
      return [match!.group(1)!, ""];
    }
    return ["unknown", ""];
  }
}

void _nullLog(Level level, Object message,
    [Object? error, StackTrace? stackTrace]) {}

void _crashlyticsLog(Level level, Object message,
    [Object? error, Object? stackTrace]) {
  if (level >= Level.WARNING) {
    FirebaseCrashlytics.instance.log("[${level.name}] $message");
    if (error != null) {
      FirebaseCrashlytics.instance.log(error.toString());
    }

    if (stackTrace != null) {
      FirebaseCrashlytics.instance.log(stackTrace.toString());
    }
  }
}
