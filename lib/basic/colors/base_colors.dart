import 'package:flutter/material.dart';

class BColors {
  static const primaryColor = Color.fromRGBO(204, 0, 255, 1);
  static const textBase = Color(0xFF000000);
  static const white = Color(0xFFFFFFFF);
  static const error = Color(0xFFD61F00);
  static const greyLighter = Color(0xFFCDD1D4);
  static const greyLighter50 = Color(0x80CDD1D4);
  static const greyLightest = Color(0xFFF5F5F7);
  static const unSelectedButton = Color(0xFFE39294);
  static const greyBase = Color(0xFF5F666D);
  static const greyLight = Color(0xFF9FA4AA);
  static const greyLight50 = Color(0x809FA4AA);
  static const lightShadow = Color(0xFF435664);
  static const primaryLightest = Color(0x1AC72528);
  static const textBase04 = Color(0x04131920);
  static const textBase05 = Color(0x0d131920);
  static const textBase12 = Color(0x1F131920);
  static const textBase30 = Color(0x4C131920);
  static const textBase50 = Color(0x80131920);
  static const textBase66 = Color(0x66131920);
  static const shadowSoft = Color(0x0A435664);
  static const border = Color(0xffe6e8ea);
  static const blueBold = Color(0xff003399);
}
