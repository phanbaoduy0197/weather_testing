import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/di/locator.dart';
import 'package:flutter/material.dart';

String font = kFontFamily;

class FontManager with ChangeNotifier {
  init() async {
    // We load theme at the start
    _loadFont();
  }

  /// Use this method on UI to get selected theme.

  void _loadFont() async {
    final preferredFont = await locator<CommonRepository>().getFont();
    font = preferredFont;
    // Once theme is loaded - notify listeners to update UI
    notifyListeners();
  }

  changeFont(String newFont) {
    font = newFont;
    notifyListeners();
    locator<CommonRepository>().setFont(newFont);
  }
}
