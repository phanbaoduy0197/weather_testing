import 'package:base_entities/data/repository/common_repository.dart';
import 'package:flutter/material.dart';

import '../../di/locator.dart';
import '../colors/base_colors.dart';
import 'text_theme.dart';

enum AppTheme { appThemeWhite, appThemeDark }

/// Returns enum value name without enum class name.
String enumName(AppTheme anyEnum) {
  return anyEnum.toString().split('.')[1];
}

final appThemeData = {
  AppTheme.appThemeWhite: ThemeData(
    useMaterial3: true,
    colorSchemeSeed: BColors.primaryColor,
    textTheme: createTextTheme(),
  ),
  AppTheme.appThemeDark: ThemeData(
    useMaterial3: true,
    colorSchemeSeed: BColors.primaryColor,
    brightness: Brightness.dark,
    //--- chinh sua text mau trang o day
    textTheme: createTextTheme(),
  ),
};

class ThemeManager with ChangeNotifier {
  ThemeData? _themeData;

  init() async {
    // We load theme at the start
    _loadTheme();
  }

  /// Use this method on UI to get selected theme.
  ThemeData? get themeData {
    _themeData ??= appThemeData[AppTheme.appThemeWhite];
    return _themeData;
  }

  void _loadTheme() async {
    final preferredTheme = await locator<CommonRepository>().getTheme();
    currentAppTheme = AppTheme.values[preferredTheme];
    _themeData = appThemeData[currentAppTheme];
    // Once theme is loaded - notify listeners to update UI
    notifyListeners();
  }

  /// Sets theme and notifies listeners about change.
  setTheme(AppTheme theme) async {
    currentAppTheme = theme;
    _themeData = appThemeData[theme];

    // Here we notify listeners that theme changed
    // so UI have to be rebuild
    notifyListeners();
    await locator<CommonRepository>().setTheme(AppTheme.values.indexOf(theme));
  }
}

AppTheme currentAppTheme = AppTheme.appThemeWhite;

ColorScheme getColor() => locator<ThemeManager>().themeData!.colorScheme;

extension MyColorScheme on ColorScheme {
  Color getColorTheme(Color colorThemeWhite, Color colorThemeDark) {
    switch (currentAppTheme) {
      case AppTheme.appThemeWhite:
        return colorThemeWhite;
      case AppTheme.appThemeDark:
        return colorThemeDark;
      default:
        return colorThemeWhite;
    }
  }

  static const List<Color> _kDarkColors = [
    Color.fromRGBO(199, 37, 40, 1),
    Color(0xFF30333A),
    Color(0xFFFF5D51),
    Color(0xFF2C81E4),
    Color(0xFF243C28),
    Color(0xFF5901D3),
    Color(0xFF767F27),
    Color(0xFF00A6BD),
    Color(0xFF613E36),
    Color(0xFF9D545D),
    Color(0xFF4CAA14),
    Color(0xFF726AA2),
    Color(0xFF4B4B1A),
    Color(0xFF5D853F),
    Color(0xFFAE3D2B),
  ];

  List<Color> get listColor => const [
        Color(0xFFC72528),
        Color(0xFFFF8A1F),
        Color(0xFFEFAC00),
        Color(0xFF03AED4),
        Color(0xFF24A148),
        Color(0xFF131920),
      ];

  List<Color> get listBgColor => const [
        Color(0xFFFCF4F4),
        Color(0xFFFEF7F1),
        Color(0xFFFCF7E9),
        Color(0xFFF3FDFF),
        Color(0xFFF0F8F2),
        Color(0xFFF2F4F8),
      ];

  Color getRandomDarkColor() {
    return (_kDarkColors.toList()..shuffle()).first;
  }

  Color get primaryColor => getColorTheme(
        BColors.primaryColor,
        BColors.textBase,
      );

  Color get blueBold => getColorTheme(
        BColors.blueBold,
        BColors.blueBold,
      );

  Color get error => getColorTheme(
        BColors.error,
        BColors.error,
      );

  Color get white => getColorTheme(
        BColors.white,
        BColors.white,
      );

  Color get textBase => getColorTheme(
        BColors.textBase,
        BColors.textBase,
      );

  Color get randomDarkColor => getRandomDarkColor();

  ///
  /// Trả về màu cho 1 string, bảo đảm 1 string thì luôn trả về 1 màu
  Color getDarkColorForString(String str) {
    int total = str.characters.fold<int>(
        0, (previousValue, element) => previousValue + element.codeUnitAt(0));

    return _kDarkColors[total % _kDarkColors.length];
  }
}
