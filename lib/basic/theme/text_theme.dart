import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/theme/font_manager.dart';
import 'package:flutter/material.dart';
import 'theme_manager.dart';

const kFontFamily = "roboto";
const kFontLOKICOLA = "LOKICOLA";
const kFontRoyalacidO = "Royalacid_o";
const kFontAardc = "AARDC___";
const kFontFreebsca = "FREEBSCA";

class TextSizes {
  static const small = 11.0;
  static const caption = 12.0;
  static const subText = 14.0;
  static const body = 17.0;
  static const heading4 = 15.0;
  static const heading3 = 17.0;
  static const heading2 = 22.0;
  static const heading1 = 34.0;
  static const heading5 = 24.0;
  static const text10 = 10.0;
  static const text12 = 12.0;
  static const text13 = 13.0;
  static const text14 = 14.0;
  static const text15 = 15.0;
  static const text16 = 16.0;
  static const text17 = 17.0;
  static const text18 = 18.0;
  static const text19 = 19.0;
  static const text20 = 20.0;
  static const text21 = 21.0;
  static const text22 = 22.0;
  static const text23 = 23.0;
  static const text24 = 24.0;
  static const text25 = 25.0;
  static const text26 = 26.0;
  static const text27 = 27.0;
  static const text28 = 28.0;
  static const text29 = 29.0;
  static const text30 = 30.0;
  static const heading6 = 20.0;
  static const primaryButton = 18.0;
}

extension TextThemeExt on TextTheme {
  TextStyle get titleH1 => AppTextStyles.titleH1.copyWith(fontFamily: font);
  TextStyle get h2 => AppTextStyles.h2.copyWith(fontFamily: font);
  TextStyle get h3 => AppTextStyles.h3.copyWith(fontFamily: font);
  TextStyle get h4 => AppTextStyles.h4.copyWith(fontFamily: font);
  TextStyle get sub1 => AppTextStyles.subtitle1.copyWith(fontFamily: font);
  TextStyle get sub2 => AppTextStyles.subtitle2.copyWith(fontFamily: font);
  TextStyle get body1 => AppTextStyles.body1.copyWith(fontFamily: font);
  TextStyle get body2 => AppTextStyles.body2.copyWith(fontFamily: font);
  TextStyle get small => AppTextStyles.small.copyWith(fontFamily: font);
  TextStyle get preTitle => AppTextStyles.preTitle.copyWith(fontFamily: font);
  TextStyle get button => AppTextStyles.button.copyWith(fontFamily: font);
  TextStyle get tableHeading =>
      AppTextStyles.tableHeading.copyWith(fontFamily: font);
  TextStyle get tableCell => AppTextStyles.tableCell.copyWith(fontFamily: font);
}

extension TextStyleExt on TextStyle {
  TextStyle get normal => copyWith(fontWeight: FontWeight.normal);
  TextStyle get light => copyWith(fontWeight: FontWeight.w300);
  TextStyle get bold => copyWith(fontWeight: FontWeight.bold);
  TextStyle get medium => copyWith(fontWeight: FontWeight.w500);
  TextStyle get semiBold => copyWith(fontWeight: FontWeight.w600);
  TextStyle get captionNormal => copyWith(fontWeight: FontWeight.normal);
  TextStyle get underLine => copyWith(
        decoration: TextDecoration.underline,
      );

  //height style
  TextStyle get height60Per => copyWith(height: 1.6);
  TextStyle get height24Per => copyWith(height: 1.24);
  TextStyle get height45Per => copyWith(height: 1.45);
  TextStyle get height28Per => copyWith(height: 1.28);
  TextStyle get height40Per => copyWith(height: 1.4);
  TextStyle get height33Per => copyWith(height: 1.33);
  TextStyle get height50Per => copyWith(height: 1.5);
  TextStyle get height20Per => copyWith(height: 1.2);
  TextStyle get height38Per => copyWith(height: 1.38);
  TextStyle get height15Per => copyWith(height: 1.15);
  TextStyle get height10Per => copyWith(height: 1.1);
  TextStyle get height00Per => copyWith(height: 1.0);

  // colors
  TextStyle get primaryColor => copyWith(color: getColor().primaryColor);
  TextStyle get colorTransparent => copyWith(color: Colors.transparent);
  TextStyle get error => copyWith(color: getColor().error);
  TextStyle get textBase => copyWith(color: getColor().textBase);
  TextStyle get white => copyWith(color: getColor().white);
  TextStyle get greyLight => copyWith(color: BColors.greyLight);
  TextStyle get grey => copyWith(color: BColors.greyBase);
  TextStyle get blueBold => copyWith(color: getColor().blueBold);

  //Font-------
  TextStyle get fontFamily => copyWith(fontFamily: kFontFamily);
  TextStyle get fontLOKICOLA => copyWith(fontFamily: kFontLOKICOLA);
  TextStyle get fontRoyalacidO => copyWith(fontFamily: kFontRoyalacidO);
  TextStyle get fontAardc => copyWith(fontFamily: kFontAardc);
  TextStyle get fontFreebsca => copyWith(fontFamily: kFontFreebsca);
}

TextTheme createTextTheme() => TextTheme(
      titleLarge: AppTextStyles.subtitle1.copyWith(fontFamily: font),
      titleMedium: AppTextStyles.subtitle2.copyWith(fontFamily: font),
      titleSmall: AppTextStyles.preTitle.copyWith(fontFamily: font),
      bodySmall: AppTextStyles.small.copyWith(fontFamily: font),
      bodyLarge: AppTextStyles.body1.copyWith(fontFamily: font),
      bodyMedium: AppTextStyles.body2.copyWith(fontFamily: font),
      headlineSmall: TextStyle(
        fontFamily: font,
        fontWeight: FontWeight.w500, //medium
        fontSize: 13.0,
        height: 1.38,
        color: BColors.textBase,
      ),
      headlineMedium: TextStyle(
        fontFamily: font,
        fontWeight: FontWeight.w500, //medium
        fontSize: 16.0,
        height: 1.4444,
        color: BColors.textBase,
      ),
      headlineLarge: AppTextStyles.h4.copyWith(fontFamily: font),
      displaySmall: AppTextStyles.h3.copyWith(fontFamily: font),
      displayMedium: AppTextStyles.h2.copyWith(fontFamily: font),
      displayLarge: AppTextStyles.titleH1.copyWith(fontFamily: font),
    );

TextTheme textTheme(BuildContext context) {
  return Theme.of(context).textTheme;
}

TextTheme primaryTextTheme(BuildContext context) {
  return Theme.of(context).primaryTextTheme;
}

extension BuildContextTextTheme on BuildContext {
  TextTheme get textTheme => Theme.of(this).textTheme;
}

class AppTextStyles {
  static const titleH1 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 31,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: -0.62,
  );

  static const h2 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 25,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const h3 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 20,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const h4 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 18,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const subtitle1 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 16,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const subtitle2 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 14,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const body1 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 16,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const body2 = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 14,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const small = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 13,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const preTitle = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 13,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const button = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 16,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const tableHeading = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 14,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  static const tableCell = TextStyle(
    fontFamily: kFontFamily,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 14,
    color: BColors.textBase,
    decoration: TextDecoration.none,
    letterSpacing: 0,
  );

  AppTextStyles._();
}

extension TextThemeContextExt on BuildContext {
  TextTheme get tt => textTheme(this);
}
