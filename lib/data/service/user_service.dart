import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/data/client/api_constant.dart';
import 'package:base_entities/data/service/base_service.dart';

class UserService extends BaseService {
  Future<User> getMyProfile() async {
    final response = await get(apiGetMyProfile);
    return User.fromJson(response);
  }

  Future<void> logout() async {
    // await post(apiLogout);
  }
}
