import 'package:dio/dio.dart' as dio;
import 'package:dio/dio.dart';
import '../client/rest_client.dart';

abstract class BaseService {
  //-------------API--------------

  Future<dynamic> get(
    String path, {
    Map<String, dynamic>? params,
    TypeURL typeUrl = TypeURL.baseUrl,
    Function(int, int)? onReceiveProgress,
    Options? options,
  }) async {
    final response = await RestClient.getDio(typeUrl: typeUrl).get(
      path,
      queryParameters: params,
      options: options,
      onReceiveProgress: onReceiveProgress,
    );
    return _handleResponse(response);
  }

  Future<dynamic> post(
    String path, {
    data,
    Map<String, dynamic>? queryParameters,
    TypeURL typeUrl = TypeURL.baseUrl,
    int? timeOutMillisecond,
    Function(int, int)? onSendProgress,
    Function(int, int)? onReceiveProgress,
  }) async {
    final response = await RestClient.getDio(
      typeUrl: typeUrl,
      timeOutMillisecond: timeOutMillisecond,
    ).post(
      path,
      data: data,
      queryParameters: queryParameters,
      onSendProgress: onSendProgress,
      onReceiveProgress: onReceiveProgress,
    );
    return _handleResponse(response);
  }

  Future<dynamic> put(
    String path, {
    data,
    TypeURL typeUrl = TypeURL.baseUrl,
    Map<String, dynamic>? queryParameters,
    Function(int, int)? onSendProgress,
    Function(int, int)? onReceiveProgress,
    int? timeOutMillisecond,
  }) async {
    final response = await RestClient.getDio(
      typeUrl: typeUrl,
      timeOutMillisecond: timeOutMillisecond,
    ).put(
      path,
      data: data,
      queryParameters: queryParameters,
      onReceiveProgress: onReceiveProgress,
      onSendProgress: onSendProgress,
    );
    return _handleResponse(response);
  }

  Future<dynamic> delete(
    String path, {
    data,
    Map<String, dynamic>? queryParameters,
    TypeURL typeUrl = TypeURL.baseUrl,
    int? timeOutMillisecond,
  }) async {
    final response = await RestClient.getDio(
      typeUrl: typeUrl,
      timeOutMillisecond: timeOutMillisecond,
    ).delete(
      path,
      queryParameters: queryParameters,
      data: data,
    );
    return _handleResponse(response);
  }

  dynamic _handleResponse(dio.Response response) {
    // if (response.data is! Map) return response.data;
    return response.data;
  }
}
