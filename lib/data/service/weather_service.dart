import 'package:base_entities/data/service/base_service.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:weather/weather.dart';

class WeatherService extends BaseService {
  Future<Weather> getWeatherByLatLon(double lat, double lon) async {
    return locator<WeatherParameter>().wf.currentWeatherByLocation(lat, lon);
  }

  Future<Weather> getWeatherByCityName(String name) async {
    return locator<WeatherParameter>().wf.currentWeatherByCityName(name);
  }
}
