import 'dart:convert';

import 'package:base_entities/app/app_entities/data_list_response.dart';
import 'package:base_entities/data/service/base_service.dart';
import 'package:base_entities/module/auth/model/city_weather.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:flutter/services.dart';

class CommonServive extends BaseService {
  Future<DataListResponse<String>> searchExampleList({
    required int page,
    required int size,
  }) async {
    final res = {
      "data": [
        {"value": "Cập nhật"},
        {"value": "Đổi Font"},
        {"value": "Logout"}
      ],
      "total": 3
    };
    return DataListResponse.fromJson(res, (json) => json["value"]);
  }

  Future<List<Province>> getListProvinces() async {
    String data = await rootBundle.loadString("assets/json/tree.json");
    return List<Province>.from(
        jsonDecode(data).map((item) => Province.fromJson(item))).toList();
  }

  Future<List<CityWeather>> getListCityWeather() async {
    String data = await rootBundle.loadString("assets/json/city_list.json");
    return List<CityWeather>.from(
        jsonDecode(data).map((item) => CityWeather.fromJson(item))).toList();
  }
}

final fonts = ["Roboto", "AARDC___", "LOKICOLA", "FREEBSCA", "Royalacid_o"];
