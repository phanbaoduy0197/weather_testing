import 'package:base_entities/app/app_entities/auth_info.dart';
import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/data/preferences/preferences.dart';
import 'package:base_entities/data/repository/base_repository.dart';
import 'package:base_entities/data/service/user_service.dart';
import 'package:base_entities/di/locator.dart';

class UserRepository extends BaseRepository {
  final _userService = locator<UserService>();

  Future<User?> getMyProfile() async {
    // ---- chỗ này lấy User từ api, chưa có api nên lấy tạm từ local
    final res = await Preferences.getAuthInfo();
    return res?.user;
    // return _userService.getMyProfile();
  }

  // --------- LOGOUT ---------
  Future logout() async {
    return _userService.logout();
  }

  //---------- SAVE AUTH INFO -------------

  void setAutInfo(AuthInfo? info) {
    Preferences.setAuthInfo(info);
  }

  Future<AuthInfo?> getAuthInfo() {
    return Preferences.getAuthInfo();
  }
}
