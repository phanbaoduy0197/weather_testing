import 'package:base_entities/app/app_entities/data_list_response.dart';
import 'package:base_entities/data/preferences/preferences.dart';
import 'package:base_entities/data/service/common_service.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/city_weather.dart';
import 'package:base_entities/module/auth/model/province.dart';

class CommonRepository {
  final _commonService = locator<CommonServive>();
  List<Province> listProvince = [];
  List<CityWeather> listCityWeather = [];
  setTheme(int theme) async {
    await Preferences.setTheme(theme);
  }

  Future<int> getTheme() async {
    return Preferences.getTheme();
  }

  Future<String> getFont() async {
    return Preferences.getFont();
  }

  Future<void> setFont(String font) async {
    return Preferences.setFont(font);
  }

  Future<DataListResponse<String>> searchExampleList({
    required int page,
    required int size,
    // required ProjectSearchRequest request,
  }) async {
    return _commonService.searchExampleList(page: page, size: size);
  }

  Future<List<Province>> getListProvinces() async {
    if (listProvince.isNotEmpty) {
      return listProvince;
    }
    final res = await _commonService.getListProvinces();
    listProvince.addAll(res);
    return res;
  }

  Future<List<CityWeather>> getListCityWeather() async {
    if (listCityWeather.isNotEmpty) {
      return listCityWeather;
    }
    final res = await _commonService.getListCityWeather();
    listCityWeather.addAll(res);
    return res;
  }
}
