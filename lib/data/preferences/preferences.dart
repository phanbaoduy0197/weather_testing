import 'package:base_entities/app/app_entities/auth_info.dart';
import 'package:base_entities/basic/misc/extension.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static const preferencesLanguage = "language";
  static const preferencesAuthInfo = "auth_info";
  static const preferencesProfile = "profile";
  static const preferencesRefreshToken = "refresh_token";
  static const preferencesFcmToken = "fcm_token";
  static const preferencesSendFcmToken = "send_fcm_token";
  static const preferencesTheme = "theme";
  static const preferencesFont = "font";
  static const preferencesGuideParameter = "guide_parameter";

  static setLanguage(String language) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(preferencesLanguage, language);
  }

  static Future<String?> getLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(preferencesLanguage);
  }

  static setTheme(int theme) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(preferencesTheme, theme);
  }

  static Future<int> getTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(preferencesTheme) ?? 0;
  }

  static setFont(String font) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(preferencesFont, font);
  }

  static Future<String> getFont() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(preferencesFont) ?? "";
  }

  static Future setAuthInfo(AuthInfo? info) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (info == null) {
      return await prefs.remove(preferencesAuthInfo);
    } else {
      return await prefs.setString(
          preferencesAuthInfo, info.toJson().toJsonString());
    }
  }

  static Future<AuthInfo?> getAuthInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var userJson = prefs.getString(preferencesAuthInfo);
    if (userJson != null) {
      return AuthInfo.fromJson(userJson.toJsonObject());
    }
    return null;
  }

  //----

  //--------------
  static setGuideParameter(List<int> data) async {
    final stringsList = data.map((i) => i.toString()).toList();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(preferencesGuideParameter, stringsList);
  }

  static Future<List<int>> getGuideParameter() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final stringsList = prefs.getStringList(preferencesGuideParameter) ?? [];
    return stringsList.map((i) => int.parse(i)).toList();
  }
}
