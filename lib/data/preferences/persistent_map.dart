import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/data/preferences/key_value_store.dart';

class PersistentMap<V> implements Map<String, V> {
  final KeyValueStorage storage;
  final String key;
  final Map<String, V> cache;

  PersistentMap._(this.cache, this.storage, this.key);
  static Future<PersistentMap<V>> load<V>(
      KeyValueStorage storage, String key) async {
    Map<String, V> cache;
    try {
      cache = {
        ...((await storage.getMap(key))?.cast<String, V>() ?? <String, V>{})
      };
    } catch (err, stack) {
      Log.e(err, stack);
      cache = <String, V>{};
    }

    return PersistentMap._(cache, storage, key);
  }

  Future save() => storage.setMap(key, cache);

  @override
  V? operator [](Object? key) {
    assert(key is String, "only string key supported");
    return cache[key as String];
  }

  @override
  void operator []=(String key, V? value) {
    if (value == null) {
      cache.remove(key);
    } else {
      cache[key] = value;
    }

    save();
  }

  @override
  void addAll(Map<String, V> other) {
    cache.addAll(other);
    save();
  }

  @override
  void addEntries(Iterable<MapEntry<String, V>> newEntries) {
    cache.addEntries(newEntries);
    save();
  }

  @override
  Map<RK, RV> cast<RK, RV>() => cache.cast<RK, RV>();

  @override
  void clear() {
    cache.clear();
    save();
  }

  @override
  bool containsKey(Object? key) => cache.containsKey(key);

  @override
  bool containsValue(Object? value) => cache.containsValue(value);

  @override
  Iterable<MapEntry<String, V>> get entries => cache.entries;

  @override
  void forEach(void Function(String key, V value) f) => cache.forEach(f);

  @override
  bool get isEmpty => cache.isEmpty;

  @override
  bool get isNotEmpty => cache.isNotEmpty;

  @override
  Iterable<String> get keys => cache.keys;

  @override
  int get length => cache.length;

  @override
  Map<K2, V2> map<K2, V2>(MapEntry<K2, V2> Function(String key, V value) f) =>
      cache.map(f);

  @override
  V putIfAbsent(String key, V Function() ifAbsent) {
    final result = cache.putIfAbsent(key, ifAbsent);
    if (result == null) {
      save();
    }

    return result;
  }

  @override
  V? remove(Object? key) {
    final result = cache.remove(key);
    if (result != null) {
      save();
    }

    return result;
  }

  @override
  void removeWhere(bool Function(String key, V value) predicate) {
    cache.removeWhere(predicate);
    save();
  }

  @override
  V update(String key, V Function(V value) update, {V Function()? ifAbsent}) {
    final result = cache.update(key, update, ifAbsent: ifAbsent);
    save();
    return result;
  }

  @override
  void updateAll(V Function(String key, V value) update) {
    cache.updateAll(update);
    save();
  }

  @override
  Iterable<V> get values => cache.values;
}
