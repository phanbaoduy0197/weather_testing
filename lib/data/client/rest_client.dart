import 'package:base_entities/app/interceptor/auth_handler_interceptor.dart';
import 'package:base_entities/app/interceptor/error_handler_interceptor.dart';
import 'package:base_entities/flavors/flavor.dart';
import 'package:dio/dio.dart';
import 'package:dio_cache_interceptor/dio_cache_interceptor.dart';
import 'api_constant.dart';

class RestClient {
  static const timeOut = 30000;
  static const enableLog = true;
  static const clientLanguage = 'Accept-Language';

  // singleton
  static final RestClient instance = RestClient._internal();

  factory RestClient() {
    return instance;
  }

  RestClient._internal();

  late String baseUrl;
  Map<String, dynamic>? headers;
  late AuthHandlerInterceptor _authHandlerInterceptor;

  void init({
    required Flavor flavor,
    required AuthHandlerInterceptor authHandlerInterceptor,
    String? platform,
    String? deviceId,
    String? language,
    String? appVersion,
  }) {
    baseUrl = flavor.isLive ? baseUrlLive : baseUrlDev;
    _authHandlerInterceptor = authHandlerInterceptor;

    headers = {
      'Content-Type': 'application/json',
      'x-app-version': appVersion,
      'x-device-type': platform,
      'x-device-id': deviceId
    };
    setLanguage(language ?? "vi");
  }

  void setLanguage(String language) {
    headers?[clientLanguage] = language;
  }

  static Dio getDio({required TypeURL typeUrl, int? timeOutMillisecond}) {
    var dio = Dio(instance.getDioBaseOption(
        typeUrl: typeUrl, timeOutMillisecond: timeOutMillisecond));
    // dio.interceptors.clear();

    if (enableLog) {
      dio.interceptors.add(
        LogInterceptor(
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          requestHeader: false,
          // logPrint: (m) => Log.i(m),
        ),
      );
    }

    final options = CacheOptions(
      store: MemCacheStore(),
      policy: CachePolicy.request,
      hitCacheOnErrorExcept: [401, 403],
      maxStale: const Duration(days: 1),
      priority: CachePriority.normal,
      cipher: null,
      keyBuilder: CacheOptions.defaultCacheKeyBuilder,
      allowPostMethod: false,
    );

    // dio.interceptors.add(DioPerformanceInterceptor());
    dio.interceptors.add(instance._authHandlerInterceptor);
    dio.interceptors.add(DioCacheInterceptor(options: options));
    dio.interceptors.add(ErrorHandlerInterceptor());

    return dio;
  }

  // DioCacheManager getCacheManager({required typeURL typeUrl}) {
  //   final String baseUrl = getUrlFromType(typeUrl: typeUrl);
  //   return DioCacheManager(
  //       CacheConfig(baseUrl: baseUrl, skipMemoryCache: true));
  // }

  BaseOptions getDioBaseOption(
      {required TypeURL typeUrl, int? timeOutMillisecond}) {
    final String baseUrl = getUrlFromType(typeUrl: typeUrl);
    return BaseOptions(
      baseUrl: baseUrl,
      connectTimeout: 10 * Duration.millisecondsPerSecond,
      receiveTimeout: 10 * Duration.millisecondsPerSecond,
      sendTimeout: 10 * Duration.millisecondsPerSecond,
      headers: instance.headers,
      responseType: ResponseType.json,
    );
  }

  String getUrlFromType({required TypeURL typeUrl}) {
    switch (typeUrl) {
      default:
        return instance.baseUrl;
    }
  }
}

enum TypeURL {
  baseUrl,
}
