const pageSizeDefault = 20;

const baseUrlDev = "https://partner-stag.rever.vn/api";
const baseUrlLive = "https://partner.rever.vn/api";

//-----------------------------------------_USER

const apiGetMyProfile = "/user/profile";

const apiLogout = "/user/logout";
