import 'dart:async';

import 'package:base_entities/app/app_entities/noti.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/di/locator.dart';
import 'package:flutter/material.dart';

class Noti5HandlerWidget extends StatefulWidget {
  final Widget child;
  const Noti5HandlerWidget({Key? key, required this.child}) : super(key: key);

  @override
  State<Noti5HandlerWidget> createState() => _Noti5HandlerWidgetState();
}

class _Noti5HandlerWidgetState extends State<Noti5HandlerWidget> {
  // final Noti5Manager noti5Mgr = (() => locator<Noti5Manager>())();
  final UserModel userModel = (() => locator<UserModel>())();
  final _subs = <StreamSubscription>[];

  @override
  void initState() {
    super.initState();
    _subs.addAll([
      // noti5Mgr.onHandleNoti5.listen(handleNoti5),
    ]);
    // noti5Mgr.fetchInitialUri();
  }

  @override
  void dispose() {
    for (var element in _subs) {
      element.cancel();
    }
    super.dispose();
  }

  void handleNoti5(RNotificationData data) async {
    Log.i("handle ${data.toJson().toString()}");
    final extra = data.extraDataObject;
    if (extra is RN5KycResultExtraData) {
      //------
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
