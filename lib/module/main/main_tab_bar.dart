import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/module/main/main_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class BottomObject {
  final String icon1;
  final String icon2;
  final String name;
  BottomObject({
    required this.name,
    required this.icon1,
    required this.icon2,
  });
}

///
/// Bottom navigation ở màn hình r_main
class RMainBottomNavigationBar extends StatelessWidget {
  final List<BottomObject> listBottom;
  const RMainBottomNavigationBar({
    Key? key,
    required this.listBottom,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final model = context.watch<MainModel>();
    final mq = MediaQuery.of(context);
    final additionalBottomPadding = mq.padding.bottom;
    return Container(
      decoration: BoxDecoration(
        color: getColor().white,
        boxShadow: const [
          BoxShadow(
            color: Colors.grey,
            offset: Offset(0, -4),
            blurRadius: 24,
          ),
        ],
      ),
      height: kBottomNavigationBarHeight + 6 + additionalBottomPadding,
      child: Row(
        children: [
          const SizedBox(
            width: 4,
          ),
          ...List.generate(
            listBottom.length,
            (index) => Expanded(
              child: InkWell(
                onTap: () => model.onTabChanged(index),
                child: _TabBarItem(
                  additionalBottomPadding: additionalBottomPadding,
                  label: listBottom[index].name,
                  icon1: listBottom[index].icon1,
                  icon2: listBottom[index].icon2,
                  selected: model.tabValue.value == index,
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 4,
          ),
        ],
      ),
    );
  }
}

///
/// 1 Tabbar item bình thường
/// Gồm icon & title
class _TabBarItem extends StatelessWidget {
  final String label;
  final String icon1;
  final String icon2;
  final bool selected;
  final double additionalBottomPadding;
  const _TabBarItem({
    Key? key,
    required this.label,
    required this.icon1,
    required this.icon2,
    required this.additionalBottomPadding,
    this.selected = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const iconSize = 24.0;
    final color = selected ? getColor().textBase : Colors.grey;
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(bottom: additionalBottomPadding),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              selected ? icon1 : icon2,
              width: iconSize,
              height: iconSize,
            ),
            const SizedBox(height: 4),
            Text(
              label,
              style: textTheme(context).body2.copyWith(
                  fontSize: 12,
                  color: color,
                  fontWeight: selected ? FontWeight.w500 : FontWeight.normal),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
