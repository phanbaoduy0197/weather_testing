import 'package:base_entities/base/base_model/base_model.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class MainModel extends BaseModel {
  ValueNotifier<int> tabValue = ValueNotifier(0);
  final PublishSubject animatedSub = PublishSubject();

  @override
  ViewState get initState => ViewState.loaded;

  PageController? pageController;

  onTabChanged(int index) {
    pageController?.jumpToPage(index);
    tabValue.value = index;
    animatedSub.add(true);
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
    tabValue.dispose();
    pageController?.dispose();
    animatedSub.close();
  }
}
