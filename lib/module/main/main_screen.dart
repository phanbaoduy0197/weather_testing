import 'dart:async';

import 'package:base_entities/app/my_app.dart';
import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/basic/images/images.dart';
import 'package:base_entities/basic/theme/font_manager.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/generated/locale_keys.g.dart';
import 'package:base_entities/module/home/home_screen.dart';
import 'package:base_entities/module/profile/profile_screen.dart';
import 'package:base_entities/module/main/main_model.dart';
import 'package:base_entities/module/main/main_tab_bar.dart';
import 'package:base_entities/base/base_state/base_state.dart';
import 'package:base_entities/module/main/noti5_handler_widget.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

// ignore_for_file: prefer_const_constructors

@RoutePage()
class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MainScreenState();
}

class _MainScreenState extends BaseState<MainModel, MainScreen>
    with SingleTickerProviderStateMixin {
  StreamSubscription? _subscription;
  StreamSubscription? _animatedSub;
  late AnimationController primaryController;

  @override
  void initState() {
    super.initState();
    locator<WeatherParameter>().init();
    _animatedSub = model.animatedSub.listen((value) {
      forwardController();
    });
    primaryController = AnimationController(
      duration: const Duration(milliseconds: 250),
      vsync: this,
    );
    model.pageController = PageController();
    _subscription = eventBus.on<EventChangeTab>().listen((event) async {
      model.onTabChanged(event.index);
    });
    primaryController.forward();
  }

  forwardController() async {
    if (primaryController.isAnimating) {
      primaryController.reset();
    }
    await primaryController.reverse();
    await primaryController.forward();
  }

  @override
  void dispose() {
    super.dispose();
    _animatedSub?.cancel();
    _subscription?.cancel();
  }

  @override
  Widget buildContentView(BuildContext context, MainModel model) {
    return ReverPopScope(
      onWillPop: onWillPop,
      child: Consumer<FontManager>(builder: (context, f, child) {
        return Scaffold(
          backgroundColor: Colors.white,
          resizeToAvoidBottomInset: false,
          body: SizedBox.expand(
            child: Consumer<MainModel>(builder: (context, myModel, child) {
              return Noti5HandlerWidget(
                child: Stack(
                  children: [
                    PageView(
                      controller: myModel.pageController,
                      physics: const NeverScrollableScrollPhysics(),
                      onPageChanged: (index) {
                        myModel.onTabChanged(index);
                      },
                      children: [
                        HomeScreen(),
                        ProfileScreen(),
                      ]
                          .map(
                            (e) => FadeTransition(
                              opacity: Tween<double>(
                                begin: 0.2,
                                end: 1.0,
                              ).animate(primaryController),
                              child: e,
                            ),
                          )
                          .toList(),
                    ),
                    //------POSITIONED TO SHOW HELP BUTTON-------
                  ],
                ),
              );
            }),
          ),
          // floatingActionButton: const _SupportButton(),
          bottomNavigationBar: RMainBottomNavigationBar(
            listBottom: [
              BottomObject(
                name: "Trang chủ",
                icon1: DImages.homeDiscover,
                icon2: DImages.homeDiscover2,
              ),
              BottomObject(
                name: "Thông tin",
                icon1: DImages.userCircleBold,
                icon2: DImages.userCircle,
              ),
            ],
          ),
        );
      }),
    );
  }
}

class EventChangeTab {
  int index;
  EventChangeTab({required this.index});
}

class ReverPopScope extends StatefulWidget {
  final Widget child;
  final Future<bool> Function() onWillPop;

  const ReverPopScope({
    super.key,
    required this.child,
    required this.onWillPop,
  });

  @override
  State<ReverPopScope> createState() => _ReverPopScopeState();
}

class _ReverPopScopeState extends State<ReverPopScope> {
  @override
  Widget build(BuildContext context) {
    return PopScope(
      canPop: false,
      onPopInvoked: (didPop) async {
        if (!didPop) {
          final res = await widget.onWillPop();
          if (res && mounted) {
            Navigator.pop(context);
          }
        }
      },
      child: widget.child,
    );
  }
}

DateTime? currentBackPressTime;

Future<bool> onWillPop() {
  DateTime now = DateTime.now();
  if (currentBackPressTime == null ||
      now.difference(currentBackPressTime!) > const Duration(seconds: 2)) {
    currentBackPressTime = now;
    Fluttertoast.showToast(msg: LocaleKeys.backToExit.tr());
    return Future.value(false);
  }
  return Future.value(true);
}
