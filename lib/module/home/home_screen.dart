import 'dart:async';

import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/base/base_state/base_state.dart';
import 'package:base_entities/basic/misc/dimens.dart';
import 'package:base_entities/basic/misc/extension.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/basic/util/date_time_utils.dart';
import 'package:base_entities/basic/util/snack_bar_util.dart';
import 'package:base_entities/generated/locale_keys.g.dart';
import 'package:base_entities/module/auth/ui/login_screen.dart';
import 'package:base_entities/module/home/home_model.dart';
import 'package:base_entities/module/widget/bottom_sheet.dart';
import 'package:base_entities/module/widget/default_button_widget.dart';
import 'package:base_entities/module/widget/guide/guide_widget.dart';
import 'package:base_entities/module/widget/image_widget.dart';
import 'package:base_entities/module/widget/modal_loading_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:rxdart/rxdart.dart';
import 'package:separated_column/separated_column.dart';
import 'package:weather/weather.dart';

import '../widget/guide/guide_type_enum.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends BaseState<HomeModel, HomeScreen>
    with AutomaticKeepAliveClientMixin {
  StreamSubscription<Position>? positionStream;

  @override
  void initState() {
    super.initState();
    init();
  }

  @override
  void dispose() {
    super.dispose();
    if (positionStream != null) {
      positionStream?.cancel();
      positionStream = null;
    }
  }

  Future init() async {
    await Future.delayed(const Duration(seconds: 1));
    //---lần đầu init sẽ lấy theo khu vực trong profile, nếu không có cũng được nên không cần báo lỗi---
    model.initWeather();
  }

  void _handleMyLocation(BuildContext context) async {
    final res = await ModalLoadingIndicator.showLoadingJob(
      context,
      model.geoManager.getCurrentPosition(context),
    );
    if (mounted) {
      if (res == null) {
        context.snackBarError("Không có quyền truy cập vị trí");
      } else {
        await ModalLoadingIndicator.showLoadingJob(
            context, model.getWeatherAtMyLocation(res.latitude, res.longitude));
        if (model.progressState == ProgressState.error && mounted) {
          context.snackBarError(model.responseError1!.message);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            LocaleKeys.appName.tr(),
            style: context.tt.h4,
          ),
          actions: [
            GuideWidget(
              type: GuideType.homeRefresh,
              child: InkWell(
                onTap: () => _handleMyLocation(context),
                child: const Padding(
                  padding: EdgeInsets.all(8),
                  child: Icon(
                    Icons.my_location,
                    size: 24,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: Dimens.paddingLeftRight,
            ),
          ],
          centerTitle: true,
        ),
        body: super.buildContent(),
      ),
    );
  }

  @override
  Widget buildContentView(BuildContext context, HomeModel model) {
    return SingleChildScrollView(
      padding: const EdgeInsets.only(
        left: Dimens.paddingLeftRight,
        right: Dimens.paddingLeftRight,
        top: 16,
        bottom: 32,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          StreamBuilder<Weather?>(
              stream: model.weather.stream,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return const SizedBox();
                }
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    if (snapshot.data != null)
                      _buildContent(context, snapshot.data),
                    if (snapshot.data != null)
                      const SizedBox(
                        height: 32,
                      ),
                  ],
                );
              }),
          Row(
            children: [
              Expanded(
                child: GuideWidget(
                  type: GuideType.homeField,
                  child: TextFieldCustom(
                    controller: model.controller,
                    title: "Tên thành phố",
                  ),
                ),
              ),
              InkWell(
                onTap: () async {
                  final res = await model.getAllCity();
                  if (mounted) {
                    final value =
                        await showListCity(context, res, model.controller.text);
                    if (value?.isNotEmpty == true) {
                      model.controller.text = value!;
                    }
                  }
                },
                child: const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.arrow_drop_down_outlined,
                    size: 24,
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 8,
          ),
          Row(
            children: [
              const Spacer(),
              Expanded(
                flex: 2,
                child: DefaultButtonWidget(
                  title: "Tìm kiếm",
                  onTap: () async {
                    FocusScope.of(context).unfocus();
                    await ModalLoadingIndicator.showLoadingJob(
                        context, model.getWeatherByCityName());
                    if (mounted) {
                      _handleCityName(context);
                    }
                  },
                ),
              ),
              const Spacer(),
            ],
          ),
          const SizedBox(
            height: 32,
          ),
          Row(
            children: [
              Expanded(
                child: Text(
                  "Bật chế độ tự động cập nhật theo vị trí cứ mỗi 10 phút",
                  style: context.tt.sub1.blueBold,
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              StreamBuilder<bool>(
                  initialData: false,
                  stream: model.enableStream.stream,
                  builder: (context, snapshot) {
                    return InkWell(
                      onTap: () async {
                        bool res = await _handlePermission(context);
                        if (res == true && mounted) {
                          _handleStream(context, snapshot.data == true);
                          model.enableStream.sink
                              .add(!model.enableStream.value);
                        }
                      },
                      child: IgnorePointer(
                        child: CupertinoSwitch(
                          value: snapshot.data!,
                          onChanged: (value) {},
                        ),
                      ),
                    );
                  }),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildContent(BuildContext context, Weather? data) {
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        border: Border.all(
          width: 1,
          color: getColor().primaryColor,
        ),
      ),
      child: SeparatedColumn(
        crossAxisAlignment: CrossAxisAlignment.start,
        separatorBuilder: (context, index) {
          return const SizedBox(
            height: 8,
          );
        },
        children: [
          Row(
            children: [
              if (data?.weatherIcon != null)
                RoundNetworkImage(
                  width: 40,
                  height: 40,
                  url: data!.weatherIcon!.parseIcon(),
                ),
              if (data?.weatherIcon != null)
                const SizedBox(
                  width: 12,
                ),
              if (data?.areaName?.isNotEmpty == true)
                Expanded(
                  child: Text(
                    data?.areaName ?? "",
                    style: context.tt.sub1.primaryColor,
                  ),
                ),
            ],
          ),
          if (data?.date != null)
            Text(
              formatDateTime1(
                data!.date!.millisecondsSinceEpoch,
                withTime: true,
                withYear: true,
              ),
              style: context.tt.body1.grey,
            ),
          if (data?.weatherDescription?.isNotEmpty == true)
            _item(
              context,
              "Thời tiết: ",
              data?.weatherDescription,
            ),
          if (data?.temperature?.celsius?.checkZero() != null)
            _item(
              context,
              "Nhiệt độ: ",
              "${data?.temperature?.celsius?.formatDouble()} °C",
            ),
          if (data?.tempMin?.celsius?.checkZero() != null)
            _item(
              context,
              "Nhiệt độ thấp nhất: ",
              "${data?.tempMin?.celsius?.formatDouble()} °C, ",
            ),
          if (data?.tempMax?.celsius?.checkZero() != null)
            _item(
              context,
              "Nhiệt độ cao nhất: ",
              "${data?.tempMax?.celsius?.formatDouble()} °C, ",
            ),
        ],
      ),
    );
  }

  Widget _item(BuildContext context, String title, String? sub) {
    return Text.rich(
      TextSpan(
        children: [
          TextSpan(
            text: title,
            style: context.tt.sub1,
          ),
          TextSpan(
            text: sub,
            style: context.tt.body1.blueBold,
          ),
        ],
      ),
    );
  }

  Future<String?> showListCity(
      BuildContext context, List<String> list, String? init) async {
    final res = await RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(list.length, (index) {
                final item = list[index];
                return InkWell(
                  onTap: () {
                    Navigator.pop(ct, item);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            list[index],
                            style: ct.tt.body1,
                          ),
                        ),
                        if (init == item)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
    if (res != null && res is String) {
      return res;
    }
    return null;
  }

  Future<bool> _handlePermission(BuildContext context) async {
    if (model.geoManager.hasPermission) {
      return true;
    } else {
      return await model.geoManager.handlePermission(context);
    }
  }

  Future _handleStream(BuildContext context, bool isPause) async {
    if (isPause) {
      positionStream?.pause();
    } else {
      if (positionStream == null) {
        final ps = model.geoManager.geolocatorPlatform.getPositionStream();
        positionStream = ps
            .handleError((error) {
              positionStream?.cancel();
              positionStream = null;
            })
            .delay(const Duration(minutes: 10))
            .listen((position) {
              model.getWeatherAtMyLocation(
                  position.latitude, position.longitude);
            });
      } else {
        positionStream?.resume();
      }
    }
  }

  Future _handleCityName(BuildContext context) async {
    if (model.progressState == ProgressState.success) {
      context.snackBarSuccess("Đã cập nhật thành công");
    } else if (model.progressState == ProgressState.error) {
      context.snackBarError(model.responseError1!.message);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
