import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/basic/misc/extension.dart';
import 'package:base_entities/basic/util/geolocator_manager.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/data/service/weather_service.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';
import 'package:weather/weather.dart';

class HomeModel extends BaseModel {
  @override
  ViewState get initState => ViewState.loaded;

  final weather = BehaviorSubject<Weather?>();
  late final parameter = locator<WeatherParameter>();
  late final service = locator<WeatherService>();
  late final geoManager = locator<GeolocatorManager>();
  final controller = TextEditingController();
  final enableStream = BehaviorSubject<bool>.seeded(false);

  Future initWeather() async {
    String? codename = parameter.province.valueOrNull?.codename;
    if (codename?.isWeather() == true) {
      codename = codename!.configCity();
      await doTask(doSomething: () async {
        final res = await service.getWeatherByCityName(codename!);
        weather.sink.add(res);
      });
    }
  }

  Future getWeatherByCityName() async {
    await doTask(doSomething: () async {
      final res = await service.getWeatherByCityName(controller.text);
      weather.sink.add(res);
    });
  }

  Future<List<String>> getAllCity() async {
    final res = await locator<CommonRepository>().getListCityWeather();
    return res.map((e) => e.name).whereNotNull().toList();
  }

  Future getWeatherAtMyLocation(double lat, double lon) async {
    await doTask(doSomething: () async {
      final res = await service.getWeatherByLatLon(lat, lon);
      weather.sink.add(res);
    });
  }

  changeNotify() {
    notifyListeners();
  }

  @override
  void dispose() {
    super.dispose();
    weather.close();
    enableStream.close();
    controller.dispose();
  }
}
