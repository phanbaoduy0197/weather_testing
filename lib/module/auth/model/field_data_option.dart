import 'package:freezed_annotation/freezed_annotation.dart';

part 'field_data_option.freezed.dart';
// optional: Since our Person class is serializable, we must add this line.
// But if Person was not serializable, we could skip it.
part 'field_data_option.g.dart';

@freezed
class RFieldDataOption with _$RFieldDataOption {
  const factory RFieldDataOption({
    String? internalValue,
    String? name,
    Meta? meta,
  }) = _RFieldDataOption;

  factory RFieldDataOption.fromJson(Map<String, Object?> json) =>
      _$RFieldDataOptionFromJson(json);
}

@freezed
class Meta with _$Meta {
  const factory Meta({
    List<String>? group,
    bool? http,
  }) = _Meta;

  factory Meta.fromJson(Map<String, Object?> json) => _$MetaFromJson(json);
}
