class Province {
  String? name;
  int? code;
  String? codename;
  String? divisionType;
  int? phoneCode;
  List<Districts>? districts;

  Province({
    this.name,
    this.code,
    this.codename,
    this.divisionType,
    this.phoneCode,
    this.districts,
  });

  Province copyWith({
    String? name,
    int? code,
    String? codename,
    String? divisionType,
    int? phoneCode,
    List<Districts>? districts,
  }) {
    return Province(
      name: name ?? this.name,
      code: code ?? this.code,
      codename: codename ?? this.codename,
      divisionType: divisionType ?? this.divisionType,
      phoneCode: phoneCode ?? this.phoneCode,
      districts: districts ?? this.districts,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'code': code,
      'codename': codename,
      'division_type': divisionType,
      'phone_code': phoneCode,
      'districts': districts,
    };
  }

  factory Province.fromJson(Map<String, dynamic> json) {
    return Province(
      name: json['name'] as String?,
      code: json['code'] as int?,
      codename: json['codename'] as String?,
      divisionType: json['division_type'] as String?,
      phoneCode: json['phone_code'] as int?,
      districts: (json['districts'] as List<dynamic>?)
          ?.map((e) => Districts.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  @override
  String toString() =>
      "Province(name: $name,code: $code,codename: $codename,divisionType: $divisionType,phoneCode: $phoneCode,districts: $districts)";

  @override
  int get hashCode =>
      Object.hash(name, code, codename, divisionType, phoneCode, districts);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Province &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code &&
          codename == other.codename &&
          divisionType == other.divisionType &&
          phoneCode == other.phoneCode &&
          districts == other.districts;
}

class Districts {
  String? name;
  int? code;
  String? codename;
  String? divisionType;
  String? shortCodename;
  List<Wards>? wards;

  Districts({
    this.name,
    this.code,
    this.codename,
    this.divisionType,
    this.shortCodename,
    this.wards,
  });

  Districts copyWith({
    String? name,
    int? code,
    String? codename,
    String? divisionType,
    String? shortCodename,
    List<Wards>? wards,
  }) {
    return Districts(
      name: name ?? this.name,
      code: code ?? this.code,
      codename: codename ?? this.codename,
      divisionType: divisionType ?? this.divisionType,
      shortCodename: shortCodename ?? this.shortCodename,
      wards: wards ?? this.wards,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'code': code,
      'codename': codename,
      'division_type': divisionType,
      'short_codename': shortCodename,
      'wards': wards,
    };
  }

  factory Districts.fromJson(Map<String, dynamic> json) {
    return Districts(
      name: json['name'] as String?,
      code: json['code'] as int?,
      codename: json['codename'] as String?,
      divisionType: json['division_type'] as String?,
      shortCodename: json['short_codename'] as String?,
      wards: (json['wards'] as List<dynamic>?)
          ?.map((e) => Wards.fromJson(e as Map<String, dynamic>))
          .toList(),
    );
  }

  @override
  String toString() =>
      "Districts(name: $name,code: $code,codename: $codename,divisionType: $divisionType,shortCodename: $shortCodename,wards: $wards)";

  @override
  int get hashCode =>
      Object.hash(name, code, codename, divisionType, shortCodename, wards);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Districts &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code &&
          codename == other.codename &&
          divisionType == other.divisionType &&
          shortCodename == other.shortCodename &&
          wards == other.wards;
}

class Wards {
  String? name;
  int? code;
  String? codename;
  String? divisionType;
  String? shortCodename;

  Wards({
    this.name,
    this.code,
    this.codename,
    this.divisionType,
    this.shortCodename,
  });

  Wards copyWith({
    String? name,
    int? code,
    String? codename,
    String? divisionType,
    String? shortCodename,
  }) {
    return Wards(
      name: name ?? this.name,
      code: code ?? this.code,
      codename: codename ?? this.codename,
      divisionType: divisionType ?? this.divisionType,
      shortCodename: shortCodename ?? this.shortCodename,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'code': code,
      'codename': codename,
      'division_type': divisionType,
      'short_codename': shortCodename,
    };
  }

  factory Wards.fromJson(Map<String, dynamic> json) {
    return Wards(
      name: json['name'] as String?,
      code: json['code'] as int?,
      codename: json['codename'] as String?,
      divisionType: json['division_type'] as String?,
      shortCodename: json['short_codename'] as String?,
    );
  }

  @override
  String toString() =>
      "Wards(name: $name,code: $code,codename: $codename,divisionType: $divisionType,shortCodename: $shortCodename)";

  @override
  int get hashCode =>
      Object.hash(name, code, codename, divisionType, shortCodename);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Wards &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          code == other.code &&
          codename == other.codename &&
          divisionType == other.divisionType &&
          shortCodename == other.shortCodename;
}
