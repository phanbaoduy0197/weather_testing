// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'field_data_option.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$RFieldDataOptionImpl _$$RFieldDataOptionImplFromJson(
        Map<String, dynamic> json) =>
    _$RFieldDataOptionImpl(
      internalValue: json['internalValue'] as String?,
      name: json['name'] as String?,
      meta: json['meta'] == null
          ? null
          : Meta.fromJson(json['meta'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$RFieldDataOptionImplToJson(
        _$RFieldDataOptionImpl instance) =>
    <String, dynamic>{
      'internalValue': instance.internalValue,
      'name': instance.name,
      'meta': instance.meta,
    };

_$MetaImpl _$$MetaImplFromJson(Map<String, dynamic> json) => _$MetaImpl(
      group:
          (json['group'] as List<dynamic>?)?.map((e) => e as String).toList(),
      http: json['http'] as bool?,
    );

Map<String, dynamic> _$$MetaImplToJson(_$MetaImpl instance) =>
    <String, dynamic>{
      'group': instance.group,
      'http': instance.http,
    };
