class CityWeather {
  num? id;
  String? name;
  String? state;
  String? country;
  Coord? coord;

  CityWeather({
    this.id,
    this.name,
    this.state,
    this.country,
    this.coord,
  });

  CityWeather copyWith({
    num? id,
    String? name,
    String? state,
    String? country,
    Coord? coord,
  }) {
    return CityWeather(
      id: id ?? this.id,
      name: name ?? this.name,
      state: state ?? this.state,
      country: country ?? this.country,
      coord: coord ?? this.coord,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'state': state,
      'country': country,
      'coord': coord,
    };
  }

  factory CityWeather.fromJson(Map<String, dynamic> json) {
    return CityWeather(
      id: json['id'] as num?,
      name: json['name'] as String?,
      state: json['state'] as String?,
      country: json['country'] as String?,
      coord: json['coord'] == null
          ? null
          : Coord.fromJson(json['coord'] as Map<String, dynamic>),
    );
  }

  @override
  String toString() =>
      "CityWeather(id: $id,name: $name,state: $state,country: $country,coord: $coord)";

  @override
  int get hashCode => Object.hash(id, name, state, country, coord);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CityWeather &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          name == other.name &&
          state == other.state &&
          country == other.country &&
          coord == other.coord;
}

class Coord {
  double? lon;
  double? lat;

  Coord({
    this.lon,
    this.lat,
  });

  Coord copyWith({
    double? lon,
    double? lat,
  }) {
    return Coord(
      lon: lon ?? this.lon,
      lat: lat ?? this.lat,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lon': lon,
      'lat': lat,
    };
  }

  factory Coord.fromJson(Map<String, dynamic> json) {
    return Coord(
      lon: json['lon'] as double?,
      lat: json['lat'] as double?,
    );
  }

  @override
  String toString() => "Coord(lon: $lon,lat: $lat)";

  @override
  int get hashCode => Object.hash(lon, lat);

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Coord &&
          runtimeType == other.runtimeType &&
          lon == other.lon &&
          lat == other.lat;
}
