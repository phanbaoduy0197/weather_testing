import 'dart:convert';

class Session {
  Session({
    this.key,
    this.value,
    this.domain,
    this.timeoutInMs,
    this.path,
    this.maxAge,
  });

  String? key;
  String? value;
  String? domain;
  int? timeoutInMs;
  String? path;
  int? maxAge;

  factory Session.fromRawJson(String str) => Session.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory Session.fromJson(Map<String, dynamic> json) => Session(
        key: json["key"],
        value: json["value"],
        domain: json["domain"],
        timeoutInMs: json["timeout_in_ms"],
        path: json["path"],
        maxAge: json["max_age"],
      );

  Map<String, dynamic> toJson() => {
        "key": key,
        "value": value,
        "domain": domain,
        "timeout_in_ms": timeoutInMs,
        "path": path,
        "max_age": maxAge,
      };
}
