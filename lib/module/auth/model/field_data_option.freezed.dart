// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'field_data_option.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RFieldDataOption _$RFieldDataOptionFromJson(Map<String, dynamic> json) {
  return _RFieldDataOption.fromJson(json);
}

/// @nodoc
mixin _$RFieldDataOption {
  String? get internalValue => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;
  Meta? get meta => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RFieldDataOptionCopyWith<RFieldDataOption> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RFieldDataOptionCopyWith<$Res> {
  factory $RFieldDataOptionCopyWith(
          RFieldDataOption value, $Res Function(RFieldDataOption) then) =
      _$RFieldDataOptionCopyWithImpl<$Res, RFieldDataOption>;
  @useResult
  $Res call({String? internalValue, String? name, Meta? meta});

  $MetaCopyWith<$Res>? get meta;
}

/// @nodoc
class _$RFieldDataOptionCopyWithImpl<$Res, $Val extends RFieldDataOption>
    implements $RFieldDataOptionCopyWith<$Res> {
  _$RFieldDataOptionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? internalValue = freezed,
    Object? name = freezed,
    Object? meta = freezed,
  }) {
    return _then(_value.copyWith(
      internalValue: freezed == internalValue
          ? _value.internalValue
          : internalValue // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      meta: freezed == meta
          ? _value.meta
          : meta // ignore: cast_nullable_to_non_nullable
              as Meta?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $MetaCopyWith<$Res>? get meta {
    if (_value.meta == null) {
      return null;
    }

    return $MetaCopyWith<$Res>(_value.meta!, (value) {
      return _then(_value.copyWith(meta: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$RFieldDataOptionImplCopyWith<$Res>
    implements $RFieldDataOptionCopyWith<$Res> {
  factory _$$RFieldDataOptionImplCopyWith(_$RFieldDataOptionImpl value,
          $Res Function(_$RFieldDataOptionImpl) then) =
      __$$RFieldDataOptionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? internalValue, String? name, Meta? meta});

  @override
  $MetaCopyWith<$Res>? get meta;
}

/// @nodoc
class __$$RFieldDataOptionImplCopyWithImpl<$Res>
    extends _$RFieldDataOptionCopyWithImpl<$Res, _$RFieldDataOptionImpl>
    implements _$$RFieldDataOptionImplCopyWith<$Res> {
  __$$RFieldDataOptionImplCopyWithImpl(_$RFieldDataOptionImpl _value,
      $Res Function(_$RFieldDataOptionImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? internalValue = freezed,
    Object? name = freezed,
    Object? meta = freezed,
  }) {
    return _then(_$RFieldDataOptionImpl(
      internalValue: freezed == internalValue
          ? _value.internalValue
          : internalValue // ignore: cast_nullable_to_non_nullable
              as String?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
      meta: freezed == meta
          ? _value.meta
          : meta // ignore: cast_nullable_to_non_nullable
              as Meta?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$RFieldDataOptionImpl implements _RFieldDataOption {
  const _$RFieldDataOptionImpl({this.internalValue, this.name, this.meta});

  factory _$RFieldDataOptionImpl.fromJson(Map<String, dynamic> json) =>
      _$$RFieldDataOptionImplFromJson(json);

  @override
  final String? internalValue;
  @override
  final String? name;
  @override
  final Meta? meta;

  @override
  String toString() {
    return 'RFieldDataOption(internalValue: $internalValue, name: $name, meta: $meta)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RFieldDataOptionImpl &&
            (identical(other.internalValue, internalValue) ||
                other.internalValue == internalValue) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.meta, meta) || other.meta == meta));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, internalValue, name, meta);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RFieldDataOptionImplCopyWith<_$RFieldDataOptionImpl> get copyWith =>
      __$$RFieldDataOptionImplCopyWithImpl<_$RFieldDataOptionImpl>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$RFieldDataOptionImplToJson(
      this,
    );
  }
}

abstract class _RFieldDataOption implements RFieldDataOption {
  const factory _RFieldDataOption(
      {final String? internalValue,
      final String? name,
      final Meta? meta}) = _$RFieldDataOptionImpl;

  factory _RFieldDataOption.fromJson(Map<String, dynamic> json) =
      _$RFieldDataOptionImpl.fromJson;

  @override
  String? get internalValue;
  @override
  String? get name;
  @override
  Meta? get meta;
  @override
  @JsonKey(ignore: true)
  _$$RFieldDataOptionImplCopyWith<_$RFieldDataOptionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}

Meta _$MetaFromJson(Map<String, dynamic> json) {
  return _Meta.fromJson(json);
}

/// @nodoc
mixin _$Meta {
  List<String>? get group => throw _privateConstructorUsedError;
  bool? get http => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $MetaCopyWith<Meta> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MetaCopyWith<$Res> {
  factory $MetaCopyWith(Meta value, $Res Function(Meta) then) =
      _$MetaCopyWithImpl<$Res, Meta>;
  @useResult
  $Res call({List<String>? group, bool? http});
}

/// @nodoc
class _$MetaCopyWithImpl<$Res, $Val extends Meta>
    implements $MetaCopyWith<$Res> {
  _$MetaCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? group = freezed,
    Object? http = freezed,
  }) {
    return _then(_value.copyWith(
      group: freezed == group
          ? _value.group
          : group // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      http: freezed == http
          ? _value.http
          : http // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$MetaImplCopyWith<$Res> implements $MetaCopyWith<$Res> {
  factory _$$MetaImplCopyWith(
          _$MetaImpl value, $Res Function(_$MetaImpl) then) =
      __$$MetaImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<String>? group, bool? http});
}

/// @nodoc
class __$$MetaImplCopyWithImpl<$Res>
    extends _$MetaCopyWithImpl<$Res, _$MetaImpl>
    implements _$$MetaImplCopyWith<$Res> {
  __$$MetaImplCopyWithImpl(_$MetaImpl _value, $Res Function(_$MetaImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? group = freezed,
    Object? http = freezed,
  }) {
    return _then(_$MetaImpl(
      group: freezed == group
          ? _value._group
          : group // ignore: cast_nullable_to_non_nullable
              as List<String>?,
      http: freezed == http
          ? _value.http
          : http // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$MetaImpl implements _Meta {
  const _$MetaImpl({final List<String>? group, this.http}) : _group = group;

  factory _$MetaImpl.fromJson(Map<String, dynamic> json) =>
      _$$MetaImplFromJson(json);

  final List<String>? _group;
  @override
  List<String>? get group {
    final value = _group;
    if (value == null) return null;
    if (_group is EqualUnmodifiableListView) return _group;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final bool? http;

  @override
  String toString() {
    return 'Meta(group: $group, http: $http)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MetaImpl &&
            const DeepCollectionEquality().equals(other._group, _group) &&
            (identical(other.http, http) || other.http == http));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_group), http);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MetaImplCopyWith<_$MetaImpl> get copyWith =>
      __$$MetaImplCopyWithImpl<_$MetaImpl>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$MetaImplToJson(
      this,
    );
  }
}

abstract class _Meta implements Meta {
  const factory _Meta({final List<String>? group, final bool? http}) =
      _$MetaImpl;

  factory _Meta.fromJson(Map<String, dynamic> json) = _$MetaImpl.fromJson;

  @override
  List<String>? get group;
  @override
  bool? get http;
  @override
  @JsonKey(ignore: true)
  _$$MetaImplCopyWith<_$MetaImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
