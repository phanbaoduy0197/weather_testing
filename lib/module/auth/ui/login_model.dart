import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/rxdart.dart';

class LoginModel extends BaseModel {
  @override
  ViewState get initState => ViewState.loading;
  TextEditingController loginController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController wardController = TextEditingController();

  BehaviorSubject<Province?> get province =>
      locator<WeatherParameter>().province;
  BehaviorSubject<Districts?> get district =>
      locator<WeatherParameter>().district;
  BehaviorSubject<Wards?> get ward => locator<WeatherParameter>().ward;
  BehaviorSubject<List<Province>> get listProvinces =>
      locator<WeatherParameter>().listAllProvinces;

  onLogin() async {
    await doTask(doSomething: () async {
      return;
    });
  }

  Future getAllProvince() async {
    if (listProvinces.value.isEmpty) {
      final res = await locator<CommonRepository>().getListProvinces();
      listProvinces.add(res);
    }
    setState(ViewState.loaded);
  }

  @override
  void dispose() {
    super.dispose();
    loginController.dispose();
    provinceController.dispose();
    districtController.dispose();
    wardController.dispose();
  }
}
