import 'package:base_entities/app/app_entities/auth_info.dart';
import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/base/base_state/base_error.dart';
import 'package:base_entities/base/base_state/base_state.dart';
import 'package:base_entities/basic/misc/dimens.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/basic/util/snack_bar_util.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:base_entities/module/auth/model/session.dart';
import 'package:base_entities/module/auth/ui/login_model.dart';
import 'package:base_entities/module/widget/bottom_sheet.dart';
import 'package:base_entities/module/widget/default_button_widget.dart';
import 'package:base_entities/module/widget/modal_loading_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

@RoutePage()
class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends BaseState<LoginModel, LoginScreen> {
  @override
  void initState() {
    super.initState();
    model.getAllProvince();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Thông tin cơ bản",
            style: context.tt.h4,
          ),
          centerTitle: true,
        ),
        body: super.buildContent(),
        bottomNavigationBar: SafeArea(
          child: Container(
            color: getColor().white,
            padding: const EdgeInsets.all(16),
            child: DefaultButtonWidget(
              title: "Tiếp tục",
              onTap: () async {
                if (model.loginController.text.trim().isEmpty) {
                  context.snackBarError("Vui lòng nhập tên của bạn");
                } else {
                  await ModalLoadingIndicator.showLoadingJob(
                      context, model.onLogin());
                  _handleLogin();
                }
              },
            ),
          ),
        ),
      ),
    );
  }

  _handleLogin() async {
    if (model.progressState == ProgressState.success) {
      locator.get<UserModel>().login(
            AuthInfo(
              user: User(
                fullName: model.loginController.text,
                district: model.districtController.text,
                province: model.provinceController.text,
                ward: model.wardController.text,
                token: "token_${model.loginController.text}",
              ),
              session: Session(),
            ),
          );
    } else if (model.progressState == ProgressState.error) {
      context.snackBarError(model.responseError1!.localizedMessage.tr());
    }
  }

  @override
  Widget buildContentView(BuildContext context, LoginModel model) {
    if (model.listProvinces.value.isEmpty) {
      return const Center(
        child: LoadingWidget(),
      );
    }
    return SingleChildScrollView(
      padding: const EdgeInsets.all(Dimens.paddingLeftRight),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextFieldCustom(
            controller: model.loginController,
            validator: errorText,
            title: "Tên của bạn (bắt buộc)",
          ),
          StreamBuilder<Province?>(
              stream: model.province.stream,
              builder: (context, snapshot) {
                return InkWell(
                  onTap: () {
                    if (model.listProvinces.value.isEmpty) {
                      context.snackBarError(
                        "Lấy dữ liệu thất bại vui lòng đợi trong giây lát",
                      );
                      model.getAllProvince();
                    } else {
                      showProvince(context);
                    }
                  },
                  child: ChildSelection(
                    controller: model.provinceController,
                    hintText: "Tỉnh/ Thành",
                    isDisable: false,
                  ),
                );
              }),
          StreamBuilder<Province?>(
              stream: model.province.stream,
              builder: (context, snapshot) {
                return InkWell(
                  onTap: () {
                    if (!snapshot.hasData) {
                      context.snackBarError("Vui lòng nhập Tỉnh/ Thành");
                    } else {
                      if (snapshot.data?.districts?.isNotEmpty == true) {
                        showDistrict(context);
                      } else {
                        context.snackBarError("Không có Quận/ Huyện");
                      }
                    }
                  },
                  child: ChildSelection(
                    controller: model.districtController,
                    hintText: "Quận/ Huyện",
                    isDisable: false,
                  ),
                );
              }),
          StreamBuilder<Districts?>(
              stream: model.district.stream,
              builder: (context, snapshot) {
                return InkWell(
                  onTap: () {
                    if (!snapshot.hasData) {
                      context.snackBarError("Vui lòng nhập Quận/ Huyện");
                    } else {
                      if (snapshot.data?.wards?.isNotEmpty == true) {
                        showWard(context);
                      } else {
                        context.snackBarError("Không có Phường/ Xã");
                      }
                    }
                  },
                  child: ChildSelection(
                    controller: model.wardController,
                    hintText: "Phường/ Xã",
                    isDisable: !snapshot.hasData,
                  ),
                );
              }),
        ],
      ),
    );
  }

  void showDistrict(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(model.province.value!.districts!.length,
                  (index) {
                final item = model.province.value!.districts![index];
                return InkWell(
                  onTap: () {
                    model.districtController.text = item.name ?? "";
                    model.district.add(item);
                    model.ward.add(null);
                    model.wardController.text = "";
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${item.name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.district.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  void showWard(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
                  List.generate(model.district.value!.wards!.length, (index) {
                final item = model.district.value!.wards![index];
                return InkWell(
                  onTap: () {
                    model.wardController.text = item.name ?? "";
                    model.ward.add(item);
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${item.name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.ward.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  void showProvince(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
                  List.generate(model.listProvinces.value.length, (index) {
                final item = model.listProvinces.value[index];
                return InkWell(
                  onTap: () {
                    model.provinceController.text = item.name ?? "";
                    model.province.add(item);
                    model.district.add(null);
                    model.districtController.text = "";
                    model.ward.add(null);
                    model.wardController.text = "";
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${model.listProvinces.value[index].name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.province.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  String? errorText(String? text) {
    if (text?.trim().isNotEmpty != true) {
      return "Vui lòng nhập tên của bạn";
    }
    return null;
  }
}

class TextFieldCustom extends StatelessWidget {
  final String? Function(String?)? validator;
  final TextEditingController controller;
  final String? title;
  const TextFieldCustom({
    super.key,
    this.validator,
    required this.controller,
    this.title,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      style: context.tt.body2,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      validator: validator,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        labelText: title ?? "Title",
        labelStyle: context.tt.body2.copyWith(color: Colors.grey),
        errorStyle: context.tt.body2.error,
      ),
    );
  }
}

class ChildSelection extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final bool isDisable;
  const ChildSelection({
    super.key,
    required this.controller,
    required this.hintText,
    this.isDisable = true,
  });

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: isDisable ? 0.5 : 1,
      child: TextFormField(
        enabled: false,
        controller: controller,
        style: context.tt.body2,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        decoration: InputDecoration(
          labelText: hintText,
          labelStyle: context.tt.body2.copyWith(color: Colors.grey),
          errorStyle: context.tt.body2.error,
          disabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
            color: Colors.black.withOpacity(0.5),
            width: 1,
          )),
        ),
      ),
    );
  }
}
