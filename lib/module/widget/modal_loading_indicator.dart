import 'dart:ui';

import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class ModalLoadingIndicator extends StatelessWidget {
  const ModalLoadingIndicator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BackdropFilter(
      filter: ImageFilter.blur(
        sigmaX: 5.0,
        sigmaY: 5.0,
      ),
      child: const AbsorbPointer(
          child: Center(child: CircularProgressIndicator())));

  static Route? _loadingRoute;
  static showLoading(BuildContext context) {
    if (_loadingRoute != null) {
      return;
    }

    SchedulerBinding.instance.addPostFrameCallback((_) async {
      _loadingRoute = PageRouteBuilder(
          opaque: false,
          barrierColor: const Color(0x33333333),
          pageBuilder: (context, _, __) {
            return const ModalLoadingIndicator();
          });

      await Navigator.of(context).push(_loadingRoute!);
      _loadingRoute = null;
    });
  }

  static hideLoading(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (_loadingRoute != null) {
        try {
          Navigator.of(context).pop();
        } catch (err) {
          Log.i("hide loading", err);
        }
      }
    });
  }

  /// Lưu ý context phải nằm trong Scaffold để hiện snackbar lỗi
  ///
  static const _routeNAME = "_loading_popup";
  static Future<T?> showLoadingJob<T>(BuildContext context, Future<T> job,
      {bool dismissable = false,
      Stream<double>? progress,
      Duration delayed = const Duration(milliseconds: 250)}) async {
    bool done = false;
    T? result;
    await showGeneralDialog<T>(
      context: context,
      routeSettings: const RouteSettings(name: _routeNAME),
      pageBuilder: (BuildContext buildContext, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return FutureBuilder<T>(future: Future<T>(() async {
          try {
            return await job;
          } catch (err, stack) {
            Log.e(err, stack);
            rethrow;
          }
        }), builder: (futureContext, snapshot) {
          result = snapshot.data;
          if (!done) {
            final navigator = Navigator.of(context, rootNavigator: true);
            final route = ModalRoute.of<T>(buildContext)!;
            if (snapshot.hasError) {
              done = true;
              SchedulerBinding.instance.addPostFrameCallback((_) {
                final exception = RException.wrap(snapshot.error);
                final content = exception.localizedMessage;

                final snackbar = SnackBar(content: Text(content));
                try {
                  final scaffold = ScaffoldMessenger.of(context);
                  scaffold.showSnackBar(snackbar);
                  //snackbar bị lỗi, duration ko đúng, phải tự hide
                  Future.delayed(const Duration(milliseconds: 3000))
                      .then((_) => scaffold.hideCurrentSnackBar());
                } catch (e, stack) {
                  Log.e(e, stack);
                }

                if (route.isCurrent) {
                  navigator.pop();
                } else {
                  navigator.removeRoute(route);
                }
              });
            }

            if (snapshot.connectionState == ConnectionState.done) {
              done = true;
              SchedulerBinding.instance.addPostFrameCallback((_) {
                if (route.isCurrent) {
                  navigator.pop();
                } else {
                  navigator.removeRoute(route);
                }
              });
            }
          }

          return FutureBuilder(
              future: Future.delayed(delayed, () => Future.value(true)),
              builder: (context, snapshot) {
                if (snapshot.hasData == false || done) {
                  return const SizedBox();
                }
                return GestureDetector(
                  onTap: dismissable == false
                      ? null
                      : () => Navigator.of(context).pop(),
                  child: Material(
                    color: const Color(0x50000000),
                    child: Stack(
                      children: <Widget>[
                        Center(
                            child: Container(
                                width: 100,
                                height: 100,
                                padding: const EdgeInsets.all(25),
                                decoration: BoxDecoration(
                                    color: BColors.white,
                                    borderRadius: BorderRadius.circular(10)),
                                child: StreamBuilder<double?>(
                                  stream: progress ?? Stream.value(null),
                                  builder: (context, snapshot) =>
                                      CircularProgressIndicator(
                                    strokeWidth: 2,
                                    value: snapshot.data,
                                    color: BColors.textBase,
                                  ),
                                ))),
                      ],
                    ),
                  ),
                );
              });
        });
      },
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: const Color(0x01000000),
      transitionDuration: const Duration(milliseconds: 150),
    );
    return result;
  }
}
