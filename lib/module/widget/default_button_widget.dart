import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:flutter/material.dart';

class DefaultButtonWidget extends StatelessWidget {
  final Function()? onTap;
  final String title;
  final Color? bgColor;
  final TextStyle? textStyle;
  final double? heightButton;
  final double radius;
  final BoxBorder? border;
  final EdgeInsetsGeometry? padding;
  const DefaultButtonWidget({
    Key? key,
    this.onTap,
    this.title = "",
    this.bgColor,
    this.textStyle,
    this.heightButton,
    this.radius = 8,
    this.border,
    this.padding,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: padding,
        height: heightButton ?? 40,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius),
          color: bgColor ?? getColor().textBase,
          border: border,
        ),
        child: Text(
          title,
          style: textStyle ?? context.tt.sub2.white,
        ),
      ),
    );
  }
}

class BuildLineWidget extends StatelessWidget {
  final EdgeInsets? padding;
  const BuildLineWidget({
    Key? key,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 1,
      margin: padding ?? EdgeInsets.zero,
      width: double.infinity,
      color: Colors.grey,
    );
  }
}
