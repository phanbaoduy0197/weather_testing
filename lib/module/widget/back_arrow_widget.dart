import 'package:flutter/material.dart';

class BackArrowWidget extends StatelessWidget {
  final bool isIconClose;
  final Function()? onBack;
  final Alignment alignment;
  final Color? icColor;
  final double? height;
  final double? width;
  final EdgeInsets? padding;
  const BackArrowWidget({
    this.isIconClose = false,
    this.onBack,
    this.icColor,
    this.height,
    this.width,
    this.padding,
    this.alignment = Alignment.center,
    super.key,
  });
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onBack ??
          () {
            Navigator.maybePop(context);
          },
      splashColor: Colors.transparent,
      child: Container(
          width: width ?? 56,
          height: height ?? 40,
          alignment: alignment,
          padding: padding ?? const EdgeInsets.only(
            left: 8,
            right: 8,
          ),
          child: Icon(isIconClose ? Icons.close : Icons.arrow_back_ios_new)),
    );
  }
}

class BackNaviDefaultWidget extends StatelessWidget {
  final Color? icColor;
  const BackNaviDefaultWidget({
    super.key,
    this.icColor,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      onTap: () async {
        Navigator.pop(context);
      },
      child: Padding(
        padding: const EdgeInsets.only(
          left: 16,
          right: 16,
          top: 12,
          bottom: 12,
        ),
        child: Image.asset(
          "assets/images/img_chevron_left.png",
          width: 24,
          height: 24,
          color: icColor,
        ),
      ),
    );
  }
}
