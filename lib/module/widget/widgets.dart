import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/util/utils.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

class ExceptionWidget extends StatelessWidget {
  final VoidCallback? onRetry;
  final VoidCallback? onBack;
  final RException? exception;
  const ExceptionWidget(
      {super.key, this.onRetry, required this.exception, this.onBack});

  @override
  Widget build(BuildContext context) {
    final content = exception?.toErrorContent();
    return content == null
        ? const SizedBox()
        : Container(
            color: BColors.greyBase,
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      content.message,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  const Flexible(child: SizedBox(height: 20.0)),
                  onRetry == null
                      ? const SizedBox()
                      : TextButton(
                          onPressed: onRetry,
                          style: TextButton.styleFrom(
                            foregroundColor: BColors.primaryColor,
                            backgroundColor: BColors.error,
                          ),
                          child: const Text('Thử lại'),
                        ),
                ],
              ),
            ),
          );
  }
}

class SimpleExceptionWidget extends StatelessWidget {
  final RException? exception;
  final EdgeInsetsGeometry? padding;
  const SimpleExceptionWidget({
    super.key,
    required this.exception,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    final content = exception?.toErrorContent();
    return content == null
        ? const SizedBox()
        : Padding(
            padding: padding ?? const EdgeInsets.all(16.0),
            child: Center(
              child: Text(
                content.message,
                textAlign: TextAlign.center,
              ),
            ),
          );
  }
}

class RAlertFormDialogButton {
  final String title;
  // final bool isNeedValidate;
  final Color backgroundColor;
  final Color textColor;
  final Color borderColor;
  final Callback1<BuildContext>? callback;

  const RAlertFormDialogButton(
      {required this.title,
      // this.isNeedValidate = true,
      this.backgroundColor = BColors.textBase,
      this.textColor = BColors.primaryColor,
      this.borderColor = BColors.textBase,
      this.callback});

  factory RAlertFormDialogButton.cancel({String title = "Hủy"}) {
    return RAlertFormDialogButton(
        title: title,
        // isNeedValidate: false,
        backgroundColor: BColors.white,
        textColor: BColors.primaryColor,
        borderColor: BColors.greyBase,
        callback: (context) {
          Navigator.of(context).pop();
        });
  }
}

class RAlertFormDialog extends StatefulWidget {
  final String title;
  final Widget? body;
  final List<RAlertFormDialogButton> buttons;

  const RAlertFormDialog({
    super.key,
    this.title = "Rever",
    this.body,
    required this.buttons,
  });

  @override
  State<RAlertFormDialog> createState() => _RAlertFormDialogState();
}

class _RAlertFormDialogState extends State<RAlertFormDialog> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: RAlertDialog(
          title: widget.title,
          body: widget.body,
          buttonsBuilder: widget.buttons.isEmpty
              ? (context) => const SizedBox.shrink()
              : (context) => Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: widget.buttons.length == 1
                            ? MainAxisAlignment.center
                            : MainAxisAlignment.spaceBetween,
                        children: widget.buttons.mapIndexed((idx, btn) {
                          return RAlertDialogButton(
                              label: Text(btn.title),
                              backgroundColor: btn.backgroundColor,
                              textColor: btn.textColor,
                              borderColor: btn.borderColor,
                              onPressed: btn.callback != null
                                  ? () => btn.callback!(context)
                                  : null);
                        }).toList()),
                  )),
    );
  }
}

class RAlertDialog extends StatelessWidget {
  static const kIndexDimiss = -1;
  final String title;
  final String? message;
  final Widget? body;
  final bool? hasCloseIcon;
  final String dismissText;
  final List<String>? othersButton;
  final WidgetBuilder? buttonsBuilder;
  final CallbackR1<bool, int>? handleButtonTap; // return true nếu handled

  const RAlertDialog({
    super.key,
    this.title = "Rever",
    this.body,
    this.hasCloseIcon,
    this.message,
    this.dismissText = "OK",
    this.othersButton,
    this.buttonsBuilder,
    this.handleButtonTap,
  });

  static Future<bool> confirm({
    required BuildContext context,
    String title = "Rever",
    String? message,
    Widget? body,
    String cancelText = "Huỷ",
    String confirmText = "Đồng ý",
    bool? hasX,
    Color confirmButtonColor = BColors.textBase,
    Color confirmTextColor = BColors.primaryLightest,
    Function(BuildContext)? confirmAction,
    Function(BuildContext)? cancelAction,
    bool barrierDismissible = true,
  }) async {
    final result = await _show(
        context: context,
        barrierDismissible: barrierDismissible,
        builder: (context) {
          return RAlertDialog(
            title: title,
            message: message,
            body: body,
            dismissText: cancelText,
            hasCloseIcon: hasX,
            buttonsBuilder: (context) => Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RAlertDialogButton(
                      label: Text(cancelText),
                      backgroundColor: BColors.white,
                      textColor: BColors.primaryColor,
                      borderColor: BColors.greyBase,
                      onPressed: () =>
                          (cancelAction == null
                              ? null
                              : cancelAction(context)) ??
                          Navigator.of(context).pop(),
                    ),
                    RAlertDialogButton(
                      key: const Key("btn_alert_confirm"),
                      label: Text(confirmText),
                      backgroundColor: confirmButtonColor,
                      textColor: confirmTextColor,
                      disabledTextColor: BColors.white,
                      borderColor: confirmButtonColor,
                      onPressed: () =>
                          (confirmAction == null
                              ? null
                              : confirmAction(context)) ??
                          Navigator.of(context).pop(0),
                    )
                  ]),
            ),
          );
        });

    return result == 0;
  }

  static Future<int?> show({
    required BuildContext context,
    String title = "Rever",
    String? message,
    Widget? body,
    String dismissText = "Huỷ",
    List<String>? othersButton,
    bool barrierDismissible = true,
    CallbackR1<bool, int>? handleButtonTap,
  }) async {
    final result = await _show(
        context: context,
        barrierDismissible: barrierDismissible,
        builder: (context) {
          return RAlertDialog(
            title: title,
            message: message,
            body: body,
            dismissText: dismissText,
            othersButton: othersButton,
            handleButtonTap: handleButtonTap,
          );
        });

    return result;
  }

  static Future<int?> loading({
    required BuildContext context,
    String? message,
    bool barrierDismissible = true,
  }) async {
    final tt = Theme.of(context).textTheme;
    final result = await _show(
        context: context,
        barrierDismissible: barrierDismissible,
        builder: (context) {
          return RAlertDialog(
            title: "",
            body: Column(
              children: <Widget>[
                if (message != null)
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 20.0, left: 16, right: 16),
                    child: Text(
                      message,
                      style: tt.body1.copyWith(
                          color: BColors.textBase, fontSize: 16, height: 1.5),
                    ),
                  ),
                const Padding(
                  padding: EdgeInsets.only(top: 15, bottom: 23),
                  child: SizedBox(
                    height: 16,
                    width: 16,
                    child: CircularProgressIndicator(
                      strokeWidth: 2,
                      valueColor:
                          AlwaysStoppedAnimation<Color>(BColors.textBase),
                    ),
                  ),
                ),
              ],
            ),
            buttonsBuilder: (context) => const SizedBox(),
          );
        });

    return result;
  }

// Dùng method này để show do cần custom lại barrier color
  static Future<T?> _show<T>({
    required BuildContext context,
    bool barrierDismissible = true,
    required WidgetBuilder builder,
  }) {
    final ThemeData theme = Theme.of(context);
    return showGeneralDialog(
      context: context,
      pageBuilder: (BuildContext buildContext, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        final Widget pageChild = Builder(builder: builder);
        return SafeArea(
          child: Builder(builder: (BuildContext context) {
            // return theme != null
            // ? Theme(data: theme, child: pageChild)
            return Theme(data: theme, child: pageChild);
            // return pageChild;
          }),
        );
      },
      barrierDismissible: barrierDismissible,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: BColors.greyLightest,
      transitionDuration: const Duration(milliseconds: 150),
      transitionBuilder: (_, animation, __, child) => FadeTransition(
        opacity: CurvedAnimation(
          parent: animation,
          curve: Curves.easeOut,
        ),
        child: child,
      ),
      useRootNavigator: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    final buttons = <String>[dismissText, ...(othersButton ?? [])];
    const radius = 8.0;
    final hasTitle = title.isNotEmpty == true;
    final hasBody = body != null;
    final hasX = hasCloseIcon == true;
    return Dialog(
        backgroundColor: Colors.transparent,
        elevation: 4.0,
        child: Container(
          decoration: BoxDecoration(
            color: BColors.white,
            // shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(radius),
          ),
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                if (hasX)
                  Row(
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Padding(
                          padding: EdgeInsets.only(
                              left: 16, right: 16, bottom: 12, top: 12),
                          child: Icon(
                            Icons.close,
                            size: 24,
                          ),
                        ),
                      ),
                    ],
                  ),
                if (!hasX)
                  const SizedBox(
                    height: 20,
                  ),
                if (hasTitle)
                  Container(
                    width: double.infinity,
                    padding:
                        const EdgeInsets.only(left: 16, right: 16, bottom: 12),
                    child: Text(title, style: context.tt.h3),
                  ),
                if (message != null)
                  Padding(
                    padding: EdgeInsets.only(
                        left: 24,
                        right: 24,
                        top: (hasTitle ? 0 : 20),
                        bottom: 20),
                    child: Text(
                      message!,
                      // textAlign: TextAlign.center,
                      style: context.tt.body2,
                    ),
                  ),
                ...(hasBody
                    ? [
                        body!,
                      ]
                    : []),
                Container(color: BColors.border, height: 1),
                buttonsBuilder == null
                    ? buildButtons(buttons, radius, context)
                    : buttonsBuilder!(context),
              ],
            ),
          ),
        ));
  }

  void _onButtonTap(BuildContext context, int idx) {
    if (handleButtonTap != null && handleButtonTap!(idx) == true) return;

    Navigator.of(context).pop(idx);
  }

  Widget buildButtons(
      List<String> buttons, double radius, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: buttons.length == 1
              ? MainAxisAlignment.center
              : MainAxisAlignment.spaceBetween,
          children: buttons.map((txt) {
            int index = buttons.indexOf(txt);
            bool isPrimary = index == buttons.length - 1;
            return RAlertDialogButton(
              label: Text(txt),
              radius: radius,
              backgroundColor:
                  isPrimary ? BColors.textBase : BColors.primaryColor,
              textColor: isPrimary ? BColors.textBase : BColors.primaryColor,
              borderColor: isPrimary ? BColors.textBase : BColors.greyBase,
              onPressed: () => _onButtonTap(context, index - 1),
              disabledTextColor: BColors.white,
            );
          }).toList()),
    );
  }
}

class RAlertDialogButton extends StatelessWidget {
  final double radius;
  final Widget label;
  final VoidCallback? onPressed;
  final Color backgroundColor;
  final Color textColor;
  final Color disabledTextColor;
  final Color borderColor;

  const RAlertDialogButton(
      {super.key,
      required this.label,
      this.radius = 8.0,
      required this.onPressed,
      this.backgroundColor = BColors.greyLightest,
      this.textColor = BColors.textBase,
      this.borderColor = BColors.border,
      this.disabledTextColor = BColors.greyLightest});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      decoration: BoxDecoration(
        color: backgroundColor,
        border: Border.all(color: borderColor, width: 0.5),
        borderRadius: BorderRadius.all(Radius.circular(radius)),
      ),
      child: TextButton(
        style: TextButton.styleFrom(
          disabledForegroundColor: disabledTextColor,
          foregroundColor: textColor,
          // textStyle: Mater,
          // primary: textColor,
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 4),
        ),
        onPressed: onPressed,
        child: label,
      ),
    );
  }
}

class CircleInkWell extends StatelessWidget {
  final VoidCallback? onTap;
  final VoidCallback? onTapDown;
  final Widget child;
  final Color? backgroundColor;

  const CircleInkWell(
      {super.key,
      this.onTap,
      this.onTapDown,
      this.backgroundColor,
      required this.child});

  @override
  Widget build(BuildContext context) {
    return ClipOval(
        child: Material(
            color: backgroundColor,
            child: InkWell(
              onTap: onTap,
              onTapDown: (_) => onTapDown?.call(),
              child: child,
            )));
  }
}

BoxConstraints minHeightBoxConstraints(double minHeight) => BoxConstraints(
    minWidth: 0,
    maxWidth: double.infinity,
    minHeight: minHeight,
    maxHeight: double.infinity);

BoxConstraints maxHeightBoxConstraints(double maxHeight) => BoxConstraints(
    minWidth: 0, maxWidth: double.infinity, minHeight: 0, maxHeight: maxHeight);
