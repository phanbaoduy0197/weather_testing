import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/util/utils.dart';
import 'package:base_entities/module/auth/model/field_data_option.dart';
import 'package:base_entities/module/widget/bottom_sheet.dart';
import 'package:base_entities/module/widget/default_button_widget.dart';
import 'package:flutter/material.dart';

class RBottomSheetMenu extends StatelessWidget {
  final List<Tuple3<int, Widget?, Widget>> menu;
  final Widget? header;

  const RBottomSheetMenu({super.key, required this.menu, this.header});

  ///
  /// show menu lựa chọn với dạng bottom sheet
  /// Data là Tuple2 bao gôm:
  ///  + idx
  ///  + text
  /// Trả về Future menu idx, null nếu bấm ra ngoài
  ///
  static Future<int?> showWithText({
    required BuildContext context,
    required List<Tuple2<int, String>> menu,
    String? title,
  }) async {
    menuTransformer(Tuple2<int, String> item) =>
        Tuple3(item.item1, null, Text(item.item2));

    return RBottomSheet.show(
        context: context,
        builder: (context) => RBottomSheetMenu(
              menu: menu.map(menuTransformer).toList(),
              header: _menuTitleTransformer(context, title),
            ));
  }

  ///
  /// show menu lựa chọn với dạng bottom sheet
  /// Data là Tuple3 bao gôm:
  ///  + idx
  ///  + icon
  ///  + text
  /// Trả về Future menu idx, null nếu bấm ra ngoài
  ///
  static Future<int?> showWithIcon({
    required BuildContext context,
    required List<Tuple3<int, IconData, String>> menu,
    String? title,
  }) async {
    menuTransformer(Tuple3<int, IconData, String> item) =>
        Tuple3(item.item1, Icon(item.item2, size: 20), Text(item.item3));

    return RBottomSheet.show(
        context: context,
        builder: (context) => RBottomSheetMenu(
              menu: menu.map(menuTransformer).toList(),
              header: _menuTitleTransformer(context, title),
            ));
  }

  @override
  Widget build(BuildContext context) {
    return RBottomSheet(
      header: header,
      body: Column(
        children: <Widget>[
          for (var i = 0; i < menu.length; i++) ...[
            _MenuRow(item: menu[i]),
            if (i < menu.length - 1) const BuildLineWidget(),
          ],
        ],
      ),
    );
  }
}

_menuTitleTransformer(BuildContext context, String? title) => title == null
    ? null
    : Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Text(title, style: context.tt.h4),
      );

class _MenuRow extends StatelessWidget {
  final Tuple3<int, Widget?, Widget> item;
  const _MenuRow({required this.item});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => Navigator.of(context).pop(item.item1),
      child: SizedBox(
        height: 47,
        child: Row(
          children: <Widget>[
            const SizedBox(width: 16),
            item.item2 != null
                ? Padding(
                    padding: const EdgeInsets.only(right: 12),
                    child: item.item2)
                : const SizedBox(),
            Flexible(child: item.item3),
            const SizedBox(width: 16),
          ],
        ),
      ),
    );
  }
}

class RGroupedBottomSheetMenu extends StatelessWidget {
  final List<Tuple3<int, Widget?, Widget>> menuGroup1;
  final List<Tuple3<int, Widget?, Widget>> menuGroup2;
  final Widget? header;

  const RGroupedBottomSheetMenu(
      {super.key,
      required this.menuGroup1,
      required this.menuGroup2,
      this.header});

  ///
  /// show menu lựa chọn với dạng bottom sheet
  /// Data là Tuple3 bao gôm:
  ///  + idx
  ///  + icon
  ///  + text
  /// Trả về Future menu idx, null nếu bấm ra ngoài
  ///
  static Future<int?> showWithIcon({
    required BuildContext context,
    required List<Tuple3<int, IconData, String>> menuGroup1,
    required List<Tuple3<int, IconData, String>> menuGroup2,
    String? title,
  }) async {
    menuTransformer(Tuple3<int, IconData, String> item) =>
        Tuple3(item.item1, Icon(item.item2, size: 20), Text(item.item3));

    return RBottomSheet.show(
        context: context,
        builder: (context) => RGroupedBottomSheetMenu(
              menuGroup1: menuGroup1.map(menuTransformer).toList(),
              menuGroup2: menuGroup2.map(menuTransformer).toList(),
              header: _menuTitleTransformer(context, title),
            ));
  }

  Widget buildGroup(
      BuildContext context, List<Tuple3<int, Widget?, Widget>> menu) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: BColors.greyLightest,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            for (var i = 0; i < menu.length; i++) ...[
              _MenuRow(item: menu[i]),
              if (i < menu.length - 1) const BuildLineWidget(),
            ],
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return RBottomSheet(
      header: header,
      bodyPadding: header == null
          ? const EdgeInsets.only(top: 20)
          : const EdgeInsets.only(top: 16),
      body: Column(
        children: <Widget>[
          buildGroup(context, menuGroup1),
          const SizedBox(height: 16),
          buildGroup(context, menuGroup2),
        ],
      ),
    );
  }
}

class RGroupedBottomSheetMenu1 extends StatelessWidget {
  final Map<String, List<RFieldDataOption>> map;
  final String selected;
  final Widget? header;

  const RGroupedBottomSheetMenu1(
      {super.key, required this.map, this.header, required this.selected});

  ///
  /// show menu lựa chọn với dạng bottom sheet
  /// Data là Tuple3 bao gôm:
  ///  + idx
  ///  + icon
  ///  + text
  /// Trả về Future menu idx, null nếu bấm ra ngoài
  ///
  static Future<String?> showWithIcon(
      {required BuildContext context,
      required Map<String, List<RFieldDataOption>> map,
      String? title,
      required String selected}) async {
    return RBottomSheet.show(
        context: context,
        builder: (context) => RGroupedBottomSheetMenu1(
              map: map,
              header: _menuTitleTransformer(context, title),
              selected: selected,
            ));
  }

  @override
  Widget build(BuildContext context) {
    return RBottomSheet(
      header: header,
      bodyPadding: header == null
          ? const EdgeInsets.only(top: 20)
          : const EdgeInsets.only(top: 16),
      body: Container(
        margin: const EdgeInsets.only(left: 16),
        width: MediaQuery.of(context).size.width,
        child: SingleChildScrollView(
          child: Column(
              children: map.keys
                  .map((e) => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            e,
                            style: context.tt.sub2.greyLight,
                          ),
                          ...map[e]!.map((e) => InkWell(
                                onTap: () {
                                  Navigator.of(context).pop(e.internalValue);
                                },
                                child: Container(
                                    margin: const EdgeInsets.only(
                                        left: 16, right: 16),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            const SizedBox(
                                              height: 12,
                                            ),
                                            Text(e.name ?? ''),
                                            const SizedBox(
                                              height: 12,
                                            ),
                                            const Divider(
                                              color: BColors.border,
                                            ),
                                          ],
                                        ),
                                        e.internalValue == selected
                                            ? Image.asset(
                                                "assets/images/img_check_box_checked_1.png",
                                                width: 20,
                                                height: 20,
                                              )
                                            : const SizedBox(),
                                      ],
                                    )),
                              ))
                        ],
                      ))
                  .toList()),
        ),
      ),
    );
  }
}
