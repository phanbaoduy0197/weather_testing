import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:flutter/material.dart';

class RBottomSheet extends StatelessWidget {
  final Widget? header;
  final Widget body;
  final EdgeInsets contentPadding;
  final EdgeInsets bodyPadding;
  const RBottomSheet({
    super.key,
    this.header,
    required this.body,
    this.contentPadding = const EdgeInsets.only(top: 8),
    this.bodyPadding = const EdgeInsets.all(16),
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      minimum: const EdgeInsets.only(bottom: 20),
      child: Container(
          width: double.infinity,
          padding: contentPadding,
          child: Column(mainAxisSize: MainAxisSize.min, children: [
            InkWell(
              onTap: () => Navigator.of(context).pop(),
              child: Container(
                width: 40,
                height: 6,
                decoration: BoxDecoration(
                  color: BColors.greyLight,
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
            ),
            if (header != null) header!,
            Flexible(
              child: SingleChildScrollView(
                padding: bodyPadding,
                child: body,
              ),
            )
          ])),
    );
  }

  static Future<T?> show<T>({
    required BuildContext context,
    required WidgetBuilder builder,
  }) {
    return showModalBottomSheet(
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0)),
        ),
        backgroundColor: Colors.white,
        context: context,
        isScrollControlled: true,
        constraints: BoxConstraints(
            maxHeight: MediaQuery.of(context).size.height * 0.9,
            minHeight: 100),
        builder: builder);
  }
}

class BottomSheetIcon extends StatelessWidget {
  const BottomSheetIcon({
    super.key,
    this.icon,
    this.title,
    this.description,
    this.actions = const [],
  });

  final IconData? icon;
  final String? title;
  final String? description;
  final List<Widget> actions;

  @override
  Widget build(BuildContext context) {
    return RBottomSheet(
      contentPadding: const EdgeInsets.only(top: 8.0),
      bodyPadding: const EdgeInsets.only(top: 12.0),
      body: Align(
        alignment: Alignment.topLeft,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (icon != null)
                    Container(
                      padding: const EdgeInsets.all(12.0),
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: BColors.greyLightest,
                      ),
                      child: Icon(
                        icon!,
                        size: 32.0,
                      ),
                    ),
                  if (title?.isNotEmpty == true)
                    Padding(
                      padding: const EdgeInsets.only(top: 24.0),
                      child: Text(
                        title!,
                        style: context.tt.h3,
                      ),
                    ),
                  if (description?.isNotEmpty == true)
                    Padding(
                      padding: const EdgeInsets.only(top: 24.0),
                      child: Text(
                        description!,
                        style: context.tt.body1,
                      ),
                    ),
                ],
              ),
            ),
            if (actions.isNotEmpty)
              Container(
                padding: const EdgeInsets.all(16.0),
                decoration: const BoxDecoration(
                  border: Border(
                    top: BorderSide(color: BColors.border),
                  ),
                ),
                child: Row(
                  children: List.generate(actions.length * 2 - 1, (index) {
                    if (index % 2 == 0) {
                      final idx = (index / 2).truncate();
                      return Expanded(child: actions[idx]);
                    }
                    return const SizedBox(width: 10.0);
                  }),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
