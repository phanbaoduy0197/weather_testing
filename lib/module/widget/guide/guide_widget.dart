import 'package:base_entities/module/widget/guide/guide_overlay_widget.dart';
import 'package:base_entities/module/widget/guide/guide_type_enum.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:visibility_detector/visibility_detector.dart';

class GuideWidget extends StatefulWidget {
  final Widget child;
  final GuideType type;

  const GuideWidget({
    super.key,
    required this.child,
    required this.type,
  });

  @override
  State<GuideWidget> createState() => _GuideWidgetState();
}

class _GuideWidgetState extends State<GuideWidget> {
  late final GuideWidgetController controller;

  @override
  void initState() {
    super.initState();
    controller = context.read<GuideWidgetController>();
  }

  @override
  void dispose() {
    controller.remove(type: widget.type);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return VisibilityDetector(
      onVisibilityChanged: (visibilityInfo) {
        if (visibilityInfo.visibleFraction >= 1.0) {
          controller.add(
              type: widget.type,
              getRect: () {
                const insets = EdgeInsets.all(16);
                final rb = context.findRenderObject() as RenderBox;
                final loc = rb.localToGlobal(Offset.zero);
                return Rect.fromLTRB(
                  loc.dx - insets.left,
                  loc.dy - insets.top,
                  loc.dx + rb.size.width + insets.right,
                  loc.dy + rb.size.height + insets.bottom,
                );
              });
        } else {
          controller.remove(type: widget.type);
        }
      },
      key: Key(widget.type.name),
      child: widget.child,
    );
  }
}
