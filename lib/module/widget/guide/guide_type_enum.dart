import 'package:base_entities/basic/misc/extension.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:flutter/material.dart';

extension GuideTypeExt on int {
  GuideType getGuideType() {
    switch (this) {
      case 0:
        return GuideType.homeRefresh;
      case 1:
        return GuideType.homeField;
      default:
        return GuideType.homeRefresh;
    }
  }
}

enum GuideType {
  homeRefresh,
  homeField;

  int getOrder() {
    switch (this) {
      case homeRefresh:
        return 0;
      case homeField:
        return 1;
    }
  }

  Alignment getAlignment() {
    switch (this) {
      case homeRefresh:
        return Alignment.bottomRight;
      case homeField:
        return Alignment.topCenter;
    }
  }

  String getText() {
    switch (this) {
      case homeRefresh:
        return "Nhấn để cập nhật lại bảng thời tiết theo vị trí hiện tại của bạn";
      case homeField:
        return "Nhập tên thành phố bạn muốn tìm ở đây, hoặc chọn trong danh sách ở bên phải và nhấn tìm kiếm";
    }
  }

  Widget? customWidget(BuildContext context) {
    switch (this) {
      case homeRefresh:
        return Container(
          width: context.w / 1.3,
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: getColor().white,
          ),
          child: Text(
            homeRefresh.getText(),
            style: context.tt.body1,
            textAlign: TextAlign.justify,
          ),
        );
      case homeField:
        return Container(
          width: context.w / 1.3,
          padding: const EdgeInsets.all(12),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            color: getColor().white,
          ),
          child: Text(
            homeField.getText(),
            style: context.tt.body1,
            textAlign: TextAlign.justify,
          ),
        );
      default:
        return null;
    }
  }
}
