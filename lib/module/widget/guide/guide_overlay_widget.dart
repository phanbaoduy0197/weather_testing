import 'dart:math';
import 'package:base_entities/basic/misc/extension.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/widget/guide/guide_parameter.dart';
import 'package:base_entities/module/widget/guide/guide_type_enum.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:collection/collection.dart';

abstract class GuideWidgetController {
  void add({required GuideType type, required Rect Function() getRect});

  void remove({required GuideType type});
}

class GuideOverlayWidget extends StatefulWidget {
  final Widget child;

  const GuideOverlayWidget({super.key, required this.child});

  @override
  State<GuideOverlayWidget> createState() => _GuideOverlayWidgetState();
}

class _GuideOverlayWidgetState extends State<GuideOverlayWidget>
    implements GuideWidgetController {
  final parameter = locator.get<GuideParameter>();
  bool scrolling = false;
  bool hasVisibleGuide = false;

  // Thêm hướng dẫn cần hiển thị
  // guideContext: context của widget cần hiển thị hướng dẫn, dùng để tính vị trí
  @override
  void add({required GuideType type, required RectGetter getRect}) {
    if (!mounted) return;

    if (!_canShown(type)) {
      return;
    }

    parameter.addEntries(type, getRect);

    if (parameter.guideEntries.isEmpty) return;

    sorter(GuideEntry a, GuideEntry b) {
      return b.type.getOrder().compareTo(a.type.getOrder());
    }

    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        parameter.guideEntries.sort(sorter);
      });
    });
  }

  // Remove hướng dẫn cần hiển thị đi
  @override
  void remove({required GuideType type}) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        parameter.removeGuideEntries(type);
      });
    });
  }

  // Check xem Guide đã được hiển thị chưa, nếu đã hiển thị rồi thì trả về false
  bool _canShown(GuideType type) {
    if (!parameter.guideSaved.contains(type)) {
      return true;
    }
    return false;
  }

  int _deleyToken = 0;
  bool onScroll(ScrollNotification notif) {
    if (notif is ScrollStartNotification) {
      _deleyToken = 0;
      if (hasVisibleGuide) {
        WidgetsBinding.instance.addPostFrameCallback((_) {
          setState(() {
            scrolling = true;
          });
        });
      } else {
        scrolling = false;
      }
    } else if (notif is ScrollEndNotification) {
      if (parameter.guideEntries.isNotEmpty == true) {
        final token = DateTime.now().millisecondsSinceEpoch;
        _deleyToken = token;
        Future.delayed(const Duration(seconds: 1)).then((value) {
          if (_deleyToken != token) return;
          WidgetsBinding.instance.addPostFrameCallback((_) {
            setState(() {
              scrolling = false;
            });
          });
        });
      } else {
        scrolling = false;
      }
    }
    return false;
  }

  GuideEntry? getEntry() {
    if (scrolling == false) {
      // nếu muốn custom thứ tự hiển thị thì sửa ở đây
      return parameter.guideEntries
          .sorted((a, b) => a.type.getOrder().compareTo(b.type.getOrder()))
          .firstOrNull;
    }

    return null;
  }

  void next(GuideType type) {
    // đánh dấu hiện rồi
    parameter.setGuideType(type);
    // set state để nó render cái tiếp
    setState(() {
      parameter.removeGuideEntries(type);
    });
  }

  @override
  Widget build(BuildContext context) {
    final entry = getEntry();
    hasVisibleGuide = getEntry() != null;
    return Provider<GuideWidgetController>.value(
      value: this,
      child: Stack(
        children: [
          NotificationListener<ScrollNotification>(
              onNotification: onScroll, child: widget.child),
          if (entry != null)
            GestureDetector(
              key: const Key("viw_guide_bg"),
              onTap: () => next(entry.type),
              child: Container(
                color: const Color(0x01000000),
              ),
            ),
          AnimatedSwitcher(
            duration: const Duration(milliseconds: 250),
            switchInCurve: Curves.easeIn,
            switchOutCurve: Curves.easeOut,
            child: entry == null
                ? const SizedBox()
                : _OverlayWidget(
                    type: entry.type,
                    rect: entry.rectGetter(),
                  ),
          ),
        ],
      ),
    );
  }
}

class _OverlayWidget extends StatelessWidget {
  final GuideType type;
  final Rect rect;

  const _OverlayWidget({
    required this.type,
    required this.rect,
  });

  @override
  Widget build(BuildContext context) {
    final mq = MediaQuery.of(context);

    final ss = mq.size;
    double left = 0, top = 0, right = 0, bottom = 0;
    const padding = 79.0;
    Alignment alignment;
    var columnAligment = CrossAxisAlignment.center;
    bool textTop = false;

    if (type.getAlignment() == Alignment.bottomRight) {
      // hiện ở dưới bên phải
      // text dưới arrow
      right = ss.width - rect.right + rect.width / 2 - _ArrowWidget.x;
      left = max(padding - right, 12);
      top = rect.bottom - 16;
      alignment = Alignment.topRight;
      columnAligment = CrossAxisAlignment.end;
    } else if (type.getAlignment() == Alignment.bottomLeft) {
      // hiện ở dưới bên trái
      // text ở dưới arrow
      left = rect.left;
      right = max(padding - left, 12);
      top = rect.bottom - 16;
      alignment = Alignment.topLeft;
    } else if (type.getAlignment() == Alignment.bottomCenter) {
      // hiện ở dưới chính giữa
      // text ở dưới arrow
      left = rect.left;
      right = ss.width - rect.right;
      top = rect.bottom - 16;

      alignment = Alignment.topCenter;
    } else if (type.getAlignment() == Alignment.topRight) {
      // hiện ở trên bên phải
      // text ở trên arrow
      right = ss.width - rect.right + rect.width / 2 - _ArrowWidget.x;
      left = max(padding - right, 12);
      bottom = ss.height - rect.top - 16;
      alignment = Alignment.bottomRight;
      columnAligment = CrossAxisAlignment.end;
      textTop = true;
    } else if (type.getAlignment() == Alignment.topLeft) {
      // hiện ở trên bên trái
      // text ở trên arrow
      left = rect.left;
      right = max(padding - left, 12);
      bottom = ss.height - rect.top - 16;
      alignment = Alignment.bottomLeft;
      textTop = true;
    } else if (type.getAlignment() == Alignment.topCenter) {
      // hiện ở trên chính giữa
      // text ở trên arrow
      left = right = padding / 2;
      bottom = ss.height - rect.top - 16;
      alignment = Alignment.bottomCenter;
      textTop = true;
      columnAligment = CrossAxisAlignment.start;
    } else {
      assert(false, "unhandled");
      left = right = padding / 2;

      if (rect.top < ss.height / 4) {
        top = rect.bottom + 12;
        alignment = Alignment.topCenter;
      } else {
        bottom = ss.height - rect.top + 12;
        alignment = Alignment.bottomCenter;
      }
    }

    var children = [
      _ArrowWidget(type: type),
      _GuideText(type: type),
    ];
    if (textTop) {
      children = children.reversed.toList();
    }

    return Provider.value(
      value: type,
      child: Stack(
        children: [
          IgnorePointer(
            child: Scaffold(
              backgroundColor: Colors.transparent,
              body: Stack(
                children: [
                  ClipPath(
                    clipper: GuideBackgroundClipper(
                      childRect: rect,
                      radius: const Radius.circular(8),
                    ),
                    child: Container(
                      color: const Color(0xAA000000),
                    ),
                  ),
                  Align(
                    alignment: alignment,
                    child: Padding(
                        padding: EdgeInsets.only(
                          top: top,
                          left: left,
                          right: right,
                          bottom: bottom,
                        ),
                        child: Column(
                          crossAxisAlignment: columnAligment,
                          mainAxisSize: MainAxisSize.min,
                          children: children,
                        )),
                  ),
                ],
              ),
            ),
          ),
          //--------Add them custom widget builder neu can thiet--------
        ],
      ),
    );
  }
}

class GuideBackgroundClipper extends CustomClipper<Path> {
  final Rect? childRect;
  final Radius? radius;

  GuideBackgroundClipper({this.childRect, this.radius});

  @override
  Path getClip(Size size) {
    final p1 = Path()..addRect(Rect.fromLTWH(0, 0, size.width, size.height));

    final p2 = Path()
      ..addRRect(RRect.fromRectAndRadius(
        childRect!,
        radius!,
      ));
    return Path.combine(PathOperation.difference, p1, p2);
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => true;
}

/// Widget hiện text của 1 entry
class _GuideText extends StatelessWidget {
  final GuideType type;

  const _GuideText({required this.type});

  @override
  Widget build(BuildContext context) {
    final ts = context.tt.body1;

    if (type.customWidget(context) != null) {
      return type.customWidget(context)!;
    }

    return Container(
      width: context.w / 1.6,
      padding: const EdgeInsets.only(top: 8, bottom: 8, left: 16, right: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: getColor().white,
      ),
      child: Text(
        type.getText(),
        style: ts,
        textAlign: TextAlign.justify,
      ),
    );
  }
}

///
/// Widget 1 đầu là vòng tròn đỏ, 1 đầu là vòng tròn trắng
/// có line nối 2 vòng tròn
class _ArrowWidget extends StatelessWidget {
  final GuideType type;

  const _ArrowWidget({required this.type});
  static const x = 25.0;
  static const y = 15.0;

  @override
  Widget build(BuildContext context) {
    const height = 40.0;
    final alignment = type.getAlignment();
    double tailTop = 0;
    double? tailLeft, tailRight;
    bool down = true;
    AlignmentGeometry stackAlginment = AlignmentDirectional.topStart;
    if (alignment == Alignment.topRight) {
      tailRight = x / 2;
      stackAlginment = AlignmentDirectional.bottomEnd;
    } else if (alignment == Alignment.bottomLeft) {
      down = false;
      tailTop = height - x / 2;
      tailLeft = x / 2;
    } else if (alignment == Alignment.bottomRight) {
      down = false;
      tailTop = height - x / 2;
      tailRight = x / 2;
      stackAlginment = AlignmentDirectional.topEnd;
    } else if (alignment == Alignment.bottomCenter) {
      down = false;
      tailTop = height - x / 2;
      stackAlginment = AlignmentDirectional.center;
    } else if (alignment == Alignment.topCenter) {
      stackAlginment = AlignmentDirectional.center;
    } else if (alignment == Alignment.topLeft) {
      tailLeft = x / 2;

      stackAlginment = AlignmentDirectional.topStart;
    }

    return SizedBox(
      height: height,
      child: Stack(
        alignment: stackAlginment,
        children: [
          Positioned(
              top: tailTop,
              left: tailLeft,
              right: tailRight,
              width: x,
              height: y,
              child: _triangleDown(down: down))
        ],
      ),
    );
  }

  Widget _triangleDown({bool down = true}) {
    return Transform.rotate(
        angle: down ? pi : pi / 180,
        child: CustomPaint(
          painter: TrianglePainter(),
        ));
  }
}

class TrianglePainter extends CustomPainter {
  final Color color;

  TrianglePainter({this.color = Colors.white});

  @override
  void paint(Canvas canvas, Size size) {
    double max = 1000000;

    double point0x = size.width;
    double point0y = size.height;

    double point1x = size.width / max;
    double point1y = size.height;

    double point2x = size.width / 2;
    double point2y = size.height / max;

    Path path = Path();
    path.moveTo(point0x, point0y);
    path.lineTo(point1x, point1y);
    path.lineTo(point2x, point2y);
    path.lineTo(point0x, point0y);
    path.close();
    canvas.drawPath(
        path,
        Paint()
          ..color = color
          ..style = PaintingStyle.fill);

    canvas.save();
    canvas.restore();
  }

  @override
  bool shouldRepaint(TrianglePainter oldDelegate) {
    return oldDelegate.color != color;
  }
}
