import 'package:base_entities/basic/misc/disposable.dart';
import 'package:base_entities/data/preferences/preferences.dart';
import 'package:base_entities/module/widget/guide/guide_type_enum.dart';
import 'package:flutter/material.dart';

class GuideParameter extends Disposable {
  final List<GuideType> guideSaved = [];
  List<GuideEntry> guideEntries = [];

  Future init() async {
    final res = await Preferences.getGuideParameter();
    guideSaved.clear();
    guideSaved.addAll(res.map((e) => e.getGuideType()).toList());
  }

  void removeGuideEntries(GuideType type) {
    final ixW = guideEntries.indexWhere((e) => e.type == type);
    if (ixW != -1) {
      guideEntries.removeAt(ixW);
    }
  }

  void addEntries(GuideType type, RectGetter rectGetter) {
    guideEntries.add(GuideEntry(type, rectGetter));
    guideEntries =
        guideEntries.where((e) => !guideSaved.contains(e.type)).toList();
  }

  Future setGuideType(GuideType type) async {
    if (guideSaved.contains(type)) {
      return;
    }
    guideSaved.add(type);
    return Preferences.setGuideParameter(
        guideSaved.map((e) => e.getOrder()).toList());
  }

  @override
  void dispose() {}
}

typedef RectGetter = Rect Function();

class GuideEntry {
  final GuideType type;
  final RectGetter rectGetter;

  GuideEntry(this.type, this.rectGetter);
}
