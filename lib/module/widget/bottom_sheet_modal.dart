import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/module/widget/back_arrow_widget.dart';
import 'package:flutter/material.dart';

class BottomSheetModal extends StatelessWidget {
  final String? title;
  final Widget? left;
  final Widget? right;
  final Widget contentWidget;
  final double? height;
  final TextStyle? titleStyle;
  final EdgeInsets contentPadding;
  final double? heightAppBar;
  final bool expanded;

  const BottomSheetModal({
    super.key,
    required this.contentWidget,
    this.title,
    this.left,
    this.right,
    this.height = 280,
    this.titleStyle,
    this.contentPadding = const EdgeInsets.symmetric(horizontal: 16),
    this.heightAppBar = 62,
    this.expanded = true,
  });

  static Future<T?> show<T>({
    required BuildContext context,
    required WidgetBuilder builder,
    String? title,
    Widget? titleLeftWidget,
    Widget? titleRightWidget,
    bool expanded = true,
  }) {
    final left = titleLeftWidget ?? const BackArrowWidget(isIconClose: true);
    final mq = MediaQuery.of(context);
    return showModalBottomSheet<T>(
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(top: Radius.circular(20.0)),
        ),
        backgroundColor: Colors.white,
        isScrollControlled: true,
        constraints: BoxConstraints(maxHeight: mq.size.height * 0.9),
        builder: (context) {
          return BottomSheetModal(
            contentWidget: builder(context),
            title: title,
            left: left,
            right: titleRightWidget,
            contentPadding: EdgeInsets.zero,
            height: null,
            expanded: expanded,
          );
        });
  }

  bool get _hasHeader {
    return left != null || right != null || title != null;
  }

  Widget _buildContentWidget(BuildContext context) {
    final child = Padding(
      padding: contentPadding,
      child: contentWidget,
    );
    if (expanded) {
      return Expanded(child: child);
    }

    return Flexible(child: child);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: SafeArea(
        minimum: const EdgeInsets.only(bottom: 20),
        child: Container(
          height: height,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(12), topRight: Radius.circular(12)),
            color: Colors.white,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (_hasHeader) const SizedBox(height: 8),
              if (_hasHeader)
                Container(
                  alignment: Alignment.center,
                  height: heightAppBar,
                  child: Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      if (left != null)
                        Align(
                          alignment: Alignment.centerLeft,
                          child: left,
                        ),
                      if (title != null)
                        Center(
                          child: Text(title ?? "",
                              style: titleStyle ?? context.tt.h4),
                        ),
                      if (right != null)
                        Align(
                          alignment: Alignment.centerRight,
                          child: right,
                        ),
                    ],
                  ),
                ),
              _buildContentWidget(context),
            ],
          ),
        ),
      ),
    );
  }
}
