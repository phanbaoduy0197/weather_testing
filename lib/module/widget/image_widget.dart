import 'package:base_entities/basic/misc/cache_manager.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/di/locator.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class RoundNetworkImage extends StatelessWidget {
  final String? url;
  final double height;
  final double width;
  final double radius;
  final Widget? errorHolder;
  final dynamic placeholder;
  final BoxFit? boxFit;
  final Color? strokeColor;
  final double? strokeWidth;
  final bool isHero;
  final String tagHero;
  final BorderRadius? borderRadius;
  final ImageResolution imageResolution;
  final bool isAddTs;
  final int? rotate;

  const RoundNetworkImage({
    required this.width,
    required this.height,
    this.url,
    this.radius = 0.0,
    this.placeholder,
    this.errorHolder,
    this.boxFit,
    this.strokeColor,
    this.strokeWidth,
    this.isHero = false,
    this.tagHero = "",
    this.borderRadius,
    this.isAddTs = false,
    this.rotate,
    this.imageResolution = ImageResolution.medium,
    Key? key,
  }) : super(key: key);

  static Future preloadUrl(BuildContext context, String? url) async {
    if (url == null) return Future.value(null);
    final cacheMgr = locator.get<RCacheManager>();
    return cacheMgr.getSingleFile(url);
  }

  static Future preload(BuildContext context, MultiResolutionImage image,
      [List<ImageResolution> resolutions = const [
        ImageResolution.small,
        ImageResolution.medium,
        ImageResolution.large
      ]]) {
    return Future.wait([
      if (resolutions.contains(ImageResolution.small))
        preloadUrl(context, image.smallUrl),
      if (resolutions.contains(ImageResolution.medium))
        preloadUrl(context, image.url),
      if (resolutions.contains(ImageResolution.large))
        preloadUrl(context, image.largeUrl),
    ]);
  }

  @override
  Widget build(BuildContext context) {
    if (url == null || url!.isEmpty) {
      return _placeHolder(context);
    }
    String? tsUrl;
    if (isAddTs) {
      tsUrl = "$url?ts=${DateTime.now().millisecondsSinceEpoch}";
    }
    Widget content = CachedNetworkImage(
      cacheManager: locator.get<RCacheManager>(),
      imageUrl: tsUrl ?? url!,
      imageBuilder: (context, imageProvider) => Container(
        height: height,
        width: width,
        decoration: BoxDecoration(
          borderRadius:
              borderRadius ?? BorderRadius.all(Radius.circular(radius)),
          border: Border.all(
              color: strokeColor ?? Colors.transparent,
              width: strokeWidth ?? 0),
          image: DecorationImage(
            image: imageProvider,
            fit: boxFit ?? BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => _placeHolder(context),
      errorWidget: _buildError,
      // memCacheWidth: getMemCacheWidth(),
      // memCacheHeight: getMemCacheHeight(),
    );
    return ClipRRect(
      borderRadius: borderRadius ?? BorderRadius.circular(radius),
      child: isHero
          ? Hero(tag: "${tagHero == "" ? url : tagHero}", child: content)
          : content,
    );
  }

  Widget _placeHolder(BuildContext context) {
    return SizedBox(
      height: height,
      width: width,
      child: placeholder != null
          ? placeholder is Widget
              ? placeholder
              : Container(
                  decoration: BoxDecoration(
                      borderRadius: borderRadius ??
                          BorderRadius.all(Radius.circular(radius)),
                      color: getColor().textBase,
                      image: placeholder is String
                          ? DecorationImage(
                              image: AssetImage(placeholder),
                              fit: boxFit ?? BoxFit.cover,
                            )
                          : null))
          : RotatedBox(
              quarterTurns: rotate ?? 0,
              child: PlaceHolderImage(
                width: width,
                height: height,
                radius: radius,
                borderRadius: borderRadius,
              ),
            ),
    );
  }

  Widget _buildError(BuildContext context, String url, dynamic error) {
    return errorHolder ??
        PlaceHolderImage(
          width: width,
          height: height,
          radius: radius,
          borderRadius: borderRadius,
        );
  }
}

class CircleNetworkImage extends RoundNetworkImage {
  const CircleNetworkImage({
    required double size,
    String? url,
    Widget? errorHolder,
    dynamic placeholder,
    BoxFit? boxFit,
    Color? strokeColor,
    double? strokeWidth,
    Key? key,
  }) : super(
            key: key,
            radius: size / 2,
            url: url,
            width: size,
            height: size,
            boxFit: boxFit,
            placeholder: placeholder,
            errorHolder: errorHolder,
            strokeWidth: strokeWidth,
            strokeColor: strokeColor);
}

abstract class MultiResolutionImage {
  String? get smallUrl;
  String? get url;
  String? get largeUrl;
}

enum ImageResolution { small, medium, large }

class PlaceHolderImage extends StatelessWidget {
  final double width;
  final double height;
  final double radius;
  final BorderRadius? borderRadius;
  const PlaceHolderImage({
    Key? key,
    required this.width,
    required this.height,
    this.radius = 0.0,
    this.borderRadius,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        color: const Color(0xffc2cedb),
        borderRadius: borderRadius ?? BorderRadius.circular(radius),
      ),
      child: ClipRRect(
        borderRadius: borderRadius ?? BorderRadius.circular(radius),
        child: const Image(
          width: double.infinity,
          height: double.infinity,
          image: AssetImage("assets/images/img_default_avatar_shimmer.jpg"),
        ),
      ),
    );
  }
}
