import 'package:base_entities/app/app_entities/app_entities.dart';
import 'package:base_entities/app/app_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:base_entities/app/route/router.dart' as routes;
import 'package:base_entities/generated/locale_keys.g.dart';

///
/// Wait for app initialized
///
class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appState = context.select<AppModel, AppState>((value) => value.state);
    // var showLoading = true;
    if (appState == AppState.error) {
      //TODO: change enviroment here
      return const _ErrorApp(
        environment: Environment.staging,
      );
    } else if (appState == AppState.initialized) {
      // showLoading = false;
      WidgetsBinding.instance.addPostFrameCallback((_) {
        context.router.replace(const routes.AuthRouterRoute());
      });
    }

    return const Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.ac_unit,
              size: 157,
            ),
            // if (showLoading)
            //   Padding(
            //     padding: const EdgeInsets.only(top: 8.0),
            //     child: CircularProgressIndicator(),
            //   ),
          ],
        ),
      ),
    );
  }
}

class _ErrorApp extends StatelessWidget {
  final Environment environment;
  const _ErrorApp({
    Key? key,
    required this.environment,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final error = context.select<AppModel, Object?>((model) => model.initError);
    final message = error?.toString() ?? "Unknown error";

    return MaterialApp(
      home: Scaffold(
        body: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(24),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                  child: Text(
                message,
                textAlign: TextAlign.center,
              )),
              TextButton(
                onPressed: () {
                  context.read<AppModel>().init();
                },
                child: Text(LocaleKeys.retry.tr()),
              )
            ],
          ),
        ),
      ),
    );
  }
}
