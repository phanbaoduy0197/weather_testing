import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class UpdateProfileModel extends BaseModel {
  @override
  ViewState get initState => ViewState.loaded;
  TextEditingController nameController = TextEditingController();
  TextEditingController provinceController = TextEditingController();
  TextEditingController districtController = TextEditingController();
  TextEditingController wardController = TextEditingController();

  BehaviorSubject<Province?> get province =>
      locator<WeatherParameter>().province;
  BehaviorSubject<Districts?> get district =>
      locator<WeatherParameter>().district;
  BehaviorSubject<Wards?> get ward => locator<WeatherParameter>().ward;
  BehaviorSubject<List<Province>> get listProvinces =>
      locator<WeatherParameter>().listAllProvinces;

  @override
  void dispose() {
    super.dispose();
    nameController.dispose();
    provinceController.dispose();
    districtController.dispose();
    wardController.dispose();
  }
}
