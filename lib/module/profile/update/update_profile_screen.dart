import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/base/base_state/base_state.dart';
import 'package:base_entities/basic/misc/dimens.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/util/snack_bar_util.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:base_entities/module/auth/ui/login_screen.dart';
import 'package:base_entities/module/profile/update/update_profile_model.dart';
import 'package:base_entities/module/widget/bottom_sheet.dart';
import 'package:base_entities/module/widget/default_button_widget.dart';
import 'package:flutter/material.dart';

@RoutePage()
class UpdateProfileScreen extends StatefulWidget {
  const UpdateProfileScreen({super.key});

  @override
  State<UpdateProfileScreen> createState() => _UpdateProfileScreenState();
}

class _UpdateProfileScreenState
    extends BaseState<UpdateProfileModel, UpdateProfileScreen> {
  @override
  void initState() {
    super.initState();
    initData();
  }

  Future initData() async {
    final user = locator.get<UserModel>().user;
    if (user?.fullName?.isNotEmpty == true) {
      model.nameController.text = user!.fullName!;
    }
    model.provinceController.text = model.province.valueOrNull?.name ?? "";
    model.districtController.text = model.district.valueOrNull?.name ?? "";
    model.wardController.text = model.ward.valueOrNull?.name ?? "";
  }

  Future onUpdate() async {
    locator.get<UserModel>().onUpdateUser(
          User(
            fullName: model.nameController.text,
            district: model.districtController.text,
            province: model.provinceController.text,
            ward: model.wardController.text,
            token: "token_${model.nameController.text}",
          ),
        );
    locator.get<UserModel>().changeNotifierModel();
    Navigator.pop(context);
  }

  @override
  Widget buildContentView(BuildContext context, UpdateProfileModel model) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            "Cập nhật",
            style: context.tt.h4,
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.all(Dimens.paddingLeftRight),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                controller: model.nameController,
                style: context.tt.body2,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                validator: (text) {
                  return errorText(text);
                },
                textCapitalization: TextCapitalization.sentences,
                decoration: InputDecoration(
                  labelText: "Tên của bạn (bắt buộc)",
                  labelStyle: context.tt.body2.copyWith(color: Colors.grey),
                  errorStyle: context.tt.body2.error,
                ),
              ),
              StreamBuilder<Province?>(
                  stream: model.province.stream,
                  builder: (context, snapshot) {
                    return InkWell(
                      onTap: () {
                        showProvince(context);
                      },
                      child: ChildSelection(
                        controller: model.provinceController,
                        hintText: "Tỉnh/ Thành",
                        isDisable: false,
                      ),
                    );
                  }),
              StreamBuilder<Province?>(
                  stream: model.province.stream,
                  builder: (context, snapshot) {
                    return InkWell(
                      onTap: () {
                        if (!snapshot.hasData) {
                          context.snackBarError("Vui lòng nhập Tỉnh/ Thành");
                        } else {
                          if (snapshot.data?.districts?.isNotEmpty == true) {
                            showDistrict(context);
                          } else {
                            context.snackBarError("Không có Quận/ Huyện");
                          }
                        }
                      },
                      child: ChildSelection(
                        controller: model.districtController,
                        hintText: "Quận/ Huyện",
                        isDisable: false,
                      ),
                    );
                  }),
              StreamBuilder<Districts?>(
                  stream: model.district.stream,
                  builder: (context, snapshot) {
                    return InkWell(
                      onTap: () {
                        if (!snapshot.hasData) {
                          context.snackBarError("Vui lòng nhập Quận/ Huyện");
                        } else {
                          if (snapshot.data?.wards?.isNotEmpty == true) {
                            showWard(context);
                          } else {
                            context.snackBarError("Không có Phường/ Xã");
                          }
                        }
                      },
                      child: ChildSelection(
                        controller: model.wardController,
                        hintText: "Phường/ Xã",
                        isDisable: !snapshot.hasData,
                      ),
                    );
                  }),
            ],
          ),
        ),
        bottomNavigationBar: SafeArea(
          child: Padding(
            padding: const EdgeInsets.all(Dimens.paddingLeftRight),
            child: DefaultButtonWidget(
              title: "Lưu",
              onTap: onUpdate,
            ),
          ),
        ),
      ),
    );
  }

  void showDistrict(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(model.province.value!.districts!.length,
                  (index) {
                final item = model.province.value!.districts![index];
                return InkWell(
                  onTap: () {
                    model.districtController.text = item.name ?? "";
                    model.district.add(item);
                    model.ward.add(null);
                    model.wardController.text = "";
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${item.name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.district.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  void showWard(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
                  List.generate(model.district.value!.wards!.length, (index) {
                final item = model.district.value!.wards![index];
                return InkWell(
                  onTap: () {
                    model.wardController.text = item.name ?? "";
                    model.ward.add(item);
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${item.name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.ward.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  void showProvince(BuildContext context) {
    RBottomSheet.show(
        context: context,
        builder: (ct) {
          return RBottomSheet(
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children:
                  List.generate(model.listProvinces.value.length, (index) {
                final item = model.listProvinces.value[index];
                return InkWell(
                  onTap: () {
                    model.provinceController.text = item.name ?? "";
                    model.province.add(item);
                    model.district.add(null);
                    model.districtController.text = "";
                    model.ward.add(null);
                    model.wardController.text = "";
                    Navigator.pop(ct);
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 12, bottom: 12),
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            "${model.listProvinces.value[index].name}",
                            style: ct.tt.body1,
                          ),
                        ),
                        if (model.province.valueOrNull?.name == item.name)
                          const Icon(
                            Icons.check,
                            size: 24,
                          ),
                      ],
                    ),
                  ),
                );
              }),
            ),
          );
        });
  }

  String? errorText(String? text) {
    if (text?.trim().isNotEmpty != true) {
      return "Vui lòng nhập tên của bạn";
    }
    return null;
  }
}
