import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/base/base_model/base_list_model.dart';
import 'package:base_entities/basic/theme/font_manager.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/di/locator.dart';

class ListingModel extends BaseListModel<String> {
  late final repo = locator.get<CommonRepository>();
  late final font = locator.get<FontManager>();
  late final userModel = locator.get<UserModel>();
  int total = 0;
  @override
  Future<List<String>> getData({params}) async {
    //Lay danh sach bang cach goi API se nhu the nay
    final res = await repo.searchExampleList(page: page, size: pageSize);
    total = res.total ?? 0;
    return res.data ?? [];
  }

  String getAddress() {
    return [
      userModel.user?.province,
      userModel.user?.district,
      userModel.user?.ward
    ].where((e) => e?.trim().isNotEmpty == true).join(", ");
  }

  changeNotify() {
    notifyListeners();
  }
}
