import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/basic/misc/dimens.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/basic/util/utils.dart';
import 'package:base_entities/data/service/common_service.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/profile/profile_model.dart';
import 'package:base_entities/base/base_state/base_list_state.dart';
import 'package:base_entities/module/widget/bottom_sheet_menu.dart';
import 'package:base_entities/module/widget/default_button_widget.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState
    extends BaseListState<String, ListingModel, ProfileScreen>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: Dimens.paddingLeftRight,
            right: Dimens.paddingLeftRight,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _buildProfile(context),
              //-- widget nằm ngoài model, muốn lắng nghe data từ model thì phải dùng thế này
              ChangeNotifierProvider.value(
                value: model,
                child: Consumer<ListingModel>(builder: (context, model, _) {
                  return Padding(
                    padding: const EdgeInsets.only(
                      top: Dimens.paddingLeftRight,
                      bottom: Dimens.paddingLeftRight,
                    ),
                    child: Text(
                      "Thông tin cơ bản (${model.total})",
                      style: context.tt.body1,
                    ),
                  );
                }),
              ),
              //--- buildContent là widget listing được làm sẵn từ base list state, với item là buildItem
              Expanded(
                child: super.buildContent(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildProfile(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: model.userModel,
      child: Consumer<UserModel>(builder: (context, userModel, child) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(
              height: 20,
            ),
            Text(
              "Xin chào, ${model.userModel.user?.fullName}",
              style: context.tt.h4,
            ),
            const SizedBox(
              height: 4,
            ),
            Text(
              model.getAddress(),
              style: context.tt.body2.greyLight,
            ),
            const SizedBox(
              height: 12,
            ),
          ],
        );
      }),
    );
  }

  @override
  Widget buildItem(BuildContext context, String item, int index) {
    return InkWell(
      onTap: () async {
        switch (item) {
          case "Cập nhật":
            context.router.push(const UpdateProfileRoute());
            break;
          case "Logout":
            locator<UserModel>().logout();
            break;
          case "Đổi Font":
            final menu =
                fonts.mapIndexed((index, e) => Tuple2(index, e)).toList();
            final res = await RBottomSheetMenu.showWithText(
                context: context, menu: menu, title: "Chọn Font");
            if (res != null) {
              model.font.changeFont(menu[res].item2);
            }
            break;
          default:
            break;
        }
      },
      child: Container(
        color: getColor().white,
        padding: const EdgeInsets.only(top: 12, bottom: 12),
        child: Text(
          item,
          style: context.tt.body1,
        ),
      ),
    );
  }

  @override
  Widget buildSeparator(BuildContext context, int index) {
    return const BuildLineWidget();
  }

  @override
  bool get wantKeepAlive => true;
}
