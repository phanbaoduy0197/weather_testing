import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/basic/misc/disposable.dart';
import 'package:base_entities/data/client/keys_constant.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/module/auth/model/province.dart';
import 'package:collection/collection.dart';
import 'package:rxdart/rxdart.dart';
import 'package:weather/weather.dart';

class WeatherParameter extends Disposable {
  WeatherFactory wf = WeatherFactory(keyWeather, language: Language.VIETNAMESE);

  final province = BehaviorSubject<Province?>();
  final district = BehaviorSubject<Districts?>();
  final ward = BehaviorSubject<Wards?>();
  final listAllProvinces = BehaviorSubject<List<Province>>.seeded([]);

  void init() async {
    final user = locator<UserModel>().user;
    if (listAllProvinces.value.isEmpty) {
      final res = await locator<CommonRepository>().getListProvinces();
      listAllProvinces.add(res);
    }
    if (user?.province?.isNotEmpty == true) {
      province.add(listAllProvinces.value
          .firstWhereOrNull((e) => e.name == user?.province));
      if (user?.district?.isNotEmpty == true) {
        district.add(province.valueOrNull?.districts
            ?.firstWhereOrNull((e) => e.name == user?.district));
        if (user?.ward?.isNotEmpty == true) {
          ward.add(district.valueOrNull?.wards
              ?.firstWhereOrNull((e) => e.name == user?.ward));
        }
      }
    }
  }

  @override
  void dispose() {
    province.close();
    district.close();
    ward.close();
    listAllProvinces.close();
  }
}
