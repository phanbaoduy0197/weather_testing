import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/data/client/api_constant.dart';
import 'package:flutter/cupertino.dart';

import 'base_model.dart';

abstract class BaseListModel<T> extends BaseModel {
  List<T> items = <T>[];
  dynamic params;
  int page = 1;
  int moreItems = 0;
  ValueNotifier<bool> showLoadingMoreIcon = ValueNotifier(false);
  int get pageSize => pageSizeDefault;
  late BuildContext apiContext;

  @override
  ViewState get initState => ViewState.loading;

  bool isLoadingData = false;

  bool isScaleWhenLoaded = true;

  loadData(
      {dynamic params,
      bool isClear = false,
      bool isLoadMore = false,
      required BuildContext context}) async {
    this.params = params;
    apiContext = context;
    if (isLoadMore) showLoadingMoreIcon.value = true;
    try {
      final data = await getData(params: params);
      if (isScaleWhenLoaded) {
        notifier?.notifyListeners();
      }
      if (isClear) items.clear();
      items.addAll(data);
      progressState = ProgressState.success;
      if (isLoadMore) showLoadingMoreIcon.value = false;
      viewState = ViewState.loaded;
    } catch (e, st) {
      Log.e(e, st);
      responseError1 = RException.wrap(e);
      progressState = ProgressState.error;
      if (isLoadMore) showLoadingMoreIcon.value = false;
      viewState = ViewState.error;
    }
    notifyListeners();
    isLoadingData = false;
    debugPrint(
        "\n\n*************************** page: $page ***************************\n\n");
  }

  loadMoreData(BuildContext context) async {
    if (isLoadingData) return;
    if (page * pageSize + moreItems == items.length) {
      debugPrint(
          "\n\n***************************  load more  ***************************\n\n");
      isLoadingData = true;
      page++;
      loadData(params: params, isLoadMore: true, context: context);
    }
  }

  Future<void> refresh({dynamic params, required BuildContext context}) async {
    page = 1;
    if (params != null) this.params = params;
    loadData(params: params ?? this.params, isClear: true, context: context);
  }

  clearAll() async {
    page = 1;
    items.clear();
    notifyListeners();
  }

  Future<List<T>> getData({dynamic params});

  @override
  void dispose() {
    super.dispose();
    showLoadingMoreIcon.dispose();
  }
}
