import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/base/base_state/animated_indexed_stack.dart';
import 'package:base_entities/basic/colors/base_colors.dart';
import 'package:base_entities/basic/misc/dimens.dart';
import 'package:base_entities/basic/theme/text_theme.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/generated/locale_keys.g.dart';
import 'package:base_entities/module/widget/modal_loading_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'base_error.dart';

abstract class BaseState<M extends BaseModel, W extends StatefulWidget>
    extends State<W> {
  late M model;
  BuildContext? _loadingDialogContext;
  // Widget? _loadingDialog;

  @override
  void initState() {
    super.initState();
    model = createModel();
    model.notifier = ValueNotifier(null);
    onModelReady();
  }

  bool get isSliverOverlapAbsorber => false;

  Color get backgroundLoadingColor => BColors.white;

  bool get disposeModel => true;

  Widget get loadingWidget => const Scaffold(
        body: Center(
          child: LoadingWidget(),
        ),
      );

  @override
  void dispose() {
    super.dispose();
    if (disposeModel) model.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return buildContent();
  }

  Widget buildContent() {
    return ChangeNotifierProvider<M>.value(
      value: model,
      child: Consumer<M>(
          builder: (context, model, child) => AnimatedIndexedStack(
              notifier: model.notifier,
              children: buildViewByState(context, model))),
    );
  }

  Widget buildViewByState(BuildContext context, M model) {
    switch (model.viewState) {
      case ViewState.loading:
        return loadingWidget;
      case ViewState.error:
        model.changeNotifyAnimation();
        return ErrorViewWidget(
          message: model.responseError1?.localizedMessage ??
              LocaleKeys.unknownErrorMessage.tr(),
          onRetry: onRetry,
        );
      case ViewState.loaded:
        if (isSliverOverlapAbsorber) {
          return buildProgressByState(context, model);
        } else {
          return AnimatedSwitcher(
            duration: const Duration(milliseconds: 150),
            child: buildProgressByState(context, model),
          );
        }
      default:
        return Container();
    }
  }

  Widget buildProgressByState(BuildContext context, M model) {
    Widget? errorView;
    if (model.progressState == ProgressState.error) {
      errorView = buildErrorView(context);
    }
    return Stack(
      children: [
        buildContentView(context, model),
        if (errorView != null) errorView,
      ],
    );
  }

  showLoading({BuildContext? dialogContext}) async {
    if (_loadingDialogContext != null) {
      return;
    }
    _loadingDialogContext = dialogContext ?? context;

    await showDialog(
        barrierDismissible: false,
        context: _loadingDialogContext!,
        useRootNavigator: false,
        builder: (_) => const LoadingDialog());

    _loadingDialogContext = null;
  }

  hideLoading() {
    if (_loadingDialogContext == null) {
      return;
    }

    Navigator.of(_loadingDialogContext!).pop();
    _loadingDialogContext = null;
  }

  showError(String errorMessage,
      {String? title,
      String? textConfirm,
      Function()? onTapConfirm,
      bool centerContent = false}) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) => PopupDefaultWidget(
              title: title ?? "",
              centerContent: centerContent,
              content: errorMessage.tr(),
              textConfirm: textConfirm,
              selectConfirm: onTapConfirm,
            ));
  }

  void callApi({
    required Function callApiTask,
    Function? onSuccess,
    Function? onFail,
    BuildContext? dialogContext,
  }) async {
    await ModalLoadingIndicator.showLoadingJob(
        dialogContext ?? context, callApiTask());
  }

  M createModel() => locator<M>();

  Widget buildContentView(BuildContext context, M model);

  Widget? buildErrorView(BuildContext context) {
    return const SizedBox();
  }

  void onModelReady() {}

  void onRetry() {}
}

mixin BaseAutomaticKeepAliveClientMixin<M extends BaseModel,
    T extends StatefulWidget> on BaseState<M, T> {}

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: LoadingWidget(),
    );
  }
}

class LoadingWidget extends StatelessWidget {
  final Color? color;
  final double? strokeWidth;

  const LoadingWidget({Key? key, this.color, this.strokeWidth})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      strokeWidth: strokeWidth ?? 4.0,
      color: color ?? BColors.primaryColor,
    );
  }
}

class PopupDefaultWidget extends StatelessWidget {
  final String? title;
  final String? content;
  final String? textConfirm;
  final bool centerContent;
  final Function()? selectConfirm;
  const PopupDefaultWidget({
    this.title,
    this.content,
    this.selectConfirm,
    this.textConfirm,
    this.centerContent = true,
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: getColor().white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      elevation: 3,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          if (title != null && title != "")
            Padding(
              padding: const EdgeInsets.only(
                  left: Dimens.paddingLeftRight,
                  right: Dimens.paddingLeftRight,
                  top: Dimens.size16,
                  bottom: Dimens.size12),
              child: Text(
                title!,
                style: textTheme(context).h4,
              ),
            ),
          SizedBox(
            height: title == null || title == "" ? 20 : 0,
          ),
          if (content != null && content != "")
            Padding(
              padding: const EdgeInsets.only(
                  left: Dimens.paddingLeftRight,
                  right: Dimens.paddingLeftRight,
                  bottom: 20),
              child: Text(
                content ?? "",
                style: textTheme(context).body1,
                textAlign: centerContent ? TextAlign.center : TextAlign.start,
              ),
            ),
          Container(
            height: 1,
            color: Colors.grey,
          ),
          Row(
            children: [
              Expanded(
                child: InkWell(
                  onTap: () {
                    if (selectConfirm != null) selectConfirm!();
                    Navigator.maybePop(context);
                  },
                  child: Container(
                    height: 50,
                    decoration: BoxDecoration(
                      color: getColor().primaryColor,
                      borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(8),
                      ),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      textConfirm ?? LocaleKeys.confirm.tr(),
                      style: textTheme(context).sub2.white,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50,
                    decoration: BoxDecoration(
                      color: getColor().white,
                      borderRadius: const BorderRadius.only(
                        bottomRight: Radius.circular(8),
                      ),
                    ),
                    child: Text(
                      LocaleKeys.cancel.tr(),
                      style: textTheme(context).sub2.textBase,
                      maxLines: 1,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class ErrorViewWidget extends StatelessWidget {
  final String? message;
  final VoidCallback? onRetry;

  const ErrorViewWidget({
    @required this.message,
    @required this.onRetry,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      alignment: const Alignment(0, -0.25),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text(
            message ?? "",
            style: textTheme(context).body1,
            textAlign: TextAlign.center,
          ),
          const SizedBox(height: 16),
          TextButton(
            style: TextButton.styleFrom(
              backgroundColor: getColor().primaryColor,
              foregroundColor: Colors.white,
              textStyle: textTheme(context).sub2,
            ),
            onPressed: onRetry,
            child: Text(LocaleKeys.retry.tr()),
          )
        ],
      ),
    );
  }
}

class EmptyWidget extends StatelessWidget {
  final String? message;

  const EmptyWidget({this.message, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: const Alignment(0, -0.25),
      child: Text(
        message ?? LocaleKeys.noData.tr(),
        style: textTheme(context).body1.height40Per,
      ),
    );
  }
}
