import 'package:base_entities/base/base_model/base_list_model.dart';
import 'package:base_entities/base/base_state/base_state.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';
import 'base_list_state.dart';

abstract class BaseGridState<I, M extends BaseListModel<I>,
    W extends StatefulWidget> extends BaseListState<I, M, W> {
  @override
  Widget buildContentView(BuildContext context, M model) {
    Widget content;
    if (!(model.items.isNotEmpty == true)) {
      content = buildEmptyView(context);
    } else {
      content = Stack(
        children: [
          Container(
            color: backgroundColor,
            child: GridView.count(
              crossAxisCount: crossAxisCount,
              physics: scrollPhysics,
              padding: paddingGrid,
              controller: controller,
              scrollDirection: axis,
              childAspectRatio: childAspectRatio,
              mainAxisSpacing: mainAxisSpacing,
              crossAxisSpacing: crossAxisSpacing,
              shrinkWrap: shrinkWrap,
              children: model.items
                  .map((e) => buildItem(context, e, model.items.indexOf(e)))
                  .toList(),
            ),
          ),
          ValueListenableBuilder<bool>(
              valueListenable: model.showLoadingMoreIcon,
              builder: (context, value, _) {
                return Provider<bool>.value(
                  value: value,
                  child: Visibility(
                    visible: value,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        width: radiusLoading,
                        height: radiusLoading + 20,
                        padding: const EdgeInsets.only(bottom: 20),
                        child: const LoadingWidget(
                          strokeWidth: 3,
                        ),
                      ),
                    ),
                  ),
                );
              }),
        ],
      );
    }
    if (enableRefresh) {
      content = RefreshIndicator(onRefresh: onRefresh, child: content);
    }
    content = LazyLoadScrollView(
      scrollOffset: rangeLoadMore,
      onEndOfPage: () {
        model.loadMoreData(context);
      },
      child: content,
    );
    return content;
  }

  @override
  Widget buildEmptyView(BuildContext context) {
    return Container();
  }

  int crossAxisCount = 2;

  double childAspectRatio = 1;

  double mainAxisSpacing = 0;

  double crossAxisSpacing = 0;

  Color? backgroundColor;

  @override
  bool get shrinkWrap => false;

  Axis get axis => Axis.vertical;

  ScrollPhysics get scrollPhysics => const AlwaysScrollableScrollPhysics();

  EdgeInsets paddingGrid = const EdgeInsets.symmetric(horizontal: 0);

  int get loadMoreBeforeRange => 6;

  int get scrollOffset => 500;
}
