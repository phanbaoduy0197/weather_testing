import 'package:flutter/material.dart';

class AnimatedIndexedStack extends StatefulWidget {
  final Widget children;
  final ValueNotifier? notifier;

  const AnimatedIndexedStack({
    Key? key,
    required this.children,
    required this.notifier,
  }) : super(key: key);

  @override
  State<AnimatedIndexedStack> createState() => _AnimatedIndexedStackState();
}

class _AnimatedIndexedStackState extends State<AnimatedIndexedStack>
    with SingleTickerProviderStateMixin {
  late AnimationController _controller;
  late Animation<double> _animation1;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 150),
      value: 1.0,
    );
    _animation1 = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(parent: _controller, curve: Curves.easeInOut),
    );
    super.initState();

    widget.notifier?.addListener(() {
      if (mounted) {
        _controller.reverse().then((_) async {
          _controller.forward();
        });
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: _animation1,
      builder: (context, child) {
        return Opacity(
          opacity: _controller.value < 0.5 ? 0.5 : _controller.value,
          child: Transform.scale(
            scale: 1.015 - (_controller.value * 0.015),
            child: child,
          ),
        );
      },
      child: widget.children,
    );
  }
}
