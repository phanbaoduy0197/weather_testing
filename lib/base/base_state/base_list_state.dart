import 'dart:async';

import 'package:base_entities/base/base_model/base_list_model.dart';
import 'package:flutter/material.dart';
import 'package:lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:provider/provider.dart';
import 'base_state.dart';

abstract class BaseListState<I, M extends BaseListModel<I>,
    W extends StatefulWidget> extends BaseState<M, W> {
  ScrollController? controller;

  double get radiusLoading => 25;

  @override
  void dispose() {
    super.dispose();
    controller?.dispose();
  }

  @override
  void initState() {
    super.initState();
    controller = ScrollController();
    if (autoLoadData) {
      model.loadData(context: context);
    }
  }

  @override
  Widget buildContentView(BuildContext context, M model) {
    Widget content;
    if (model.items.isEmpty && (listWidgetMore.isEmpty || hideWhenItemsEmpty)) {
      content = buildEmptyView(context);
    } else {
      content = Stack(
        children: [
          ListView.separated(
              physics: physics,
              shrinkWrap: shrinkWrap,
              padding: padding,
              controller: controller,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                if (listWidgetMore.isNotEmpty &&
                    index < listWidgetMore.length &&
                    isMoreOnTop) {
                  return listWidgetMore[index];
                }
                if (listWidgetMore.isNotEmpty &&
                    !isMoreOnTop &&
                    index >= model.items.length) {
                  return listWidgetMore[index - model.items.length];
                }
                return buildItem(
                  context,
                  isMoreOnTop
                      ? model.items[index - listWidgetMore.length]
                      : model.items[index],
                  index,
                );
              },
              separatorBuilder: (context, index) =>
                  buildSeparator(context, index),
              itemCount: model.items.length + listWidgetMore.length),
          ValueListenableBuilder<bool>(
              valueListenable: model.showLoadingMoreIcon,
              builder: (context, value, _) {
                return Provider<bool>.value(
                  value: value,
                  child: Visibility(
                    visible: value,
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        width: radiusLoading,
                        height: radiusLoading + 20,
                        padding: const EdgeInsets.only(bottom: 20),
                        child: const LoadingWidget(
                          strokeWidth: 3,
                        ),
                      ),
                    ),
                  ),
                );
              })
        ],
      );
      if (enableRefresh) {
        content = RefreshIndicator(onRefresh: onRefresh, child: content);
      }
      content = LazyLoadScrollView(
        scrollOffset: rangeLoadMore,
        onEndOfPage: () {
          model.loadMoreData(context);
        },
        child: content,
      );
    }

    return content;
  }

  Future<void> onRefresh() async {
    return model.refresh(context: context);
  }

  @override
  void onRetry() {
    model.loadData(context: context, params: model.params);
  }

  Widget buildItem(BuildContext context, I item, int index);

  Widget buildSeparator(BuildContext context, int index) {
    return SizedBox(height: itemSpacing);
  }

  Widget buildEmptyView(BuildContext context) {
    return const EmptyWidget();
  }

  bool get enableRefresh => true;

  EdgeInsets get padding => const EdgeInsets.all(0);

  double get itemSpacing => 0;

  bool get autoLoadData => true;

  bool get hideWhenItemsEmpty => false;

  int get rangeLoadMore => 500;

  bool get shrinkWrap => false;

  bool get isMoreOnTop => false;

  List<Widget> get listWidgetMore => [];

  ScrollPhysics get physics => const AlwaysScrollableScrollPhysics();
}
