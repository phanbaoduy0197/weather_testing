//ignore: implementation_imports
import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/generated/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

extension ErrorContentParser on RException {
  String get localizedMessage {
    final res = message.tr();
    if (res != message) {
      // vd api resp "error_msg":"onboarding_data_already_set"
      // nếu có define thì trả về luôn
      return res;
    }

    // nếu không trả về message tuỳ theo mã lỗi
    return (_errorMessageMap[code] ?? LocaleKeys.unknownErrorMessage).tr();
  }
}

// ignore: non_constant_identifier_names
const _errorMessageMap = <int, String>{
  RError.kErrorUnknown: LocaleKeys.unknownErrorMessage,
};
