import 'dart:convert';
import 'dart:io';

import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/basic/util/utils.dart';
import 'package:weather/weather.dart';

class RException<T> implements Exception {
  static const String kUnknownError = "Unknown error";
  final int code;
  final String message;
  final T? data;

  RException({this.code = -1, this.message = kUnknownError, this.data});

  RException.code(int code, [String? message, T? data])
      : this(
          code: code,
          message: message ?? kUnknownError,
          data: data,
        );

  RException.unknown([String? message, T? data])
      : this(
          code: RError.kErrorUnknown,
          message: message ?? kUnknownError,
          data: data,
        );

  factory RException.wrap(dynamic error, [T? data]) {
    if (error is RException) {
      if (data != null) {
        return RException.code(error.code, error.message, data);
      }
      return error as RException<T>;
    }

    if (error is OpenWeatherAPIException) {
      final input = error.toString();
      try {
        String output =
            input.substring(input.indexOf("{"), input.lastIndexOf("}") + 1);
        Map map = json.decode(output);
        return RException(
          code: (map["cod"] as String).toInt(),
          message: map["message"],
          data: data,
        );
      } catch (e, stack) {
        Log.e(e, stack);
      }
    }

    if (_isFileNotFoundError(error)) {
      FileSystemException err = error;
      return RException(
          code: RError.itemNotFound,
          message: err.osError?.message ?? kUnknownError,
          data: data);
    }

    return RException<T>.unknown(error.toString(), data);
  }

  @override
  String toString() {
    return "[$code][$message]";
  }
}

bool _isFileNotFoundError(error) =>
    error is FileSystemException && error.osError?.errorCode == 2;

class ErrorContent {
  String title;
  String message;

  ErrorContent(this.title, this.message);
}

extension ErrorContentParser on RException {
  String get localizedMessage {
    final res = message;
    if (res != message) {
      // vd api resp "error_msg":"onboarding_data_already_set"
      // nếu có define thì trả về luôn
      return res;
    }

    // nếu không trả về message tuỳ theo mã lỗi
    return (_kMappingError[code] ?? 'Lỗi không xác định, vui lòng thử lại');
  }

  ErrorContent toErrorContent() {
    String title = 'AgentsCRM';

    String? content;
    if (_hasUnknwonMessage()) {
      content = message;
    } else {
      content = _kMappingError[code];
    }

    content ??= _kMappingError[RError.kErrorUnknown];

    return ErrorContent(title, content!);
  }

  bool _hasUnknwonMessage() {
    var x = (code == RError.kErrorUnknown || code == 400) &&
        message.isNotEmpty == true;
    return x;
  }
}

const Map<int, String> _kMappingError = {
  RError.itemNotFound: 'Không tìm thấy dữ liệu.',
  RError.kErrorNoConnection:
      'Không thể kết nối đến máy chủ, xin vui lòng kiểm tra kết nối internet và thử lại.',
  RError.kErrorUnknown: 'Có lỗi xảy ra, xin vui lòng thử lại.',
  RError.badRequest: 'Bad Request',
  RError.unauthorized: 'Chưa đăng nhập',
  RError.kErrorTimeout: 'Vượt quá thời gian tải',
};

class RError {
  static const none = 0;

  static const itemNotFound = 404;
  static const unauthorized = 401;
  static const badRequest = 400;
  static const kErrorUnknown = -1000;
  static const kErrorNoConnection = -1001;
  static const kErrorTimeout = -1002;

  static const Map<String, int> _kErrorCodeMap = {
    "not_found": RError.itemNotFound,
    "bad_request": RError.badRequest,
    "unknwon": RError.kErrorUnknown,
    "unauthorized": RError.unauthorized,
    "no_connection": RError.kErrorNoConnection,
    "time_out": RError.kErrorTimeout,
  };

  static int? from(String code) => _kErrorCodeMap[code];
}
