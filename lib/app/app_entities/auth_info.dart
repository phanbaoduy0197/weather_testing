import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/module/auth/model/session.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'auth_info.freezed.dart';
// optional: Since our Person class is serializable, we must add this line.
// But if Person was not serializable, we could skip it.
part 'auth_info.g.dart';

@freezed
class AuthInfo with _$AuthInfo {
  const factory AuthInfo({
    required User? user,
    required Session? session,
  }) = _AuthInfo;

  factory AuthInfo.fromJson(Map<String, Object?> json) =>
      _$AuthInfoFromJson(json);
}
