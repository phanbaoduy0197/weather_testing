import 'dart:convert';
import 'dart:io';

import 'package:base_entities/basic/misc/logger.dart';
import 'package:json_annotation/json_annotation.dart';

part 'noti.g.dart';

@JsonSerializable()
class RNotification {
  final String? id;
  final String? notifyChannel;
  final int? createdTime;
  final int? updatedTime;
  final bool? isRead;
  final bool? isSeen;
  final int? expireAt;
  final RNotificationData? data;

  RNotification(
      {this.id,
      this.notifyChannel,
      this.createdTime,
      this.updatedTime,
      this.isRead,
      this.isSeen,
      this.expireAt,
      this.data});

  static RNotification fromJson(Map<String, dynamic> json) {
    final data = json["data"];
    if (data != null && data is Map) {
      data["id"] = json["id"];
      data["channel"] = json["notify_channel"];
    }

    return _$RNotificationFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RNotificationToJson(this);
}

@JsonSerializable()
class RNotificationData {
  final String? id;
  final String? channel;
  final String? title;
  final String? thumbnail;
  final String? body;
  @JsonKey(unknownEnumValue: RNotificationDataType.unknwon)
  final RNotificationDataType? objectType;
  final String? objectId;
  final String? compress;
  final String? extraData;
  final String? url;
  final dynamic extraDataObject;

  RNotificationData(
      {this.id,
      this.channel,
      this.title,
      this.thumbnail,
      this.body,
      this.objectType,
      this.objectId,
      this.url,
      this.compress,
      this.extraData,
      this.extraDataObject});

  factory RNotificationData.fromJson(Map<String, dynamic> json) {
    // unzip extra data & parse
    final objType = $enumDecodeNullable(
        _$RNotificationDataTypeEnumMap, json['object_type'],
        unknownValue: RNotificationDataType.unknwon);

    try {
      Map<String, dynamic>? extraDataJson;
      if (json["compress"] == "gzip") {
        final bytes = const Base64Codec().decode(json["extra_data"]);
        final data = GZipCodec().decode(bytes);
        final decoded = utf8.decode(data, allowMalformed: true);
        extraDataJson = jsonDecode(decoded);
      } else if (json["compress"] == "text") {
        extraDataJson = jsonDecode(json["extra_data"]);
      }

      if (extraDataJson != null) {
        if (objType == RNotificationDataType.kycResult) {
          json["extra_data_object"] =
              RN5KycResultExtraData.fromJson(extraDataJson);
        } else {
          json["extra_data_object"] = extraDataJson;
        }
      }
    } catch (err, stack) {
      Log.e(err, stack);
    }

    return _$RNotificationDataFromJson(json);
  }

  Map<String, dynamic> toJson() => _$RNotificationDataToJson(this);
}

@JsonSerializable()
class RN5KycResultExtraData {
  final bool? result;
  final String? reason;
  RN5KycResultExtraData({this.result, this.reason});

  factory RN5KycResultExtraData.fromJson(Map<String, dynamic> json) =>
      _$RN5KycResultExtraDataFromJson(json);
  Map<String, dynamic> toJson() => _$RN5KycResultExtraDataToJson(this);
}

enum RNotificationDataType {
  unknwon,
  @JsonValue("pa_profile_auth_kyc_result")
  kycResult
}

// {thumbnail: https://my-stag.rever.vn/user/11633424316710000_972/avatar,
//channel: in_app,
//compress: text,
//id: 0154b669-f782-4d90-9626-30ab9650b255,
//extra_data: {"result":false,"reason":"ko thich"},
//title: Thông báo kết quả KYC, object_type: pa_profile_auth_kyc_result,
//object_id: 11633424316710000_972,
//body: Hồ sơ của bạn chưa được chấp nhận. Bạn hãy cập nhật lại.}

// {thumbnail: https://my-stag.rever.vn/user/11633424316710000_972/avatar,
//channel: in_app,
//compress: text,
//id: 725d98f3-97df-4ffa-8084-ce04c831f709,
//object_type: pa_profile_auth_kyc_result,
//title: Thông báo kết quả KYC,
//extra_data: {"result":true},
//object_id: 11633424316710000_972,
//body: Chúc mừng bạn đã trở thành đối tác của Rever.}
