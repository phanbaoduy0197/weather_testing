import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_entities.freezed.dart';
// optional: Since our Person class is serializable, we must add this line.
// But if Person was not serializable, we could skip it.
part 'user_entities.g.dart';

@freezed
class User with _$User {
  const factory User({
    String? fullName,
    String? province,
    String? district,
    String? ward,
    String? token,
  }) = _User;

  factory User.fromJson(Map<String, Object?> json) => _$UserFromJson(json);
}
