import 'dart:io';

import 'package:base_entities/app/app_entities/exception.dart';
import 'package:dio/dio.dart';

class ApiException extends RException implements DioError {
  ApiException({int code = -1, String message = ''})
      : super(
          code: code,
          message: message,
        );

  ApiException.noConnection({required message})
      : this(code: RError.kErrorNoConnection, message: message);
  ApiException.unknwon({required message})
      : super(code: RError.kErrorUnknown, message: message);

  static ApiException fromDio(DioError error) {
    if (_isConnectionError(error)) {
      return ApiException.noConnection(message: error.message);
    } else if (error.response != null) {
      return _parseExceptionFromResponse(error, error.response!);
    } else {
      return ApiException.unknwon(message: error.message);
    }
  }

  static ApiException _parseExceptionFromResponse(
      DioError error, Response resp) {
    try {
      final response = ApiResponse.fromJson(resp.data);

      int? code;
      if (response.error != null) {
        // parse error code tử resp
        code = RError.from(response.error!);
      }

      code ??= error.response?.statusCode ?? RError.kErrorUnknown;

      return ApiException(
          code: code,
          message: response.errorMsg ??
              response.message ??
              RException.kUnknownError);
    } catch (_) {
      return ApiException.unknwon(message: error.error);
    }
  }

  @override
  dynamic error;

  @override
  late RequestOptions requestOptions;

  @override
  Response? response;

  @override
  late DioErrorType type;

  @override
  StackTrace? stackTrace;
}

bool _isConnectionError(DioError error) =>
    error.type == DioErrorType.receiveTimeout ||
    error.type == DioErrorType.connectTimeout ||
    error.error is SocketException;

class ApiResponse {
  ApiResponse({
    this.error,
    this.message,
    this.errorMsg,
    this.data,
  });

  String? error;
  String? message;
  String? errorMsg;
  dynamic data;

  factory ApiResponse.fromJson(Map<String, dynamic> json) => ApiResponse(
        error: json["error"],
        message: json["message"],
        errorMsg: json["error_msg"],
        data: json["data"],
      );

  Map<String, dynamic> toJson() => {
        "error": error,
        "message": message,
        "error_msg": errorMsg,
        "data": data,
      };
}
