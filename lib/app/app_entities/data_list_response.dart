import 'package:base_entities/basic/misc/logger.dart';

class DataListResponse<T> {
  final int? total;
  final List<T>? data;
  final int? code;
  final String? message;
  final double? potentialIncome;
  DataListResponse(
      this.code, this.message, this.total, this.data, this.potentialIncome);

  Map<String, dynamic> toJson() {
    List? listJson = data?.fold([], ((list, item) {
      list!.add((item as dynamic).toJson());
      return list;
    }));
    final map = {
      "int": code,
      "message": message,
      "total": total,
      "data": listJson,
    };

    return map;
  }

  factory DataListResponse.fromJson(
      Map<String, dynamic> json, T Function(Map<String, dynamic>) mapper) {
    final data = json['data'];
    List<T>? toData;
    if (data != null && data is List) {
      toData = data.map((e) => mapper(e)).toList();
    } else {
      Log.e("DataListResponse unhandled type ${data.runtimeType}");
    }

    return DataListResponse(json['code'], json['msg'], json['total'], toData,
        json['potential_income']);
  }
}
