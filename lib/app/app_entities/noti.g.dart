// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'noti.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

RNotification _$RNotificationFromJson(Map<String, dynamic> json) =>
    RNotification(
      id: json['id'] as String?,
      notifyChannel: json['notifyChannel'] as String?,
      createdTime: json['createdTime'] as int?,
      updatedTime: json['updatedTime'] as int?,
      isRead: json['isRead'] as bool?,
      isSeen: json['isSeen'] as bool?,
      expireAt: json['expireAt'] as int?,
      data: json['data'] == null
          ? null
          : RNotificationData.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$RNotificationToJson(RNotification instance) =>
    <String, dynamic>{
      'id': instance.id,
      'notifyChannel': instance.notifyChannel,
      'createdTime': instance.createdTime,
      'updatedTime': instance.updatedTime,
      'isRead': instance.isRead,
      'isSeen': instance.isSeen,
      'expireAt': instance.expireAt,
      'data': instance.data,
    };

RNotificationData _$RNotificationDataFromJson(Map<String, dynamic> json) =>
    RNotificationData(
      id: json['id'] as String?,
      channel: json['channel'] as String?,
      title: json['title'] as String?,
      thumbnail: json['thumbnail'] as String?,
      body: json['body'] as String?,
      objectType: $enumDecodeNullable(
          _$RNotificationDataTypeEnumMap, json['objectType'],
          unknownValue: RNotificationDataType.unknwon),
      objectId: json['objectId'] as String?,
      url: json['url'] as String?,
      compress: json['compress'] as String?,
      extraData: json['extraData'] as String?,
      extraDataObject: json['extraDataObject'],
    );

Map<String, dynamic> _$RNotificationDataToJson(RNotificationData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'channel': instance.channel,
      'title': instance.title,
      'thumbnail': instance.thumbnail,
      'body': instance.body,
      'objectType': _$RNotificationDataTypeEnumMap[instance.objectType],
      'objectId': instance.objectId,
      'compress': instance.compress,
      'extraData': instance.extraData,
      'url': instance.url,
      'extraDataObject': instance.extraDataObject,
    };

const _$RNotificationDataTypeEnumMap = {
  RNotificationDataType.unknwon: 'unknwon',
  RNotificationDataType.kycResult: 'pa_profile_auth_kyc_result',
};

RN5KycResultExtraData _$RN5KycResultExtraDataFromJson(
        Map<String, dynamic> json) =>
    RN5KycResultExtraData(
      result: json['result'] as bool?,
      reason: json['reason'] as String?,
    );

Map<String, dynamic> _$RN5KycResultExtraDataToJson(
        RN5KycResultExtraData instance) =>
    <String, dynamic>{
      'result': instance.result,
      'reason': instance.reason,
    };
