import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/basic/theme/font_manager.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/module/widget/guide/guide_overlay_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:event_bus/event_bus.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../di/locator.dart';
import 'app_model.dart';
import 'user_model.dart';

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  final _appModel = locator<AppModel>();

  final _appRouter = locator.get<RootRouter>();

  @override
  void initState() {
    super.initState();
    EasyLocalization.logger.enableLevels = [];
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return EasyLocalization(
      path: "resources/langs",
      supportedLocales: const [
        Locale('en'),
        Locale('vi'),
      ],
      startLocale: const Locale('vi'),
      fallbackLocale: const Locale('vi'),
      saveLocale: true,
      child: MultiProvider(
        providers: [
          ChangeNotifierProvider.value(value: _appModel),
          ChangeNotifierProvider.value(value: locator<UserModel>()),
          ValueListenableProvider.value(value: locator<UserModel>().authState),
          ChangeNotifierProvider.value(value: locator<ThemeManager>()),
          ChangeNotifierProvider.value(value: locator<FontManager>()),
        ],
        child: Consumer<ThemeManager>(
          builder: (context, theme, child) {
            return MaterialApp.router(
              routerConfig: _appRouter.appConfig(),
              title: "Weather Testing",
              theme: theme.themeData,
              debugShowCheckedModeBanner: false,
              localizationsDelegates: context.localizationDelegates,
              supportedLocales: context.supportedLocales,
              locale: context.locale,
              builder: (context, widget) {
                ///
                /// Widget hiện các hướng dẫn trên màn hình
                return MediaQuery(
                  data: MediaQuery.of(context).copyWith(
                    textScaler: const TextScaler.linear(1.0),
                  ),
                  child: GuideOverlayWidget(
                    child: widget ?? const SizedBox(),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}

EventBus eventBus = EventBus();
