import 'package:base_entities/app/app_entities/api_exception.dart';
import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/basic/misc/disposable.dart';
import 'package:base_entities/module/auth/model/session.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AuthHandlerInterceptor extends Interceptor implements Disposable {
  Session? session;
  final onUnauthorizedError = PublishSubject<RException>();

  AuthHandlerInterceptor();
  @override
  void dispose() {
    onUnauthorizedError.close();
  }

  @override
  void onRequest(
      RequestOptions options, RequestInterceptorHandler handler) async {
    if (session?.value?.isNotEmpty == true) {
      debugPrint("Bearer ${session!.value}");
      options.headers["Authorization"] = "Bearer ${session!.value}";
    }

    super.onRequest(options, handler);
  }

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    final newError = err is ApiException ? err : ApiException.fromDio(err);
    if (newError.code == RError.unauthorized) {
      onUnauthorizedError.add(newError);
    }
    super.onError(newError, handler);
  }
}
