import 'package:base_entities/app/app_entities/api_exception.dart';
import 'package:dio/dio.dart';

///
/// Chuyển tất cả các DioError thành RException để có thể dễ dàng xử lý trên UI
class ErrorHandlerInterceptor extends Interceptor {
  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    final newError = ApiException.fromDio(err);
    super.onError(newError, handler);
  }
}
