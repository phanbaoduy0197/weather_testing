import 'package:base_entities/app/app_entities/auth_info.dart';
import 'package:base_entities/app/app_entities/exception.dart';
import 'package:base_entities/app/app_entities/user_entities.dart';
import 'package:base_entities/base/base_model/base_model.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/data/repository/user_repository.dart';
import 'package:base_entities/di/locator.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class UserModel extends BaseModel {
  final authState = ValueNotifier(AuthState.unauthorized);
  // auth event cần sync bởi vì cần set session cho http client ngay khi login
  final authEvent = PublishSubject<AuthEvent>(sync: true);
  UserRepository get _userRepository => locator<UserRepository>();
  AuthInfo? _authInfo;
  User? get user => _authInfo?.user;
  // AuthEvent? _initKycEvent;
  // AuthEvent? get initKycEvent => _initKycEvent;

  @override
  void dispose() {
    authState.dispose();
    authEvent.close();
    super.dispose();
  }

  init() async {
    try {
      final res = await _userRepository.getAuthInfo();
      if (res != null) {
        _authInfo = res;
        authEvent.add(OnSessionRestored(res));
        _validateSession();
        _onSessionStarted();
        return;
      }
    } catch (e, s) {
      Log.e(e, s);
    }
  }

  void _validateSession() async {
    try {
      // chỗ này lấy luôn profile để cập nhật thông tin, nếu lấy lỗi sẽ tự động đá ra màn hình login từ onError trong AuthInterceptor (mã 401 un_author)
      final user = await _userRepository.getMyProfile();
      _authInfo = _authInfo?.copyWith(user: user);
      _userRepository.setAutInfo(_authInfo);

      // if (currentStatus != user!.registerStatus) {
      //   if (user.registerStatus == RegisterStatus.notApproved) {
      //     _initKycEvent = OnKycRejectedEvent(reason: user.message);
      //     authEvent.add(_initKycEvent!);
      //   } else if (user.registerStatus == RegisterStatus.verified) {
      //     _initKycEvent = OnKycApprovedEvent();
      //     authEvent.add(_initKycEvent!);
      //   }
      // }
    } catch (e, s) {
      Log.w(e, s);
      // Nếu như unauthorize thì xử lý chỗ này
      // hay tk bị block
    }
  }

  // void didShownKycEvent(AuthEvent event) {
  //   if (event == _initKycEvent) {
  //     _initKycEvent = null;
  //   }
  // }

  void changeNotifierModel() {
    notifyListeners();
  }

  void login(AuthInfo info) {
    _authInfo = info;
    _userRepository.setAutInfo(info);
    authEvent.add(OnLoginEvent(info));
    _onSessionStarted();
  }

  void endOnbarding() {
    authState.value = AuthState.authorized;
  }

  void startOnboarding() {
    authState.value = AuthState.onboarding;
  }

  void _onSessionStarted() {
    // if (_authInfo?.user.onBoardingData == null) {
    //   authState.value = AuthState.onboarding;
    // } else {
    authState.value = AuthState.authorized;
    // }
  }

  Future logout({bool force = false}) async {
    if (force == false) {
      if (_authInfo != null) {
        authEvent.add(OnLogoutEvent(_authInfo!));
      }

      // delay để các service khác có thời gian logout
      Future.delayed(const Duration(seconds: 3)).then((value) async {
        final res = await _userRepository.logout();
        authEvent.add(OnSessionTerminated());
        return res;
      }).onError((error, stackTrace) {
        Log.w(error, stackTrace);
        authEvent.add(OnSessionTerminated());
      });
    }

    resetLocatorOnLogout();
    _authInfo = null;
    _userRepository.setAutInfo(null);
    authState.value = AuthState.unauthorized;
  }

  void onUpdateUser(User? user) {
    _authInfo = _authInfo?.copyWith(user: user);
    _userRepository.setAutInfo(_authInfo!);
  }

  /// method này được gọi khi nhận noti5 từ server.
  ///
  ///  Gọi api get lại profile và hiện bị từ chối / chấp nhận
}

enum AuthState { unauthorized, authorized, onboarding }

class AuthEvent {}

// Event khi restore lại session từ đĩa
class OnSessionRestored extends AuthEvent {
  final AuthInfo info;

  OnSessionRestored(this.info);
}

// Event sau khi user login
class OnLoginEvent extends AuthEvent {
  final AuthInfo info;

  OnLoginEvent(this.info);
}

// Event ngay trước khi user logout bình thường
class OnLogoutEvent extends AuthEvent {
  final AuthInfo info;

  OnLogoutEvent(this.info);
}

/// Event khi api trả về 401 do session hết hạn
class OnUnauthorizedError extends AuthEvent {
  final RException error;

  OnUnauthorizedError(this.error);
}

/// Event sau khi session đã bị huỷ
class OnSessionTerminated extends AuthEvent {}

class OnKycApprovedEvent extends AuthEvent {}

class OnKycRejectedEvent extends AuthEvent {
  final String? reason;

  OnKycRejectedEvent({this.reason});
}
