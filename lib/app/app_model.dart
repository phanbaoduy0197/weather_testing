import 'dart:async';
import 'dart:io';

import 'package:base_entities/app/interceptor/auth_handler_interceptor.dart';
import 'package:base_entities/basic/misc/cache_manager.dart';
import 'package:base_entities/basic/misc/logger.dart';
import 'package:base_entities/basic/misc/noti5_manager.dart';
import 'package:base_entities/basic/theme/font_manager.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/data/client/rest_client.dart';
import 'package:base_entities/data/preferences/preferences.dart';
import 'package:base_entities/di/locator.dart';
import 'package:base_entities/flavors/native_bridge.dart';
import 'package:base_entities/module/widget/guide/guide_parameter.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

import 'user_model.dart';

class AppModel extends ChangeNotifier {
  AppState state = AppState.uninitialized;
  Object? initError;
  final _authHandlerInterceptor = AuthHandlerInterceptor();
  UserModel get userModel => locator<UserModel>();
  Noti5Manager get noti5Mgr => locator<Noti5Manager>();
  RCacheManager get cacheMgr => locator<RCacheManager>();

  final _subs = <StreamSubscription>[];

  AppModel() {
    _subs.addAll([
      userModel.authEvent.listen(onAuthEvent),
      _authHandlerInterceptor.onUnauthorizedError
          .listen((value) => onAuthEvent(OnUnauthorizedError(value))),
    ]);
  }

  setLanguage(BuildContext context, String language) async {
    RestClient.instance.setLanguage(language);
    if (EasyLocalization.of(context)?.locale.languageCode != language) {
      context.setLocale(Locale(language));
      Preferences.setLanguage(language);
      notifyListeners();
    }
  }

  @override
  void dispose() {
    for (var element in _subs) {
      element.cancel();
    }
    super.dispose();
  }

  init() async {
    initError = null;
    if (state != AppState.uninitialized) {
      state = AppState.uninitialized;
      notifyListeners();
    }

    try {
      //get device info

      // init rest client
      await _initRestClient();

      // init firebase
      await Firebase.initializeApp();

      // fetch config
      // locator<AppConfig>().fetch();

      // init nhiều thứ song song
      await Future.wait<dynamic>([
        //init theme
        locator<FontManager>().init(),
        locator<ThemeManager>().init(),
        //load guide status
        locator<GuideParameter>().init(),

        userModel.init(),
        //init  notification service
        noti5Mgr.init(),
      ]);
      // await requestAppTracking();
      state = AppState.initialized;
    } catch (e, _) {
      initError = e;
      state = AppState.error;
      // Log.e(e, stack);
    } finally {
      FlutterNativeSplash.remove();
      notifyListeners();
    }
  }

  Future _initRestClient() async {
    //get init language to header api
    var language = (await Preferences.getLanguage()) ?? "vi";
    final flavor = await locator.get<NativeBridge>().getFlavor();

    // packageInfoMock();
    RestClient.instance.init(
        flavor: flavor,
        authHandlerInterceptor: _authHandlerInterceptor,
        platform: Platform.isAndroid ? "android" : "ios",
        language: language);
  }

  // Future requestAppTracking() async {
  //   try {
  //     final TrackingStatus status =
  //     await AppTrackingTransparency.trackingAuthorizationStatus;
  //     if (status == TrackingStatus.notDetermined) {
  //       await AppTrackingTransparency.requestTrackingAuthorization();
  //     }
  //   } on PlatformException {
  //     //print('PlatformException was thrown');
  //   }
  // }

  ///
  /// Chỗ này để Handle khi login và logout
  void onAuthEvent(AuthEvent event) {
    if (event is OnSessionRestored) {
      Log.i("session: ${event.info.session?.value}");
      _authHandlerInterceptor.session = event.info.session;
      cacheMgr.session = event.info.session;
      // noti5Mgr.submitToken();
    } else if (event is OnLoginEvent) {
      Log.i("session: ${event.info.session?.value}");
      _authHandlerInterceptor.session = event.info.session;
      cacheMgr.session = event.info.session;
      // noti5Mgr.submitToken();
    } else if (event is OnLogoutEvent) {
      // noti5Mgr.clearToken();
      resetLocatorOnLogout();
    } else if (event is OnSessionTerminated) {
      _authHandlerInterceptor.session = null;
      cacheMgr.session = null;
    } else if (event is OnUnauthorizedError) {
      userModel.logout(force: true);
      _authHandlerInterceptor.session = null;
      cacheMgr.session = null;
      resetLocatorOnLogout();
    }
  }
}

enum AppState {
  uninitialized,
  initialized,
  error,
}

class AppEvent {
  static const eventUpdated = 1;

  final int? type;
  final dynamic arguments;

  AppEvent({this.type, this.arguments});
}
