import 'package:auto_route/auto_route.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/di/locator.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:base_entities/app/route/router.dart' as routes;

@RoutePage(name: "AuthRouterRoute")
class AuthRouterPage extends StatefulWidget {
  const AuthRouterPage({Key? key}) : super(key: key);

  @override
  State<AuthRouterPage> createState() => _AuthRouterPageState();
}

class _AuthRouterPageState extends State<AuthRouterPage> {
  late UserModel userModel;
  AuthState? currentState;

  final _routerKey = GlobalKey<AutoRouterState>();
  @override
  void initState() {
    super.initState();
    userModel = context.read<UserModel>();
    userModel.authState.addListener(_onAuthStateChanged);
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _onAuthStateChanged();
    });
  }

  @override
  void dispose() {
    userModel.authState.removeListener(_onAuthStateChanged);
    super.dispose();
  }

  void _onAuthStateChanged() {
    final authState = userModel.authState.value;
    final router = _routerKey.currentState?.controller;
    if (authState == currentState || router == null) return;

    currentState = authState;

    if (authState == AuthState.authorized) {
      router.replaceAll([const routes.MainRootRoute()]);
    } else {
      router.replaceAll([const routes.LoginRootRoute()]);
    }
  }

  @override
  Widget build(BuildContext context) {
    return AutoRouter(
      key: _routerKey,
    );
  }
}

class AuthGuard extends AutoRouteGuard {
  AuthGuard();

  @override
  void onNavigation(NavigationResolver resolver, StackRouter router) {
    final userModel = locator<UserModel>();
    final authState = userModel.authState.value;

    if (authState != AuthState.authorized &&
        authState != AuthState.onboarding) {
      router.replaceAll([const routes.LoginRootRoute()]);
      resolver.next(false);
    } else {
      resolver.next(true);
    }
  }
}
