// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i6;
import 'package:base_entities/app/route/auth_router.dart' as _i1;
import 'package:base_entities/app/route/router.dart' as _i2;
import 'package:base_entities/module/auth/ui/login_screen.dart' as _i3;
import 'package:base_entities/module/main/main_screen.dart' as _i4;
import 'package:base_entities/module/profile/update/update_profile_screen.dart'
    as _i5;

abstract class $RootRouter extends _i6.RootStackRouter {
  $RootRouter({super.navigatorKey});

  @override
  final Map<String, _i6.PageFactory> pagesMap = {
    AuthRouterRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i1.AuthRouterPage(),
      );
    },
    LoginRootRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.LoginRootScreen(),
      );
    },
    LoginRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i3.LoginScreen(),
      );
    },
    MainRootRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i2.MainRootScreen(),
      );
    },
    MainRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i4.MainScreen(),
      );
    },
    UpdateProfileRoute.name: (routeData) {
      return _i6.AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const _i5.UpdateProfileScreen(),
      );
    },
  };
}

/// generated route for
/// [_i1.AuthRouterPage]
class AuthRouterRoute extends _i6.PageRouteInfo<void> {
  const AuthRouterRoute({List<_i6.PageRouteInfo>? children})
      : super(
          AuthRouterRoute.name,
          initialChildren: children,
        );

  static const String name = 'AuthRouterRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i2.LoginRootScreen]
class LoginRootRoute extends _i6.PageRouteInfo<void> {
  const LoginRootRoute({List<_i6.PageRouteInfo>? children})
      : super(
          LoginRootRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginRootRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i3.LoginScreen]
class LoginRoute extends _i6.PageRouteInfo<void> {
  const LoginRoute({List<_i6.PageRouteInfo>? children})
      : super(
          LoginRoute.name,
          initialChildren: children,
        );

  static const String name = 'LoginRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i2.MainRootScreen]
class MainRootRoute extends _i6.PageRouteInfo<void> {
  const MainRootRoute({List<_i6.PageRouteInfo>? children})
      : super(
          MainRootRoute.name,
          initialChildren: children,
        );

  static const String name = 'MainRootRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i4.MainScreen]
class MainRoute extends _i6.PageRouteInfo<void> {
  const MainRoute({List<_i6.PageRouteInfo>? children})
      : super(
          MainRoute.name,
          initialChildren: children,
        );

  static const String name = 'MainRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}

/// generated route for
/// [_i5.UpdateProfileScreen]
class UpdateProfileRoute extends _i6.PageRouteInfo<void> {
  const UpdateProfileRoute({List<_i6.PageRouteInfo>? children})
      : super(
          UpdateProfileRoute.name,
          initialChildren: children,
        );

  static const String name = 'UpdateProfileRoute';

  static const _i6.PageInfo<void> page = _i6.PageInfo<void>(name);
}
