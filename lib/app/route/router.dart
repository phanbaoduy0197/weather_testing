import 'package:auto_route/auto_route.dart';
import 'package:base_entities/app/route/auth_router.dart';
import 'package:base_entities/app/route/router.gr.dart';
import 'package:flutter/cupertino.dart';

export 'package:auto_route/auto_route.dart';
export 'router.gr.dart';

class RoutePath {
  static const kRoot = "/root";
  static const kSplash = "/plash";
  static const kMain = "main";
  static const kLogin = "login";
  static const kListing = "listing";
  static const kHome = "home";
  static const kUpdate = "update";
}

@AutoRouterConfig(replaceInRouteName: 'Screen|Page,Route')
class RootRouter extends $RootRouter {
  @override
  RouteType get defaultRouteType => const RouteType.cupertino();

  RouterConfig<UrlState> appConfig() {
    return config(
      navigatorObservers: () => <NavigatorObserver>[
        // HeroController(),
        AutoRouteObserver(),
      ],
      // deepLinkBuilder: (deepLink) async {
      /// Note: Deep link của flutter không tương thích với uni_links & firebase_dynamic_links
      /// Khi gắn 2 package trên thì trên iOS sẽ không nhận được deep link ở đây
      /// code: https://github.com/flutter/engine/blob/8f7488e0b4b50f0e40a196a31c8e544be26b6a7e/shell/platform/darwin/ios/framework/Source/FlutterAppDelegate.mm#L211
      // },
    );
  }

  @override
  List<AutoRoute> get routes => [
        //HomeScreen is generated as HomeRoute because
        //of the replaceInRouteName property
        CustomRoute(
          initial: true,
          path: RoutePath.kRoot,
          page: AuthRouterRoute.page,
          children: [
            mainRoute,
            loginRoute,
          ],
        ),
        RedirectRoute(path: '*', redirectTo: RoutePath.kRoot),
      ];
}

final mainRoute = CustomRoute(
  transitionsBuilder: TransitionsBuilders.slideTop,
  path: RoutePath.kMain,
  page: MainRootRoute.page,
  children: [
    AutoRoute(
      guards: [AuthGuard()],
      page: MainRoute.page,
      path: '',
    ),
    AutoRoute(
      page: UpdateProfileRoute.page,
      path: RoutePath.kUpdate,
    ),
    RedirectRoute(path: '*', redirectTo: ''),
  ],
);

final loginRoute = CustomRoute(
  path: RoutePath.kLogin,
  page: LoginRootRoute.page,
  transitionsBuilder: TransitionsBuilders.slideBottom,
  children: [
    AutoRoute(
      path: '',
      page: LoginRoute.page,
    ),
  ],
);

@RoutePage(name: 'MainRootRoute')
class MainRootScreen extends AutoRouter {
  const MainRootScreen({super.key});
}

@RoutePage(name: 'LoginRootRoute')
class LoginRootScreen extends AutoRouter {
  const LoginRootScreen({super.key});
}
