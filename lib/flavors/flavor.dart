import 'package:base_entities/basic/misc/logger.dart';

class Flavor {
  final String _flavor;
  const Flavor._(this._flavor);

  static const staging = Flavor._("Staging");
  static const live = Flavor._("Live");
  factory Flavor.fromString(String? str) {
    if (str == live._flavor) return Flavor.live;
    if (str == staging._flavor) return Flavor.staging;

    Log.e("Unknwon flavor: $str");
    return Flavor.live;
  }
  bool get isLive => _flavor == live._flavor;
  static Flavor current = Flavor.live;
  @override
  String toString() {
    return _flavor;
  }
}
