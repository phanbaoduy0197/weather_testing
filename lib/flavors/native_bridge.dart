import 'dart:io';

import 'package:base_entities/flavors/flavor.dart';
import 'package:flutter/services.dart';

class NativeBridge {
  static const trackingStatusNotDetermined = 0;

  final MethodChannel _mc;

  const NativeBridge()
      : _mc = const MethodChannel('com.example.weather_testing/native');

  Future<Flavor> getFlavor() async {
    return Flavor.fromString(await _mc.invokeMethod('flavor'));
  }

  Future<int> getNoti5Permission() async {
    return await _mc.invokeMethod('noti5Permission');
  }

  Future setNoti5UnseenCount(int count) async {
    if (Platform.isIOS) {
      _mc.invokeMethod('setNoti5UnseenCount', count);
    }
  }

  // @returns
  // notDetermined = 0
  // restricted = 1
  // denied = 2
  // authorized = 3
  Future<int> getTrackingPermission() async {
    if (Platform.isIOS) {
      return await _mc.invokeMethod("getTrackingPermission");
    } else {
      return 3;
    }
  }

  Future<int> requestTrackingPermission() async {
    if (Platform.isIOS) {
      return await _mc.invokeMethod("requestTrackingPermission");
    } else {
      return 3;
    }
  }
}
