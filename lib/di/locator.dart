import 'package:base_entities/app/route/router.dart';
import 'package:base_entities/app/user_model.dart';
import 'package:base_entities/basic/misc/cache_manager.dart';
import 'package:base_entities/basic/misc/noti5_manager.dart';
import 'package:base_entities/basic/theme/theme_manager.dart';
import 'package:base_entities/basic/util/geolocator_manager.dart';
import 'package:base_entities/data/preferences/key_value_store.dart';
import 'package:base_entities/data/repository/common_repository.dart';
import 'package:base_entities/data/repository/user_repository.dart';
import 'package:base_entities/data/service/common_service.dart';
import 'package:base_entities/data/service/user_service.dart';
import 'package:base_entities/data/service/weather_service.dart';
import 'package:base_entities/flavors/native_bridge.dart';
import 'package:base_entities/module/auth/ui/login_model.dart';
import 'package:base_entities/module/home/home_model.dart';
import 'package:base_entities/module/profile/profile_model.dart';
import 'package:base_entities/module/main/main_model.dart';
import 'package:base_entities/module/parameter/weather_parameter.dart';
import 'package:base_entities/module/profile/update/update_profile_model.dart';
import 'package:base_entities/module/widget/guide/guide_parameter.dart';
import 'package:get_it/get_it.dart';

import '../app/app_model.dart';
import '../basic/theme/font_manager.dart';

final locator = GetIt.instance;

void setupLocator() {
  //app config
  locator.registerSingleton(RootRouter());
  locator.registerLazySingleton(() => const NativeBridge());
  // locator.registerLazySingleton(() => AppConfig());
  locator.registerLazySingleton(() => ThemeManager());
  locator.registerLazySingleton(() => FontManager());
  locator.registerLazySingleton(() => UserModel());
  locator.registerLazySingleton(() => AppModel());

  //db
  locator.registerSingleton<KeyValueStorage>(
      SharedPreferencesKeyValueStorage.newInstance());

  //repository
  locator.registerLazySingleton(() => CommonRepository());
  locator.registerLazySingleton(() => UserRepository());

  //service
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => CommonServive());
  locator.registerLazySingleton(() => WeatherService());

  //util
  locator.registerLazySingleton(() => Noti5Manager());
  locator.registerLazySingleton(() => RCacheManager());
  locator.registerLazySingleton(() => GeolocatorManager());

  locator.registerLazySingleton(() => WeatherParameter());
  locator.registerLazySingleton(() => GuideParameter());

  //-------

  //parameter
  // locator.registerLazySingleton(() => SearchParameters());

  //model screen
  locator.registerFactory(() => MainModel());
  locator.registerFactory(() => HomeModel());
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => ListingModel());
  locator.registerFactory(() => UpdateProfileModel());
}

void resetLocatorOnLogout() {
  locator.resetLazySingleton<WeatherParameter>(
      disposingFunction: (item) => item.dispose());
}
