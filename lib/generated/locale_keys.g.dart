// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const locale = 'locale';
  static const en = 'en';
  static const vi = 'vi';
  static const appName = 'appName';
  static const otp = 'otp';
  static const login = 'login';
  static const retry = 'retry';
  static const unknownErrorMessage = 'unknownErrorMessage';
  static const confirm = 'confirm';
  static const cancel = 'cancel';
  static const backToExit = 'backToExit';
  static const noData = 'noData';

}
