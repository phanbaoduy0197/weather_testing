package com.example.weather_testing

import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.os.Bundle
import androidx.annotation.NonNull
import androidx.core.app.NotificationManagerCompat
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel

class MainActivity: FlutterActivity() {
    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "com.example.weather_testing/native").setMethodCallHandler { call, result ->
            if(call.method == "flavor") {
                val app: ApplicationInfo = context.packageManager.getApplicationInfo(context.packageName, PackageManager.GET_META_DATA)
                val bundle: Bundle = app.metaData
                val flavor = bundle.getString("flavor")
                result.success(flavor)
            } else if(call.method == "noti5Permission") {
                val enabled = NotificationManagerCompat.from(context).areNotificationsEnabled()
                if(enabled) {
                    result.success(1)
                }
                else {
                    result.success(-1)
                }
            }
            else {
                result.notImplemented()
            }
        }
        super.configureFlutterEngine(flutterEngine)
    }
}
